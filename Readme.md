Water project by Beulard :
===============================================
Matthias 'Beulard' Dubouchet <beulard@gmail.com>

-------------

Tools used :
------------
* C++11
* QtCreator 2.6.2 with Qt libraries 5.0.1
* MinGW with gcc/g++ 4.7.2

Libraries used :
----------------
* FreeType 2
* GLFW 2.7.6
* GLEW 1.9.0
* cJSON
* libpng 1.5.13
* zlib 1.2.7
