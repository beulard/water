TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
DEFINES += GLEW_STATIC
CONFIG(debug) {
    DEFINES += WATER_DEBUG
}
QMAKE_CXXFLAGS += -std=c++0x
DESTDIR = bin
OBJECTS_DIR = obj

win32: LIBS += -L$$PWD/lib/ -lglfw -lopengl32 -lpng -lfreetype -lcJSON -lzlib -static -static-libgcc -static-libstdc++


INCLUDEPATH += $$PWD/include + $$PWS/include/GL + $$PWD/src/Game + $$PWD/src/Map + $$PWD/src/Physics + $$PWD/src/Render + $$PWD/src/UI
DEPENDPATH += $$PWD/include

SOURCES += \
    src/Game/World.cpp \
    src/Game/wJSON.cpp \
    src/Game/Wash.cpp \
    src/Game/Timer.cpp \
    src/Game/Tile.cpp \
    src/Game/Player.cpp \
    src/Game/Mouse.cpp \
    src/Game/MasterTimer.cpp \
    src/Game/MasterAnim.cpp \
    src/Game/InputManager.cpp \
    src/Game/Game.cpp \
    src/Game/FileStream.cpp \
    src/Game/Entity.cpp \
    src/Game/DataLoader.cpp \
    src/Game/Common.cpp \
    src/Game/Camera.cpp \
    src/Map/Map.cpp \
    src/Map/Chunk.cpp \
    src/Map/Cell.cpp \
    src/Map/Biome.cpp \
    src/Physics/AABB.cpp \
    src/Render/VertexShader.cpp \
    src/Render/Triangle.cpp \
    src/Render/Texture.cpp \
    src/Render/Text.cpp \
    src/Render/SpriteBatch.cpp \
    src/Render/Sprite.cpp \
    src/Render/Shape.cpp \
    src/Render/ShaderProgram.cpp \
    src/Render/Shader.cpp \
    src/Render/Renderer.cpp \
    src/Render/Quadmesh.cpp \
    src/Render/QuadBatch.cpp \
    src/Render/Quad.cpp \
    src/Render/Outline.cpp \
    src/Render/Mesh.cpp \
    src/Render/Line.cpp \
    src/Render/Glyph.cpp \
    src/Render/FragmentShader.cpp \
    src/Render/Font.cpp \
    src/Render/Drawable.cpp \
    src/Render/Colored.cpp \
    src/Render/Color.cpp \
    src/Render/Batch.cpp \
    src/Render/Anim.cpp \
    src/UI/Widget.cpp \
    src/UI/UIWindowResizer.cpp \
    src/UI/UIWindowBar.cpp \
    src/UI/UIWindow.cpp \
    src/UI/UIText.cpp \
    src/UI/UISprite.cpp \
    src/UI/UIShape.cpp \
    src/UI/UIMainMenu.cpp \
    src/UI/UILoadingScreen.cpp \
    src/UI/UIColored.cpp \
    src/UI/UIButton.cpp \
    src/UI/RootWidget.cpp \
    src/main.cpp \
    src/glew.c \
    src/Map/Noise.cpp

HEADERS += \
    src/Game/World.hpp \
    src/Game/wJSON.hpp \
    src/Game/Wash.hpp \
    src/Game/Timer.hpp \
    src/Game/Tile.hpp \
    src/Game/Rect.hpp \
    src/Game/Random.hpp \
    src/Game/Player.hpp \
    src/Game/Mouse.hpp \
    src/Game/MasterTimer.hpp \
    src/Game/MasterAnim.hpp \
    src/Game/InputManager.hpp \
    src/Game/Game.hpp \
    src/Game/FileStream.hpp \
    src/Game/Entity.hpp \
    src/Game/DataLoader.hpp \
    src/Game/Common.hpp \
    src/Game/Camera.hpp \
    src/Map/Map.hpp \
    src/Map/Chunk.hpp \
    src/Map/Cell.hpp \
    src/Map/Biome.hpp \
    src/Physics/vec2.hpp \
    src/Physics/AABB.hpp \
    src/Render/VertexShader.hpp \
    src/Render/Triangle.hpp \
    src/Render/Texture.hpp \
    src/Render/Text.hpp \
    src/Render/SpriteBatch.hpp \
    src/Render/Sprite.hpp \
    src/Render/Shape.hpp \
    src/Render/ShaderProgram.hpp \
    src/Render/Shader.hpp \
    src/Render/Renderer.hpp \
    src/Render/Quadmesh.hpp \
    src/Render/QuadBatch.hpp \
    src/Render/Quad.hpp \
    src/Render/Outline.hpp \
    src/Render/Mesh.hpp \
    src/Render/Line.hpp \
    src/Render/Glyph.hpp \
    src/Render/FragmentShader.hpp \
    src/Render/Font.hpp \
    src/Render/Drawable.hpp \
    src/Render/Colored.hpp \
    src/Render/Color.hpp \
    src/Render/Batch.hpp \
    src/Render/Anim.hpp \
    src/UI/Widget.hpp \
    src/UI/UIWindowResizer.hpp \
    src/UI/UIWindowBar.hpp \
    src/UI/UIWindow.hpp \
    src/UI/UIText.hpp \
    src/UI/UISprite.hpp \
    src/UI/UIShape.hpp \
    src/UI/UIMainMenu.hpp \
    src/UI/UILoadingScreen.hpp \
    src/UI/UIColored.hpp \
    src/UI/UIButton.hpp \
    src/UI/RootWidget.hpp \
    src/Map/Noise.hpp
