#ifndef WATER_QUAD
#define WATER_QUAD
#include "Drawable.hpp"
#include "Colored.hpp"

//	The Quad class is not considered a shape
//	because I use quad batches to render them.
//	They therefore don't need a Render method
//	and are just a colored drawable.
class Quad : public Colored
{
    public:
        Quad();
        Quad( const Quad& other );
        void operator=( const Quad& other );
        Quad( const vec2i& pos, const vec2ui& sz );
        ~Quad();

		virtual void Move( const vec2i& offset );

        virtual void SetSize( const vec2ui& sz );
		void AddToBatch();
		void RemoveFromBatch();

        virtual void Draw();
		virtual void StopDrawing();
};

#endif // WATER_QUAD
