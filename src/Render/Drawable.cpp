#include "Drawable.hpp"
#include "Renderer.hpp"
#include "ShaderProgram.hpp"
#include "Common.hpp"

Drawable::Drawable() : size( vec2ui(0, 0) ), position( vec2i(0, 0) ), customShader( NULL ), depth( RD_MiddleLayer ), batch( NULL ), batchID(-1), drawn(false) {
    SetRenderMode( RM_None );
}

Drawable::Drawable( const Drawable& other ) : depth(RD_MiddleLayer), batch( NULL ), batchID(-1), drawn(false) {
    *this = other;
}

void Drawable::operator=( const Drawable& other ) {
    SetRenderMode( other.mode );
    customShader = other.customShader;
    depth = other.depth;
    size = other.size;
    position = other.position;
}

Drawable::~Drawable(){

}

void Drawable::SetPosition( const vec2i& pos ) {
    position = pos;
}

const vec2i& Drawable::GetPosition() const {
    return position;
}

void Drawable::Move( const vec2i& offset ) {
    position += offset;
}

void Drawable::SetSize( const vec2ui& sz ) {
    size = sz;
}

const vec2ui& Drawable::GetSize() const {
    return size;
}

void Drawable::SetShader( ShaderProgram* s ) {
    shader = s;
}

ShaderProgram* Drawable::GetShader() const {
    return shader;
}

RenderMode Drawable::GetRenderMode()    const {
    return mode;
}

void Drawable::SetRenderMode( RenderMode pMode ) {
    mode = pMode;
    switch( mode ) {
        case RM_UIText :
            shader = Renderer::UITextShader;
        break;
        case RM_WorldText :
            shader = Renderer::worldTextShader;
        break;
        case RM_UISprite :
            shader = Renderer::UISpriteShader;
        break;
        case RM_WorldSprite :
            shader = Renderer::worldSpriteShader;
        break;
        case RM_UIColored :
            shader = Renderer::UIColoredShader;
        break;
        case RM_WorldColored :
            shader = Renderer::worldColoredShader;
        break;
        case RM_Custom :
            shader = customShader;
        break;
        case RM_None :
            shader = NULL;
        break;
        default :
            Info( "Not a valid render mode" );
        break;
    }
}

ShaderProgram* Drawable::GetCustomShader()  const {
    return customShader;
}

void Drawable::SetCustomShader( ShaderProgram* s) {
    customShader = s;
    shader = s;
}

bool Drawable::IsDrawn() const {
    return drawn;
}

void Drawable::SetBatch( Batch* b ) {
    batch = b;
}

void Drawable::SetBatchID( int i ) {
    batchID = i;
    if( batchID == -1 )
        drawn = false;
}

int Drawable::GetBatchID() const {
    return batchID;
}

void Drawable::SetDepth( RenderDepth d ) {
    depth = d;
}

RenderDepth Drawable::GetDepth()    const {
    return depth;
}
