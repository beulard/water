#ifndef QUADBATCH_HPP
#define QUADBATCH_HPP
#include "Batch.hpp"
#include "Color.hpp"

//	Quad batches deal with quadrilateral shapes
class QuadBatch : public Batch
{
	public:
		QuadBatch();
		QuadBatch( const QuadBatch& other );
		void operator=( const QuadBatch& other );
		QuadBatch( ShaderProgram* s, RenderDepth d );
		~QuadBatch();

		virtual void AddDrawable( Drawable& d );
		virtual void RemoveDrawable( int id );

		void SetQuadColor( int id, const Color& col );

		virtual void Update();
		virtual void Render();

	private:
		//	A second vector, to hold shape colors
		std::vector< float > colors;

		//	OpenGL id for the color buffer
		GLuint cbID;
};

#endif // QUADBATCH_HPP
