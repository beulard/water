#include "Colored.hpp"

Colored::Colored() : color(Color::White), type(CT_Line) {
    //Info( "Coloredc" );
}

Colored::~Colored(){
    //Info( "Coloredd" );
}

void Colored::SetColor( const Color& col ) {
    color = col;
}

const Color& Colored::GetColor() const {
    return color;
}

const ColoredType& Colored::GetType() const {
    return type;
}
