#include "Text.hpp"
#include "Renderer.hpp"
#include "Camera.hpp"
#include "Common.hpp"

Text::Text() : uvID(0), font( NULL ), lineHeight( 0 ), nbrLines( 0 ) {
    glGenBuffers( 1, &uvID );
    if( uvID == 0 )
        Error( "Couldn't create a GL vertex buffer" );
    depth = RD_MiddleLayer;
    SetRenderMode( RM_WorldText );
}

Text::Text( const Text& t ) : Colored(), Mesh( t ), uvID(0) {
    glGenBuffers( 1, &uvID );
    if( uvID == 0 )
        Error( "Couldn't create a GL vertex buffer" );
    *this = t;
}

void Text::operator=( const Text& t ) {
    if( drawn )
        StopDrawing();
    lineHeight = 0;
    position = t.position;
    rotation = t.rotation;
    font = t.font;
    color = t.color;
    origin = t.origin;
    cursor = t.origin;
    size = vec2ui( 0, 0 );
    nbrLines = 0;
    SetString( t.text );

    needsUpdate = true;
}

Text::Text( const std::string& s, Font* f, const Color& col ) : uvID(0), lineHeight( 0 ), nbrLines( 0 ) {
    glGenBuffers( 1, &uvID );
    if( uvID == 0 )
        Error( "Couldn't create a GL vertex buffer" );
    SetFont(f);
    color =	col;
    size = vec2ui( 0, 0 );
    for( u32 i = 0; i < s.size(); ++i )
        AddChar( s[i] );
    SetRenderMode( RM_WorldText );
    depth = RD_MiddleLayer;
}

Text::~Text(){
    glDeleteBuffers( 1, &uvID );
    if( drawn )
        StopDrawing();
    Clear();
}

void Text::Draw() {
    if( !drawn )
        Global::renderer->Draw( *this );
    else
        Warning( "Trying to add a text to the renderer twice" );
    drawn = true;
}

void Text::StopDrawing() {
    if( drawn )
        Global::renderer->StopDrawing( *this );
    else
        Warning( "Trying to remove a text from renderer but it is not in" );
    drawn = false;
}

void Text::Render() {
    shader->Use();

    if( !font )
        Error( "Trying to draw a text with no font !" );

    if( needsUpdate )
        UpdateGLVertices();


    glActiveTexture( GL_TEXTURE0 );
    glBindTexture( GL_TEXTURE_2D, font->GetID() );

    //  Send the projection matrix to the shader
    int index = glGetUniformLocation( shader->GetID(), "projMat" );
    if( index == -1 )
        Error( "No uniform 'projMat' in shader !" );
    glUniformMatrix3fv( index, 1, GL_FALSE, Global::projMatrix );

    index = glGetUniformLocation( shader->GetID(), "color" );
    if( index == -1 )
        Error( "No uniform 'color' in shader !" );
    glUniform4f( index, color.r / 255.f, color.g / 255.f, color.b / 255.f, color.a / 255.f );

    if( mode == RM_WorldText ) {
        index = glGetUniformLocation( shader->GetID(), "camPos" );
        if( index == -1 )
            Error( "No uniform 'camPos' in shader !" );
        glUniform2f( index, Global::camera->GetGLPosition().x, Global::winSz->y - Global::camera->GetGLPosition().y );
    }

    //  Send the vertex and texture coordinates attributes to the shader
    glEnableVertexAttribArray(0);
    glBindBuffer( GL_ARRAY_BUFFER, vbID );
    glVertexAttribPointer( 0, 2, GL_FLOAT, GL_FALSE, 0, 0 );
    glEnableVertexAttribArray(1);
    glBindBuffer( GL_ARRAY_BUFFER, uvID );
    glVertexAttribPointer( 1, 2, GL_FLOAT, GL_FALSE, 0, 0 );

    glDrawArrays( GL_QUADS, 0, vertices.size() / 2 );

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
}

void Text::SetPosition( const vec2i& pos ) {
    vec2i offset = pos - position;
    Move( offset );
}

void Text::Move( const vec2i& offset ) {
    //  The vertices vector contains UVs and vertices. Every 2 value is a vertex coordinate, so we have this i += 2
    for( u32 i = 0; i < vertices.size(); i += 2 ) {
        vertices[i] += offset.x;
        vertices[i + 1] -= offset.y;
    }

    position += offset;
    origin.x += offset.x;
    origin.y -= offset.y;
    cursor.x += offset.x;
    cursor.y -= offset.y;
    needsUpdate = true;
}

void Text::AddChar( char c ) {
    if( !font )
        Error( "You need to specify a font before adding any character to a Text !" );
    if( nbrLines == 0 )
        nbrLines++;
    text += c;
    const Glyph& g = font->GetGlyph( c );
    vec2i sz = g.GetSize();
    vec2i ad = g.GetAdvance();
    vec2i o = g.GetTopLeftOffset();

    if( sz.y > (int)lineHeight && c != '\n' )
        lineHeight = sz.y;

    if( lineHeight > size.y )
        size.y = lineHeight;

    if( cursor.x + sz.x > (int)size.x )
        size.x = cursor.x + sz.x - position.x;

    if( text.size() == 1 ) {
        //  There we set the origin of our text, aka the baseline for every new character
        origin = vec2i( position.x, Global::winSz->y - position.y - o.y );
        cursor = origin;
    }

    if( c == '\n' ) {
        cursor.x = origin.x;
        cursor.y -= lineHeight + 3;
        ad.x = 0;
        size.y += lineHeight;
        nbrLines++;
    }
    vec2f pos( cursor.x + o.x, cursor.y + o.y );

    vertices.push_back( pos.x );
    vertices.push_back( pos.y );
    vertices.push_back( pos.x + sz.x );
    vertices.push_back( pos.y );
    vertices.push_back( pos.x + sz.x );
    vertices.push_back( pos.y - sz.y );
    vertices.push_back( pos.x );
    vertices.push_back( pos.y - sz.y );
    UVs.push_back( g.GetUV( 0 ).x );
    UVs.push_back( g.GetUV( 0 ).y );
    UVs.push_back( g.GetUV( 1 ).x );
    UVs.push_back( g.GetUV( 1 ).y );
    UVs.push_back( g.GetUV( 2 ).x );
    UVs.push_back( g.GetUV( 2 ).y );
    UVs.push_back( g.GetUV( 3 ).x );
    UVs.push_back( g.GetUV( 3 ).y );


    cursor.x += ad.x;
    needsUpdate = true;
}

void Text::SetString( const std::string& s ) {
    //  We need to create a temporary copy in case the string is being rewritten with a different font,
    //  in which case the 'text' vector is going to be cleared
    Clear();
    for( u32 i = 0; i < s.size(); ++i )
        AddChar( s[i] );
}

const std::string& Text::GetString()    const {
    return text;
}

void Text::Clear() {
    vertices.clear();
    UVs.clear();
    text.clear();
    needsUpdate = true;
}

void Text::SetFont( Font* f ) {
    font = f;
    if( font )
        SetString( text );
}

void Text::UpdateGLVertices() {
    glBindBuffer( GL_ARRAY_BUFFER, vbID );
    glBufferData( GL_ARRAY_BUFFER, vertices.size() * sizeof( float ), &vertices[0], GL_DYNAMIC_DRAW );
    glBindBuffer( GL_ARRAY_BUFFER, uvID );
    glBufferData( GL_ARRAY_BUFFER, UVs.size() * sizeof( float ), &UVs[0], GL_DYNAMIC_DRAW );
    needsUpdate = false;
}

