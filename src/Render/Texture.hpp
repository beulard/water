#ifndef WATER_TEXTURE
#define WATER_TEXTURE
#include <GL/glew.h>
#include "vec2.hpp"
#include <vector>
#include "Common.hpp"

class Texture
{
    public:
        Texture();
        ~Texture();

        GLuint GetID()  const;

        void SetSize( const vec2i& sz );
        void SetColorType( ColorType t );

        const vec2i& GetSize()  const;
        const ColorType& GetColorType()  const;

        void SetName( const std::string& n );
        const std::string& GetName();

        const ColorType& GetType() const;

        void SetID( GLuint id );

        //	A temporary buffer where texture data will be kept after
        //	reading the image file and cleared after it is loaded into OpenGL
        std::vector< u8 > buffer;

    private:
        GLuint id;
        vec2i size;
        ColorType type;
        std::string name;
};

#endif // WATER_TEXTURE
