#include "Batch.hpp"
#include "Camera.hpp"
#include "Common.hpp"

Batch::Batch() : shader( NULL ), vbID( 0 ), depth( RD_MiddleLayer ) {
    glGenBuffers( 1, &vbID );
    if( vbID == 0 )
        Error( "Couldn't create a GL vertex buffer" );
}

Batch::Batch( const Batch& other ) {
    glGenBuffers( 1, &vbID );
    if( vbID == 0 )
        Error( "Couldn't create a GL vertex buffer" );
    *this = other;
}

void Batch::operator=( const Batch& other ) {
    shader = other.shader;
    vertices = other.vertices;
    drawables = other.drawables;
    depth = other.depth;
    needsUpdate = true;
}

Batch::Batch( ShaderProgram* s, RenderDepth d ) : shader(s), depth(d) {
    glGenBuffers( 1, &vbID );
    if( vbID == 0 )
        Error( "Couldn't create a GL vertex buffer" );
}

Batch::~Batch() {
    while( !drawables.empty() ) {
        drawables.back()->SetBatchID( -1 );
        drawables.back()->SetBatch( NULL );
        drawables.pop_back();
    }
    glDeleteBuffers( 1, &vbID );
}

void Batch::AddDrawable( Drawable& s ) {
    //	Add 4 vertices to our array
    for( u32 i = 0; i < 8; ++i )
        vertices.push_back( 0.f );
    drawables.push_back( &s );
    drawables.back()->SetBatchID( drawables.size() - 1 );
    drawables.back()->SetBatch( this );

    SetDrawablePosition( drawables.size() - 1, s.GetPosition() );
    SetDrawableSize( drawables.size() - 1, s.GetSize() );

    needsUpdate = true;
}

void Batch::RemoveDrawable( int id ) {
    if( (u32)id >= drawables.size() )
        Error( "Trying to remove an invalid Drawable (id = %d)", id );

    drawables[id]->SetBatchID( -1 );
    if( (u32)id != drawables.size() - 1 ) {
        if( drawables.size() > 1 ) {
            drawables[id] = drawables.back();
            drawables[id]->SetBatch( this );
            drawables[id]->SetBatchID( id );

            u32 sz = vertices.size();
            for( u32 i = 0; i < 8; ++i ) {
                vertices[id * 8 + 7 - i] = vertices[sz - 1 - i];
                vertices.pop_back();
            }
        }
    }
    else
        for( u32 i = 0; i < 8; ++i )
            vertices.pop_back();


    drawables.pop_back();
    needsUpdate = true;
}

void Batch::SetShader( ShaderProgram* s ) {
    shader = s;
}

void Batch::SetDrawablePosition( int id, const vec2i& position ) {
    if( (u32)id >= drawables.size() )
        Error( "Trying to access an invalid Drawable's vertices (id = %d)", id );

    vec2i topLeft( position.x, Global::winSz->y - position.y - drawables[id]->GetSize().y );
    vec2i botRight( position.x + drawables[id]->GetSize().x, Global::winSz->y - position.y );
    vertices[id * 8] = topLeft.x;
    vertices[id * 8 + 1] = topLeft.y;
    vertices[id * 8 + 2] = botRight.x;
    vertices[id * 8 + 3] = topLeft.y;
    vertices[id * 8 + 4] = botRight.x;
    vertices[id * 8 + 5] = botRight.y;
    vertices[id * 8 + 6] = topLeft.x;
    vertices[id * 8 + 7] = botRight.y;
    needsUpdate = true;
}

void Batch::MoveDrawable( int id, const vec2i& offset ) {
    if( (u32)id >= drawables.size() )
        Error( "Trying to access an invalid Drawable's vertices (id = %d)", id );

    for( u32 i = 0; i < 8; i += 2 ) {
        vertices[id * 8 + i] += offset.x;
        vertices[id * 8 + i + 1] -= offset.y;
    }
    needsUpdate = true;
}

void Batch::SetDrawableSize( int id, const vec2ui& size ) {
    if( (u32)id >= drawables.size() )
        Error( "Trying to access an invalid Drawable's size (id = %d)", id );

    vertices[id * 8 + 2] = vertices[id * 8] + size.x;
    vertices[id * 8 + 3] = vertices[id * 8 + 1];
    vertices[id * 8 + 4] = vertices[id * 8] + size.x;
    vertices[id * 8 + 5] = vertices[id * 8 + 1] + size.y;
    vertices[id * 8 + 6] = vertices[id * 8];
    vertices[id * 8 + 7] = vertices[id * 8 + 1] + size.y;
    needsUpdate = true;
}

void Batch::Update() {
    glBindBuffer( GL_ARRAY_BUFFER, vbID );
    glBufferData( GL_ARRAY_BUFFER, vertices.size() * sizeof( float ), &vertices[0], GL_DYNAMIC_DRAW );
    needsUpdate = false;
}

void Batch::Render() {
    if( drawables.size() > 0 ) {
        if( needsUpdate )
            Update();

        shader->Use();

        //  Send the projection matrix to the shader
        int index = glGetUniformLocation( shader->GetID(), "projMat" );
        if( index == -1 )
            Error( "No uniform 'projMat' in shader" );
        glUniformMatrix3fv( index, 1, GL_FALSE, Global::projMatrix );

        if( shader->UsesCamera() ) {
            index = glGetUniformLocation( shader->GetID(), "camPos" );
            if( index == -1 )
                Error( "No uniform 'camPos' in shader" );
            glUniform2f( index, Global::camera->GetGLPosition().x, Global::winSz->y - Global::camera->GetGLPosition().y );
        }

        //  Send the vertex attributes to the shader
        glEnableVertexAttribArray(0);
        glBindBuffer( GL_ARRAY_BUFFER, vbID );
        glVertexAttribPointer( 0, 2, GL_FLOAT, GL_FALSE, 0, 0 );

        glDrawArrays( GL_QUADS, 0, 4 * drawables.size() );

        glDisableVertexAttribArray(0);
    }
}

void Batch::SetDepth( RenderDepth d ) {
    depth = d;
}

RenderDepth Batch::GetDepth() const {
    return depth;
}

