#include "Outline.hpp"
#include "Common.hpp"

Outline::Outline(){
    SetRenderMode( RM_WorldColored );
}

//  The width / 2 thing here is to avoid line overlapping, which would be visible if the outline was a bit transparent. The same thing is done in SetPosition().
Outline::Outline( int pLeft, int pTop, int pRight, int pBottom, const Color& pColor, int pWidth ) : left( vec2i( pLeft, pTop - pWidth / 2 ), vec2i( pLeft, pBottom + pWidth / 2 ), pColor, pWidth ),
                                                                                                                                                                                      top( vec2i( pLeft + pWidth / 2, pTop ), vec2i( pRight - pWidth / 2, pTop ), pColor, pWidth ),
                                                                                                                                                                                      right( vec2i( pRight, pTop - pWidth / 2 ), vec2i( pRight, pBottom + pWidth / 2 ), pColor, pWidth ),
                                                                                                                                                                                      bottom( vec2i( pLeft + pWidth / 2, pBottom ), vec2i( pRight - pWidth / 2, pBottom ), pColor, pWidth ),
                                                                                                                                                                                      width( pWidth ) {
    position = vec2i( pLeft, pTop );
    SetRenderMode( RM_WorldColored );
    left.SetRenderMode( RM_WorldColored );
    top.SetRenderMode( RM_WorldColored );
    right.SetRenderMode( RM_WorldColored );
    bottom.SetRenderMode( RM_WorldColored );
}

Outline::~Outline(){
    if( drawn )
        StopDrawing();
}

void Outline::SetLeft( int pos ) {
    int t = top.GetPosition().y;
    int b = bottom.GetPosition().y;
    left.SetPosition( vec2i( pos, t - width / 2 ) );
    top.SetStart( vec2i( pos + width / 2, t ) );
    bottom.SetStart( vec2i( pos + width / 2, b ) );
    position.x = pos;
};

void Outline::SetTop( int pos ) {
    int l = left.GetPosition().x;
    int r = right.GetPosition().x;
    top.SetPosition( vec2i( l + width / 2, pos ) );
    left.SetStart( vec2i( l, pos - width / 2 ) );
    right.SetStart( vec2i( r, pos - width / 2 ) );
    position.y = pos;
}

void Outline::SetRight( int pos ) {
    int t = top.GetPosition().y;
    int b = bottom.GetPosition().y;
    right.SetPosition( vec2i( pos, t - width / 2 ) );
    top.SetEnd( vec2i( pos - width / 2, t ) );
    bottom.SetEnd( vec2i( pos - width / 2, b ) );
}

void Outline::SetBottom( int pos ) {
    int l = left.GetPosition().x;
    int r = right.GetPosition().x;
    bottom.SetPosition( vec2i( l + width / 2, pos ) );
    left.SetEnd( vec2i( l, pos + width / 2 ) );
    right.SetEnd( vec2i( r, pos + width / 2 ) );
}

void Outline::SetWidth( int pWidth ) {
    width = pWidth;
    left.SetWidth( width );
    top.SetWidth( width );
    right.SetWidth( width );
    bottom.SetWidth( width );
}

void Outline::SetColor( const Color& pColor ) {
    left.SetColor( pColor );
    top.SetColor( pColor );
    right.SetColor( pColor );
    bottom.SetColor( pColor );
}

void Outline::Draw() {
    if( !drawn ) {
        left.Draw();
        top.Draw();
        right.Draw();
        bottom.Draw();
    }
    else
        Warning( "Trying to add an outline to the renderer twice" );
    drawn = true;
}

void Outline::StopDrawing() {
    if( drawn ) {
        left.StopDrawing();
        top.StopDrawing();
        right.StopDrawing();
        bottom.StopDrawing();
    }
    else
        Warning( "Trying to remove an outline from the renderer but it isn't in" );
    drawn = false;
}

void Outline::Render() {
    left.Render();
    top.Render();
    right.Render();
    bottom.Render();
}

void Outline::SetPosition( const vec2i& pos ) {
    vec2i offset = vec2i( pos.x - left.GetPosition().x, pos.y - top.GetPosition().y );
    Move( offset );
}

void Outline::SetSize( const vec2ui& sz ) {
    left.SetPosition( vec2i( 0, 0 ) );
    top.SetPosition( vec2i( 0, 0 ) );
    SetRight( left.GetPosition().x + sz.x );
    SetBottom( top.GetPosition().y + sz.y );
    size = sz;
}

void Outline::Move( const vec2i& offset ) {
    left.Move( offset );
    top.Move( offset );
    right.Move( offset );
    bottom.Move( offset );
}

const vec2i& Outline::GetPosition()  const {
    return position;
}
