#ifndef WATER_ANIM
#define WATER_ANIM
#include "Sprite.hpp"
#include "Timer.hpp"
#include <vector>

typedef unsigned int u32;

class Anim : public Drawable
{
    public:
        Anim();
        Anim( const Anim& anim );
        void operator=( const Anim& anim );
        virtual ~Anim();

        void AddFrame( Sprite& s, float time, float rotation = 0.f, vec2i offset = vec2i( 0, 0 ) );

        void Move( const vec2i& offset );
        void SetPosition( const vec2i& pos );
        const vec2i& GetPosition()  const;

        virtual void SetSize( const vec2ui& sz );

        void Rotate( float angle );
        float GetRotation() const;

        u32 GetFrameNumber()    const;
        u32 GetCurrentFrame()   const;

        virtual void Draw();
        virtual void StopDrawing();

        bool HasChangedFrame();

        void Update();
        void SetAnimID( int i );


    private:
        float rotation;
        std::vector< Sprite > sprites;
        //  Time between the next Sprite has to be drawn
        std::vector< float > frameTimes;
        Timer timer;
        bool running;
        u32 currentFrame;
        //  Set to true if the anim just changed frame
        //	Set to false on the next game update
        bool changedFrame;

        //	ID in the MasterAnim class
        int animID;
};

#endif // WATER_ANIM
