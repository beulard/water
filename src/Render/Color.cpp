#include "Color.hpp"

Color Color::White = Color( 255, 255, 255 );
Color Color::Green = Color( 0, 255, 0 );
Color Color::Blue = Color( 0, 0, 255 );
Color Color::Red = Color( 255, 0, 0 );
Color Color::Yellow = Color( 255, 255, 0 );
Color Color::Teal = Color( 0, 255, 255 );
Color Color::Purple = Color( 255, 0, 255 );
Color Color::Black = Color( 0, 0, 0 );

Color::Color() : r(255), g(255), b(255), a(255) {

}

Color::Color( u8 pR, u8 pG, u8 pB, u8 pA ) : r(pR), g(pG), b(pB), a(pA) {

}

Color::Color( u8 pR, u8 pG, u8 pB ) : r(pR), g(pG), b(pB), a(255) {

}

Color::~Color(){

}
