#ifndef WATER_RENDERER
#define WATER_RENDERER
#include "Mesh.hpp"
#include "ShaderProgram.hpp"
#include "QuadBatch.hpp"
#include "Outline.hpp"
#include "Timer.hpp"
#include "Text.hpp"
#include "Wash.hpp"
#include <map>

class SpriteBatch;

using namespace std;

class Renderer
{
    public:
        Renderer();
        ~Renderer();

		void Init();
        void Clean();

        void Clear();
        void Draw( Drawable& d );
		void StopDrawing( Drawable& d );
        void Render();


        static ShaderProgram* worldSpriteShader;
		static ShaderProgram* worldColoredShader;
        static ShaderProgram* worldTextShader;
        static ShaderProgram* UISpriteShader;
		static ShaderProgram* UIColoredShader;
        static ShaderProgram* UITextShader;

        void AddSpriteBatch( const string& name, SpriteBatch* batch );
        SpriteBatch* GetSpriteBatch( const string& name );
        void ClearElements();


    private:
		//	We have two dimensional vectors because of depth management
		//	The depth of texts and shapes will be held in the first vector
		//	So that we can draw everything in the right order
		vector< vector< Shape* > > shapes;
		vector< vector< Text* > > texts;
        struct {
            //  This map will contain pointers to the sprite batches that can be found by texture name for easy access
            map< Wash, SpriteBatch* > byName;
            //  And this vector will contain pointers to sprite batches ordered by depth for easy rendering / cleaning
            vector< vector< SpriteBatch* > > byDepth;
        } spriteBatches;
		//	Quad batches in order of depth
		vector< vector< QuadBatch* > > quadBatches;

};

#endif // WATER_RENDERER
