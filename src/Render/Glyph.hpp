#ifndef WATER_GLYPH
#define WATER_GLYPH
#include "Quadmesh.hpp"
#include "Font.hpp"
#include "Color.hpp"

class Glyph
{
    public:
        Glyph();
        ~Glyph();

        const vec2i& GetAdvance()    const;
        const vec2i& GetBearing()    const;
        const vec2i& GetSize()       const;
        const vec2f& GetUV( u32 index )    const;
        const vec2i& GetTopLeftOffset()    const;
        void SetAdvance( const vec2i& v );
        void SetBearing( const vec2i& v );
        void SetSize( const vec2i& v );
        void SetTopLeftOffset( const vec2i& v );
        void SetUV( u32 index, const vec2f& v );

        void SetChar( char ch );

    private :
        vec2i size;
        vec2i advance;
        vec2i bearing;
        vec2i topLeftOffset;
        char character;
        vec2f UV[4];
};

#endif // WATER_GLYPH
