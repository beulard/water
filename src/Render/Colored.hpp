#ifndef COLORED_HPP
#define COLORED_HPP
#include "Drawable.hpp"
#include "Color.hpp"
#include <GL/glew.h>
#include <vector>

enum ColoredType {
    CT_Line,
    CT_Triangle,
    CT_Quad
};

class Colored : public Drawable
{
    public:
        Colored();
        virtual ~Colored() = 0;

        virtual void SetColor( const Color& col );
        const Color& GetColor() const;

        const ColoredType& GetType() const;

    protected:
        Color color;
        ColoredType type;
};

#endif // COLORED_HPP
