#ifndef WATER_DRAWABLE
#define WATER_DRAWABLE
#include "vec2.hpp"

class Batch;

//  Those render modes are used to tell which shader should be used for which
//  drawable item. The custom mode allows a different shader for special effects
enum RenderMode {
    RM_WorldSprite,
    RM_UISprite,
    RM_WorldColored,
    RM_UIColored,
    RM_WorldText,
    RM_UIText,
    RM_Custom,
    RM_None
};

//	Different depths at which drawables can be rendered
//	The names are used just for reference ; anything can be drawn at any depth
//	It is in a decreasing order : the higher the value, the closer it is to the player
enum RenderDepth {
    //	UI layers
    RD_UITexts,
    RD_UISprites,
    //	Game texts may want to be in front of everything
    RD_Texts,
    //	Map layers
    RD_FrontLayer,
    RD_MiddleLayer,
    RD_BackLayer
};

class ShaderProgram;

//  Simple abstract class for all drawable and movable objects
//  It has to have a way to be drawn and to give its position
class Drawable
{
    public:
        Drawable();
        Drawable( const Drawable& other );
        virtual void operator=( const Drawable& other );
        virtual ~Drawable();

        virtual void Draw() = 0;
        virtual void StopDrawing() = 0;

        virtual void Move( const vec2i& offset );
        virtual void SetPosition( const vec2i& pos );
        virtual const vec2i& GetPosition() const;

        virtual void SetSize( const vec2ui& sz );
        virtual const vec2ui& GetSize() const;


        void SetShader( ShaderProgram* s );
        ShaderProgram* GetShader() const;

        RenderMode GetRenderMode()  const;
        virtual void SetRenderMode( RenderMode pMode );
        ShaderProgram* GetCustomShader()    const;
        virtual void SetCustomShader( ShaderProgram* shader );

        bool IsDrawn() const;
        int GetBatchID() const;
        void SetBatch( Batch* b );
        void SetBatchID( int i );

        void SetDepth( RenderDepth d );
        RenderDepth GetDepth()  const;

    protected :
        //  Size
        vec2ui size;
        //  Position
        vec2i position;
        //  Render mode
        RenderMode mode;
        //  Set by the renderer when the drawable is added according to its render mode
        ShaderProgram* shader;
        //  The custom shader is used in case mode is set to RM_Custom. Else it is NULL
        ShaderProgram* customShader;
        //	Depth of the rendered objects
        RenderDepth depth;
        //  Batch object where the drawable's address is held for rendering
        Batch* batch;
        //	ID for the renderer
        int batchID;
        //	True when Draw() was called
        bool drawn;
};

#endif // WATER_DRAWABLE
