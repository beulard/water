#ifndef WATER_MESH
#define WATER_MESH
#include <vector>
#include <cmath>
#include <GL/glew.h>
#include "vec2.hpp"

typedef unsigned int u32;

class Mesh
{
    public:
        Mesh();
        Mesh( const Mesh& m );
        virtual void operator=( const Mesh& m );
        virtual ~Mesh();

        //  The position given to this function must be coordinate having its origin in the top left corner of the window
        //  It will set the position of the first defined vertex and move the other vertices so that the mesh is not modified
        virtual void SetPosition( const vec2i& pos );

        //	Whereas this one function defines how the element is actually rendered to the screen
        virtual void Render() = 0;

        void SetVertex( u32 index, const vec2i& pos );
        //  Again, a positive y axis in this offset will move the mesh down
        virtual void Move( const vec2i& offset );
        //  Rotate the mesh around its first vertex by a clockwise angle in degrees
        virtual void Rotate( float angle );
        float GetRotation() const;


    protected:
        virtual void UpdateGLVertices();
        //  OpenGL id for the vertex buffer
        GLuint vbID;
        //	The array itself
        std::vector< float > vertices;

        float rotation;
        //  This flag is set when an operation is made on vertices
        bool needsUpdate;
};

#endif // WATER_MESH
