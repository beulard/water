#ifndef WATER_SPRITE
#define WATER_SPRITE
#include "Drawable.hpp"
#include "Rect.hpp"

class Texture;
class SpriteBatch;

class Sprite : public Drawable
{
    public:
        Sprite();
        Sprite( vec2i pos, vec2ui sz );
        Sprite( const Sprite& s );
        Sprite( Texture* tex, const Recti& rect );
        virtual ~Sprite();
        void operator=( const Sprite& s );

        void FlipX();
        void FlipY();

        virtual void Draw();
        virtual void StopDrawing();

        virtual void SetPosition( const vec2i& pos );
        virtual void Move( const vec2i& offset );
        virtual void SetSize( const vec2ui& sz );
        //	Rotate the sprite around its center x radians
        void Rotate( float x );
        //	Set the angle of the sprite
        void SetAngle( float x );
        void SetTexture( Texture* t );
        void SetSubRect( const Recti& rect );

        void AddToBatch();
        void RemoveFromBatch();


        const vec2i& GetPosition()	const;
        const Recti& GetSubRect()	const;
        float GetAngle() const;

        void SetBatchID( int i );

        void SetBatch( SpriteBatch* b );

	protected:
        Texture* texture;
        Recti subRect;

        float angle;
};

#endif // WATER_SPRITE
