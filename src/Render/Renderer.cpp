#include "Renderer.hpp"
#include <GL/glfw.h>
#include "SpriteBatch.hpp"
#include "Sprite.hpp"
#include "Quad.hpp"
#include "Common.hpp"

ShaderProgram* Renderer::worldSpriteShader = NULL;
ShaderProgram* Renderer::worldColoredShader = NULL;
ShaderProgram* Renderer::worldTextShader = NULL;
ShaderProgram* Renderer::UISpriteShader = NULL;
ShaderProgram* Renderer::UIColoredShader = NULL;
ShaderProgram* Renderer::UITextShader = NULL;

Renderer::Renderer(){
    //  We need to initialize the depths of elements (6 depth levels for now)
    for( u32 i = 0; i < 6; ++i ) {
        shapes.push_back( std::vector< Shape* >() );
        spriteBatches.byDepth.push_back( std::vector< SpriteBatch* >() );
        texts.push_back( std::vector< Text* >() );
        quadBatches.push_back( std::vector< QuadBatch* >() );
    }
}

Renderer::~Renderer(){

}

void Renderer::Init() {
    for( u32 i = 0; i < 6; ++i ) {
        quadBatches[i].push_back( new QuadBatch( Renderer::worldColoredShader, (RenderDepth)i ) );
        quadBatches[i].push_back( new QuadBatch( Renderer::UIColoredShader, (RenderDepth)i ) );
    }
}

void Renderer::Clean() {
    Info( "Cleaning renderer..." );

    Append( " Deallocating %d sprite batches...", spriteBatches.byName.size() );
    Info( " Deallocating %d quad batches...", quadBatches.size() * 2 );
    while( !spriteBatches.byDepth.empty() ) {
        while( !spriteBatches.byDepth.back().empty() ) {
            delete spriteBatches.byDepth.back().back();
            spriteBatches.byDepth.back().pop_back();
        }
        spriteBatches.byDepth.pop_back();
    }
    while( !quadBatches.empty() ) {
        while( !quadBatches.back().empty() ) {
            delete quadBatches.back().back();
            quadBatches.back().pop_back();
        }
        quadBatches.pop_back();
    }

    Append( " Done." );
}


void Renderer::Clear() {
    glClear( GL_COLOR_BUFFER_BIT );
}

void Renderer::Draw( Drawable& d ) {
    RenderDepth depth = d.GetDepth();
    switch( d.GetRenderMode() ) {
        case RM_WorldSprite :
        case RM_UISprite :
            dynamic_cast< Sprite* >( &d )->AddToBatch();
        break;
        case RM_WorldColored :
        case RM_UIColored :
            if( dynamic_cast< Colored* >(&d)->GetType() == CT_Quad ) {
                if( d.GetRenderMode() == RM_UIColored )
                    d.SetBatch( quadBatches[depth][1] );
                else
                    d.SetBatch( quadBatches[depth][0] );
                dcast<Quad*>(&d)->AddToBatch();
            }
            else {
                shapes[depth].push_back( dcast<Shape*>(&d) );
                d.SetBatchID( shapes[depth].size() - 1 );
            }
        break;
        case RM_WorldText :
        case RM_UIText :
            texts[depth].push_back( dcast<Text*>(&d) );
            d.SetBatchID( texts[depth].size() - 1 );
        break;
        case RM_Custom :
            //customs[depth].push_back( &d );
            //d.SetBatchID( customs[depth].size() - 1 );
            Warning( "Custom shaders not yet supported ! " );
        break;
        case RM_None :
            Error( "Trying to render a drawable with no render mode" );
        break;
        default :
            Error( "Trying to render a drawable with an invalid render mode" );
        break;
    }
}

void Renderer::StopDrawing( Drawable& d ) {
    int id = d.GetBatchID();
    if( id == -1 ) {
        Warning( "Trying to remove an invalid drawable from the renderer" );
        return;
    }
    RenderDepth depth = d.GetDepth();
    switch( d.GetRenderMode() ) {
        case RM_UISprite :
        case RM_WorldSprite :
            dynamic_cast< Sprite* >(&d)->RemoveFromBatch();
        break;
        case RM_UIColored :
        case RM_WorldColored :
            if( dcast<Colored*>(&d)->GetType() == CT_Quad )
                dcast<Quad*>(&d)->RemoveFromBatch();
            else {
                if( id >= (int)shapes[depth].size() )
                    Error( "Trying to remove an invalid shape from renderer (id = %d)", id );

                shapes[depth][id]->SetBatchID( -1 );
                if( id != (int)shapes[depth].size() - 1 ) {
                    if( shapes[depth].size() > 1 ) {
                        shapes[depth][id] = shapes[depth].back();
                        shapes[depth][id]->SetBatchID( id );
                    }
                }
                shapes[depth].pop_back();
            }
        break;

        case RM_UIText :
        case RM_WorldText :
            if( id >= (int)texts[depth].size() ) {
                Error( "Trying to remove an invalid text from renderer (id = %d)", id );
                return;
            }

            texts[depth][id]->SetBatchID( -1 );
            if( id != (int)texts[depth].size() - 1 ) {
                if( texts[depth].size() > 1 ) {
                    texts[depth][id] = texts[depth].back();
                    texts[depth][id]->SetBatchID( id );
                }
            }
            texts[depth].pop_back();
        break;
        case RM_Custom : {
            /*u32 id = d.GetBatchID();
            if( id >= customs[depth].size() ) {
                Error( "Trying to remove an invalid drawable from renderer (id = %d)", id );
                return;
            }

            customs[depth][id]->SetBatchID( -1 );
            if( id == customs[depth].size() - 1 )
                customs[depth][id] = NULL;
            else {
                if( customs[depth].size() > 1 ) {
                    customs[depth][id] = customs[depth].back();
                    customs[depth][id]->SetBatchID( id );
                    customs[depth].back() = NULL;
                }
            }
            customs[depth].pop_back();*/
            Warning( "Custom shaders not yet supported !" );
        } break;
        case RM_None :
            Error( "Trying to remove a drawable from the renderer that has no render mode" );
        break;
        default :
            Error( "Trying to remove a drawable from the renderer that has an invalid render mode" );
        break;
    }
}

void Renderer::Render() {
    for( int i = 5; i >= 0; --i ) {
        for( u32 j = 0; j < texts[i].size(); ++j ) {
            texts[i][j]->Render();
        }
        for( u32 j = 0; j < 2; ++j ) {
            quadBatches[i][j]->Render();
        }
        for( u32 j = 0; j < spriteBatches.byDepth[i].size(); ++j ) {
            spriteBatches.byDepth[i][j]->Render();
        }
        for( u32 j = 0; j < shapes[i].size(); ++j ) {
            shapes[i][j]->Render();
        }
    }
    glfwSwapBuffers();
}

void Renderer::AddSpriteBatch( const std::string& name, SpriteBatch* batch ) {
    spriteBatches.byName.insert( std::pair< Wash, SpriteBatch* >( Wash( name ), batch ) );
    spriteBatches.byDepth[batch->GetDepth()].push_back( batch );
}

SpriteBatch* Renderer::GetSpriteBatch( const std::string& name ) {
    return spriteBatches.byName[Wash( name )];
}
