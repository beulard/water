#ifndef WATER_SPRITEBATCH
#define WATER_SPRITEBATCH
#include "Batch.hpp"
#include "Rect.hpp"

class Texture;

//	Sprite batches deal with sprites that use the same texture
class SpriteBatch : public Batch
{
    public:
        SpriteBatch();
		SpriteBatch( const SpriteBatch& other );
		void operator=( const SpriteBatch& other );
		SpriteBatch( Texture* tex, ShaderProgram* s, RenderDepth d );
        ~SpriteBatch();

		virtual void AddDrawable( Drawable& d );
		virtual void RemoveDrawable( int id );

        void SetTexture( Texture* tex );
		void RotateSprite( int id, float angle );
        void SetSpriteSubRect( int id, const Recti& subRect );

		virtual void Update();
		virtual void Render();


	private:
        Texture* texture;

		//	A second vector, to hold texture coordinates
		std::vector< float > UVs;

		//	OpenGL id for the texture coordinates buffer
		GLuint uvID;

};

#endif // WATER_SPRITEBATCH
