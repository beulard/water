#ifndef WATER_LINE
#define WATER_LINE
#include "Shape.hpp"
#include "Mesh.hpp"

class Line : public Shape
{
    public:
        Line();
        Line( const vec2i& start, const vec2i& end, const Color& pColor, float width );
        ~Line();

        void SetStart( const vec2i& pos );
        void SetEnd( const vec2i& pos );
        void SetWidth( float pWidth );
        virtual void Draw();
        virtual void StopDrawing();
        virtual void Render();

    private:
        float width;
};

#endif // WATER_LINE
