#include "Sprite.hpp"
#include "Renderer.hpp"
#include "Texture.hpp"
#include "SpriteBatch.hpp"

Sprite::Sprite() : texture( NULL ), angle( 0 ) {
    SetRenderMode( RM_WorldSprite );
	size = vec2i( 200, 200 );
}

Sprite::Sprite( vec2i pos, vec2ui sz ) : texture( NULL ), angle(0) {
    size = sz;
    position = pos;
    SetRenderMode( RM_WorldSprite );
}

Sprite::Sprite( Texture* tex, const Recti& rect ) : angle(0) {

    SetTexture( tex );
    size = vec2ui( rect.GetBottomRight() - rect.GetTopLeft() );
    SetSubRect( rect );
    SetRenderMode( RM_WorldSprite );

}

Sprite::Sprite( const Sprite& s ) : Drawable( s ) {
    *this = s;
}

void Sprite::operator=( const Sprite& s ) {
    if( drawn && batch )
        StopDrawing();
    batch = s.batch;
    if( s.texture )
        SetTexture( s.texture );
    SetSubRect( s.subRect );
    position = s.position;
    size = s.size;
    angle = s.angle;
}

Sprite::~Sprite(){
    if( drawn && batch )
        StopDrawing();
}

void Sprite::SetTexture( Texture* t ) {
    texture = t;
    if( texture ) {
        subRect.SetTopLeft( vec2i( 0, 0 ) );
        subRect.SetBottomRight( vec2i( texture->GetSize().x, texture->GetSize().y ) );
        size = vec2ui(texture->GetSize());

        if( batch == NULL )
            batch = Global::renderer->GetSpriteBatch( t->GetName() );
    }
    SetSubRect( subRect );
}

void Sprite::FlipX() {
    vec2i topLeft = subRect.GetTopLeft();
    vec2i bottomRight = subRect.GetBottomRight();
    subRect.SetTopLeft( vec2i( bottomRight.x, topLeft.y ) );
    subRect.SetBottomRight( vec2i( topLeft.x, bottomRight.y ) );
    SetSubRect( subRect );
}

void Sprite::FlipY() {
    vec2i topLeft = subRect.GetTopLeft();
    vec2i bottomRight = subRect.GetBottomRight();
    subRect.SetTopLeft( vec2i( topLeft.x, bottomRight.y ) );
    subRect.SetBottomRight( vec2i( bottomRight.x, topLeft.y ) );
    SetSubRect( subRect );
}

void Sprite::SetSubRect( const Recti& rect ) {
    subRect = rect;
    if( batchID != -1 )
		dcast<SpriteBatch*>(batch)->SetSpriteSubRect( batchID, subRect );
}

void Sprite::SetBatchID( int i ) {
    batchID = i;
}

void Sprite::SetBatch( SpriteBatch* b ) {
    batch = b;
}

const vec2i& Sprite::GetPosition() const{
    return position;
}

const Recti& Sprite::GetSubRect() const {
    return subRect;
}

void Sprite::Draw() {
    if( !drawn )
		AddToBatch();
    else
        Warning( "Trying to add a sprite to the renderer twice" );
    drawn = true;
}

void Sprite::StopDrawing() {
	if( drawn )
		RemoveFromBatch();
	else
        Warning( "Trying to remove a sprite from the renderer but it isn't in" );
    drawn = false;
}

void Sprite::AddToBatch() {
    batch->AddDrawable( *this );
}

void Sprite::RemoveFromBatch() {
    batch->RemoveDrawable( batchID );
}

void Sprite::SetPosition( const vec2i& pos ) {
    position = pos;
    if( batchID != -1 )
        batch->SetDrawablePosition( batchID, position );
}

void Sprite::Move( const vec2i& offset ) {
    position += offset;
    if( batchID != -1 )
        batch->MoveDrawable( batchID, offset );
}

void Sprite::SetSize( const vec2ui& sz ) {
    size = sz;
    if( batchID != -1 )
        batch->SetDrawableSize( batchID, size );
}

void Sprite::Rotate( float x ) {
	angle += x;
    if( batchID != -1 )
		dcast<SpriteBatch*>(batch)->RotateSprite( batchID, x );
}

void Sprite::SetAngle( float x ) {
	if( angle != 0 )
		if( batchID != -1 )
			dcast<SpriteBatch*>(batch)->RotateSprite( batchID, -angle );
	if( batchID != - 1 )
		dcast<SpriteBatch*>(batch)->RotateSprite( batchID, x );
	angle = x;
}

float Sprite::GetAngle() const {
    return angle;
}
