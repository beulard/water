#ifndef WATER_OUTLINE
#define WATER_OUTLINE
#include "Line.hpp"

class Outline : public Drawable
{
    public:
        Outline();
        //  We can create an outline simply from 4 ints because the outline is basically a quad
        Outline( int left, int top, int right, int bottom, const Color& pColor = Color::Black, int pWidth = 2 );
        ~Outline();

        void SetLeft( int pos );
        void SetTop( int pos );
        void SetRight( int pos );
        void SetBottom( int pos );

        virtual void SetSize( const vec2ui& sz );

        void SetWidth( int width );
        void SetColor( const Color& color );

        virtual void Draw();
        virtual void StopDrawing();
        virtual void Render();

        virtual void SetPosition( const vec2i& pos );
        virtual void Move( const vec2i& offset );
        virtual const vec2i& GetPosition()   const;


    private:
        Line left;
        Line top;
        Line right;
        Line bottom;
        int width;
};

#endif // WATER_OUTLINE
