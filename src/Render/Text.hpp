#ifndef WATER_TEXT
#define WATER_TEXT
#include <vector>
#include "Glyph.hpp"
#include "Colored.hpp"

class Text : public Colored, public Mesh
{
    public:
        Text();
        Text( const Text& t );
        virtual void operator=( const Text& t );
        Text( const std::string& s, Font* f, const Color& col = Color::White );
        virtual ~Text();

        virtual void Draw();
        virtual void StopDrawing();
        virtual void Render();
        virtual void Move( const vec2i& offset );
        virtual void SetPosition( const vec2i& pos );

        void AddChar( char c );

        void SetString( const std::string& s );
        const std::string& GetString()  const;
        void Clear();

        void SetFont( Font* f );

    private:
        virtual void UpdateGLVertices();

		//	The text needs an OpenGL buffer for texture coordinates
		GLuint uvID;
		//	The array itself
		std::vector< float > UVs;

        Font* font;
        //  origin is defined by the first letter to be added : it represents where the text will be aligned ( in OpenGL coordinates )
        vec2i origin;
        //  cursor is used when a character is added to tell where it will be placed ( in OpenGL coordinates )
        vec2i cursor;
        //  Height of a line
        u32 lineHeight;
        //  Number of '\n' characters in the whole string
        u32 nbrLines;
        //  We need to keep this so we are able to update it when the font is changed
        std::string text;
};

#endif // WATER_TEXT
