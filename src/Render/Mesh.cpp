#include "Mesh.hpp"
#include "Common.hpp"

Mesh::Mesh() : vbID(0), rotation(0.f), needsUpdate( true ) {
    glGenBuffers( 1, &vbID );
    if( vbID == 0 )
        Error( "Couldn't create a GL vertex buffer" );
}

Mesh::Mesh( const Mesh& m ) : vbID(0), rotation(0.f) {
    glGenBuffers( 1, &vbID );
    if( vbID == 0 )
        Error( "Couldn't create a GL vertex buffer" );
    *this = m;
}

void Mesh::operator=( const Mesh& m ) {
    rotation = m.rotation;
    vertices = m.vertices;
    needsUpdate = true;
}

Mesh::~Mesh() {
    glDeleteBuffers( 1, &vbID );
}

void Mesh::Move( const vec2i& offset ) {
    for( u32 i = 0; i < vertices.size(); i += 2 ) {
        vertices[i] += offset.x;
        //	Make the opposite change for the Y axis as we need to think in OpenGL coordinates
        vertices[i + 1] -= offset.y;
    }
    needsUpdate = true;
}

void Mesh::SetPosition( const vec2i& pos ) {
    if( !vertices.empty() ) {
        vec2i offset( pos.x - vertices[0], (Global::winSz->y - pos.y) - vertices[1] );
        Move( offset );
    }
}

void Mesh::Rotate( float angle ) {
    rotation = angle;
    //	We make a rotation around the first vertex (generally top left)
    vec2i center(vertices[0], vertices[1]);
    double matrix[4] = { cos( angle ), -sin( angle ),
                        sin( angle ), cos( angle ) };
    for( u32 i = 0; i < vertices.size(); i += 2 ) {
        vec2i edge = vec2i(vertices[i], vertices[i + 1]) - center;
        vec2i rot = vec2i( edge.x * matrix[0] + edge.y * matrix[1], edge.x * matrix[2] + edge.y * matrix[3] );
        vertices[i] = center.x + rot.x;
        vertices[i + 1] = Global::winSz->y - ( center.y + rot.y );
    }
    needsUpdate = true;
}

float Mesh::GetRotation() const {
    return rotation;
}

void Mesh::SetVertex( u32 index, const vec2i& pos ) {
    if( index + 1 >= vertices.size() )
        Error( "Trying to set a non existing vertex's position !" );
    vertices[index] = pos.x;
    //	Invert the coordinate in the Y axis for OpenGL
    vertices[index + 1] = Global::winSz->y - pos.y;
    needsUpdate = true;
}

void Mesh::UpdateGLVertices() {
    glBindBuffer( GL_ARRAY_BUFFER, vbID );
    glBufferData( GL_ARRAY_BUFFER, vertices.size() * sizeof( float ), &vertices[0], GL_DYNAMIC_DRAW );
    needsUpdate = false;
}


