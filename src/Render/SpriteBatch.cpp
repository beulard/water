#include "SpriteBatch.hpp"
#include "Texture.hpp"
#include "Sprite.hpp"
#include "Camera.hpp"

SpriteBatch::SpriteBatch() {
    glGenBuffers( 1, &uvID );
    if( uvID == 0 )
        Error( "Couldn't create a GL vertex buffer" );
}

SpriteBatch::SpriteBatch( const SpriteBatch& other ) : Batch(other) {
    glGenBuffers( 1, &uvID );
    if( uvID == 0 )
        Error( "Couldn't create a GL vertex buffer" );
    *this = other;
}

void SpriteBatch::operator=( const SpriteBatch& other ) {
    texture = other.texture;
    UVs = other.UVs;
}

SpriteBatch::SpriteBatch(Texture* tex, ShaderProgram* s , RenderDepth d) : Batch( s, d ), texture( tex ) {
    glGenBuffers( 1, &uvID );
    if( uvID == 0 )
        Error( "Couldn't create a GL vertex buffer" );
}

SpriteBatch::~SpriteBatch() {
    glDeleteBuffers( 1, &uvID );
}

void SpriteBatch::AddDrawable( Drawable& d ) {
    //	Add 4 vertices and 4 texture coordinates to our array
    for( u32 i = 0; i < 8; ++i ) {
        vertices.push_back( 0.f );
        UVs.push_back( 0.f );
    }
    drawables.push_back( &d );
    drawables.back()->SetBatchID( drawables.size() - 1 );
    drawables.back()->SetBatch( this );

    SetDrawablePosition( drawables.size() - 1, d.GetPosition() );
    SetDrawableSize( drawables.size() - 1, d.GetSize() );

    Sprite* s = dynamic_cast< Sprite* >(&d);
    SetSpriteSubRect( drawables.size() - 1, s->GetSubRect() );
    //RotateSprite( drawables.size() - 1, s->GetAngle() );
    needsUpdate = true;
}

void SpriteBatch::RemoveDrawable( int id ) {
    if( (u32)id >= drawables.size() )
        Error( "Trying to remove an invalid Sprite (id = %d)", id );

    drawables[id]->SetBatchID( -1 );
    if( (u32)id != drawables.size() - 1 ) {
        if( drawables.size() > 1 ) {
            drawables[id] = drawables.back();
            drawables[id]->SetBatch( this );
            drawables[id]->SetBatchID( id );

            u32 sz = vertices.size();
            for( u32 i = 0; i < 8; ++i ) {
                vertices[id * 8 + 7 - i] = vertices[sz - 1 - i];
                UVs[id * 8 + 7 - i] = UVs[sz - 1 - i];
                vertices.pop_back();
                UVs.pop_back();
            }
        }
    }
    else {
        for( u32 i = 0; i < 8; ++i ) {
            vertices.pop_back();
            UVs.pop_back();
        }
    }

    drawables.pop_back();
    needsUpdate = true;
}

void SpriteBatch::SetTexture( Texture* tex ) {
    texture = tex;
}

void SpriteBatch::RotateSprite( int id, float angle ) {
    if( (u32)id >= drawables.size() )
        Error( "Trying to access an invalid Drawable's angle (id = %d)", id );

    double mat[4] = {   cos( angle ), -sin( angle ),
                        sin( angle ), cos( angle ) };

    vec2i v[4];
    for( u32 i = 0; i < 8; i += 2 )
        v[i] = vec2i( id * 8 + i, id * 8 + i + 1 );

    vec2i center = v[0] + (v[2] - v[0]) / 2;

    for( u32 i = 0; i < 8; i += 2 ) {
        vec2i vertex = v[i] - center;
        vec2i newVertex = center + vec2i( vertex.x * mat[0] + vertex.y * mat[1], vertex.x * mat[2] + vertex.y * mat[3] );
        vertices[id * 8 + i] = newVertex.x;
        vertices[id * 8 + i + 1] = Global::winSz->y	- newVertex.y;
    }
    needsUpdate = true;
}

void SpriteBatch::SetSpriteSubRect( int id, const Recti& subRect ) {
    if( (u32)id >= drawables.size() )
        Error( "Trying to access an invalid sprite's attributes (UV) (id = %d)", id );

    if( texture ) {
        //	/!\ We need to convert the texture coordinates to OpenGL coordinates, which means the Y axis is inverted and it is normalized
        vec2f topRight = (vec2f)subRect.GetTopLeft() / (vec2f)texture->GetSize();
        vec2f bottomLeft = (vec2f)subRect.GetBottomRight() / (vec2f)texture->GetSize();
        Rectf uv( topRight, bottomLeft );
        UVs[id * 8] = uv.GetBottomLeft().x;
        UVs[id * 8 + 1] = uv.GetBottomLeft().y;
        UVs[id * 8 + 2] = uv.GetBottomRight().x;
        UVs[id * 8 + 3] = uv.GetBottomRight().y;
        UVs[id * 8 + 4] = uv.GetTopRight().x;
        UVs[id * 8 + 5] = uv.GetTopRight().y;
        UVs[id * 8 + 6] = uv.GetTopLeft().x;
        UVs[id * 8 + 7] = uv.GetTopLeft().y;
    }
    else
        Error( "Need to set a texture before trying to set a subrect" );
    needsUpdate = true;
}

void SpriteBatch::Update() {
    glBindBuffer( GL_ARRAY_BUFFER, vbID );
    glBufferData( GL_ARRAY_BUFFER, vertices.size() * sizeof(float), &vertices[0], GL_DYNAMIC_DRAW );
    glBindBuffer( GL_ARRAY_BUFFER, uvID );
    glBufferData( GL_ARRAY_BUFFER, UVs.size() * sizeof(float), &UVs[0], GL_DYNAMIC_DRAW );
    needsUpdate = false;
}

void SpriteBatch::Render() {
    if( drawables.size() > 0 ) {
        if( needsUpdate )
            Update();

        shader->Use();

        if( texture != NULL ) {
            glActiveTexture( GL_TEXTURE0 );
            glBindTexture( GL_TEXTURE_2D, texture->GetID() );
        }
        else
            Error( "Trying to draw a sprite batch with no texture !" );

        //  Send the projection matrix to the shader
        int index = glGetUniformLocation( shader->GetID(), "projMat" );
        if( index == -1 )
            Error( "No uniform 'projMat' in shader" );
        glUniformMatrix3fv( index, 1, GL_FALSE, Global::projMatrix );

        if( shader->UsesCamera() ) {
            index = glGetUniformLocation( shader->GetID(), "camPos" );
            if( index == -1 )
                Error( "No uniform 'camPos' in shader" );
            glUniform2f( index, Global::camera->GetGLPosition().x, Global::winSz->y - Global::camera->GetGLPosition().y );
        }

        //  Send the vertex and texture coordinates attributes to the shader
        glEnableVertexAttribArray(0);
        glBindBuffer( GL_ARRAY_BUFFER, vbID );
        glVertexAttribPointer( 0, 2, GL_FLOAT, GL_FALSE, 0, 0 );
        glEnableVertexAttribArray(1);
        glBindBuffer( GL_ARRAY_BUFFER, uvID );
        glVertexAttribPointer( 1, 2, GL_FLOAT, GL_FALSE, 0, 0 );

        glDrawArrays( GL_QUADS, 0, drawables.size() * 4 );

        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
    }
}


