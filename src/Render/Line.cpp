#include "Line.hpp"
#include "Renderer.hpp"
#include "Camera.hpp"
#include "Common.hpp"

Line::Line() : width(1) {
    vertices.resize( 4 );
    SetVertex( 0, vec2i( 0, 0 ) );
    SetVertex( 1, vec2i( 10, 10 ) );
    size.x = vertices[2] - vertices[0];
    size.y = vertices[3] - vertices[1];
    SetRenderMode( RM_WorldColored );
    color = Color::Black;
    needsUpdate = true;
    type = CT_Line;
}

Line::Line( const vec2i& start, const vec2i& end, const Color& pColor, float pWidth ) : width( pWidth ) {
    vertices.resize( 4 );
    SetVertex( 0, start );
    SetVertex( 1, end );
    color = pColor;
    SetRenderMode( RM_WorldColored );
    type = CT_Line;
    needsUpdate = true;
}

Line::~Line(){
    if( drawn )
        StopDrawing();
}

void Line::SetStart( const vec2i& pos ) {
    SetVertex( 0, pos );
    size.x = vertices[2] - vertices[0];
    size.y = vertices[3] - vertices[1];
    needsUpdate = true;
}

void Line::SetEnd( const vec2i& pos ) {
    SetVertex( 1, pos );
    size.x = vertices[2] - vertices[0];
    size.y = vertices[3] - vertices[1];
    needsUpdate = true;
}

void Line::SetWidth( float pWidth ) {
    width = pWidth;
}

void Line::Draw() {
    if( !drawn )
        Global::renderer->Draw( *this );

    else
        Warning( "Trying to add a line to render twice (id = %d)", batchID );
    drawn = true;
}

void Line::StopDrawing() {
    if( drawn )
        Global::renderer->StopDrawing( *this );
    else
        Warning( "Trying to remove a line from the renderer but it isn't in" );
    drawn = false;
}

void Line::Render() {
    if( needsUpdate )
        UpdateGLVertices();

    shader->Use();


    //  Send the projection matrix to the shader
    int index = glGetUniformLocation( shader->GetID(), "projMat" );
    if( index == -1 )
        Error( "No uniform 'projMat' in shader !" );
    glUniformMatrix3fv( index, 1, GL_FALSE, Global::projMatrix );

    if( mode == RM_WorldColored ) {
        index = glGetUniformLocation( shader->GetID(), "camPos" );
        if( index == -1 )
            Error( "No uniform 'camPos' in shader" );
        glUniform2f( index, Global::camera->GetGLPosition().x, Global::winSz->y - Global::camera->GetGLPosition().y );
    }

    //  Send the vertex and color attributes to the shader
    glEnableVertexAttribArray(0);
    glBindBuffer( GL_ARRAY_BUFFER, vbID );
    glVertexAttribPointer( 0, 2, GL_FLOAT, GL_FALSE, 0, 0 );
    glEnableVertexAttribArray(1);
    glBindBuffer( GL_ARRAY_BUFFER, cbID );
    glVertexAttribPointer( 1, 4, GL_FLOAT, GL_FALSE, 0, 0 );

    glLineWidth( width );
    glDrawArrays( GL_LINES, 0, 2 );

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
}

