#include "Glyph.hpp"
#include "Common.hpp"

Glyph::Glyph() : size( vec2i( 0, 0 ) ), advance( vec2i( 0, 0 ) ), character( 'a' ) {

}

Glyph::~Glyph(){

}

void Glyph::SetChar( char ch ) {
    character = ch;
}

void Glyph::SetAdvance( const vec2i& v ) {
    advance = v;
}

void Glyph::SetBearing( const vec2i& v ) {
    bearing = v;
}

void Glyph::SetSize( const vec2i& v ) {
    size = v;
}

void Glyph::SetUV( u32 index, const vec2f& v ) {
    if( index > 3 )
        Error( "Trying to write to a non-existing UV vec2f in a Glyph !" );
    UV[index] = v;
}

void Glyph::SetTopLeftOffset( const vec2i& v ) {
    topLeftOffset = v;
}

const vec2i& Glyph::GetAdvance()     const{
    return advance;
}

const vec2i& Glyph::GetBearing()     const {
    return bearing;
}

const vec2i& Glyph::GetSize()   const {
    return size;
}

const vec2f& Glyph::GetUV( u32 index )  const {
    if( index > 3 )
        Error( "Trying to read a non-existing UV vec2f in a Glyph !" );
    return UV[index];
}

const vec2i& Glyph::GetTopLeftOffset()  const {
    return topLeftOffset;
}
