#ifndef BATCH_HPP
#define BATCH_HPP
#include <vector>
#include "ShaderProgram.hpp"
#include "Drawable.hpp"

//	A batch holds a pointer to every drawable that has
//	the same properties and
class Batch
{
    public:
        Batch();
		Batch( const Batch& other );
		void operator=( const Batch& other );
		Batch( ShaderProgram* s, RenderDepth depth );
        virtual ~Batch();

        virtual void AddDrawable( Drawable& d );
		virtual void RemoveDrawable( int id );

        void SetShader( ShaderProgram* s );

		void SetDrawablePosition( int id, const vec2i& position );
		void MoveDrawable( int id, const vec2i& offset );
		void SetDrawableSize( int id, const vec2ui& size );

		void SetDepth( RenderDepth d );
		RenderDepth GetDepth()  const;

		virtual void Update();
		virtual void Render();

    protected:
        ShaderProgram* shader;
		// Contains vertex coordinates
		std::vector< float > vertices;

		//	OpenGL id for the vertex buffer
		GLuint vbID;

        std::vector< Drawable* > drawables;
        bool needsUpdate;

        //  Depth given by the data loader when loading textures (1 texture has 1 depth)
		RenderDepth depth;
};

#endif // BATCH_HPP
