#ifndef WATER_SHAPE
#define WATER_SHAPE
#include "Colored.hpp"
#include "Mesh.hpp"

class Shape : public Colored, public Mesh
{
    public:
        Shape();
        virtual ~Shape();

        virtual void SetColor( const Color& col );

        virtual void SetPosition( const vec2i& pos );
        virtual void Move( const vec2i& offset );

        virtual void Draw() = 0;
        virtual void StopDrawing() = 0;
        virtual void Render() = 0;

    protected:
        void UpdateGLVertices();
        //  OpenGL id for the color buffer
        GLuint cbID;
        //  The color array
        std::vector< float > glColor;
};

#endif // WATER_SHAPE
