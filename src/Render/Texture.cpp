#include "Texture.hpp"

Texture::Texture() : id(0), type( CT_RGB ) {

}

Texture::~Texture(){
    if( id != 0 )
        glDeleteTextures( 1, &id );
}

GLuint Texture::GetID() const {
    return id;
}

void Texture::SetColorType( ColorType t ) {
    type = t;
}

void Texture::SetSize( const vec2i& sz ) {
    size = sz;
}

const vec2i& Texture::GetSize() const {
    return size;
}

const ColorType& Texture::GetColorType() const {
    return type;
}

void Texture::SetName( const std::string& n ) {
	name = n;
}

const ColorType& Texture::GetType() const {
    return type;
}

const std::string& Texture::GetName() {
	return name;
}

void Texture::SetID( GLuint i ) {
    id = i;
}
