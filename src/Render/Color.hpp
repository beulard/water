#ifndef WATER_COLOR
#define WATER_COLOR

typedef unsigned char u8;

class Color
{
    public:
        //  The default color is white
        Color();
        Color( u8 pR, u8 pG, u8 pB, u8 pA );
        Color( u8 pR, u8 pG, u8 pB );
        ~Color();
        u8 r, g, b, a;

        static Color White;
        static Color Green;
        static Color Blue;
        static Color Red;
        static Color Yellow;
        static Color Teal;
        static Color Purple;
        static Color Black;
};

#endif // WATER_COLOR
