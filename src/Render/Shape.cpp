#include "Shape.hpp"
#include "Common.hpp"

Shape::Shape(){
    SetRenderMode( RM_UIColored );
    glGenBuffers( 1, &cbID );
    if( cbID == 0 )
        Error( "Couldn't create a GL vertex buffer" );
    glColor.resize(4);
}

Shape::~Shape(){

}

void Shape::SetColor( const Color& col ) {
    color = col;
    glColor[0] = col.r / 255.f;
    glColor[1] = col.g / 255.f;
    glColor[2] = col.b / 255.f;
    glColor[3] = col.a / 255.f;
}

void Shape::SetPosition( const vec2i& pos ) {
    Mesh::SetPosition( pos );
    Drawable::SetPosition( pos );
}

void Shape::Move( const vec2i& offset ) {
    Mesh::Move( offset );
    Drawable::Move( offset );
}

void Shape::UpdateGLVertices() {
    glBindBuffer( GL_ARRAY_BUFFER, cbID );
    glBufferData( GL_ARRAY_BUFFER, 4 * sizeof( float ), &glColor[0], GL_DYNAMIC_DRAW );
    Mesh::UpdateGLVertices();
}

