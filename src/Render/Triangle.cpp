#include "Triangle.hpp"
#include "Renderer.hpp"
#include "Camera.hpp"
#include "Common.hpp"

Triangle::Triangle(){
    vertices.resize( 6 );
    SetVertex( 0, vec2i( 20, 0 ) );
    SetVertex( 1, vec2i( 0, 20 ) );
    SetVertex( 2, vec2i( 40, 20 ) );
    SetRenderMode( RM_WorldColored );
    type = CT_Triangle;
}

Triangle::Triangle( const vec2i& v0, const vec2i& v1, const vec2i& v2 ) {
    vertices.resize( 6 );
    SetVertex( 0, v0 );
    SetVertex( 1, v1 );
    SetVertex( 2, v2 );
    SetRenderMode( RM_WorldColored );
    type = CT_Triangle;
}

Triangle::~Triangle(){
    if( drawn )
        StopDrawing();
}

void Triangle::Draw() {
    if( !drawn )
        Global::renderer->Draw( *this );
    else
        Warning( "Trying to add a triangle to the renderer twice" );
    drawn = true;
}

void Triangle::StopDrawing() {
    if( drawn )
        Global::renderer->StopDrawing( *this );
    else
        Warning( "Trying to remove a triangle from renderer but it is not in" );
    drawn = false;
}

void Triangle::Render() {
  if( needsUpdate )
        UpdateGLVertices();

    shader->Use();


    //  Send the projection matrix to the shader
    int index = glGetUniformLocation( shader->GetID(), "projMat" );
    if( index == -1 )
        Error( "No uniform 'projMat' in shader !" );
    glUniformMatrix3fv( index, 1, GL_FALSE, Global::projMatrix );

    if( mode == RM_WorldColored ) {
        index = glGetUniformLocation( shader->GetID(), "camPos" );
        if( index == -1 )
            Error( "No uniform 'camPos' in shader" );
        glUniform2f( index, Global::camera->GetGLPosition().x, Global::winSz->y - Global::camera->GetGLPosition().y );
    }

    index = glGetUniformLocation( shader->GetID(), "color" );
    if( index == -1 )
        Error( "No uniform 'color' in shader !" );
    glUniform4f( index, color.r / 255.f, color.g / 255.f, color.b / 255.f, color.a / 255.f );

    //  Send the vertex and color attributes to the shader
    glEnableVertexAttribArray(0);
    glBindBuffer( GL_ARRAY_BUFFER, vbID );
    glVertexAttribPointer( 0, 2, GL_FLOAT, GL_FALSE, 0, 0 );
    glEnableVertexAttribArray(1);
    glBindBuffer( GL_ARRAY_BUFFER, cbID );
    glVertexAttribPointer( 1, 4, GL_FLOAT, GL_FALSE, 0, 0 );

    glDrawArrays( GL_TRIANGLES, 0, 3 );

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
}
