#include "QuadBatch.hpp"
#include "Quad.hpp"
#include "Camera.hpp"
#include "Common.hpp"

QuadBatch::QuadBatch() : cbID(0) {
    glGenBuffers( 1, &cbID );
    if( cbID == 0 )
        Error( "Couldn't create a GL vertex buffer" );
}

QuadBatch::QuadBatch( const QuadBatch& other ) : Batch(other) {
    glGenBuffers( 1, &cbID );
    if( cbID == 0 )
        Error( "Couldn't create a GL vertex buffer" );
    *this = other;
}

void QuadBatch::operator=( const QuadBatch& other ) {
    colors = other.colors;
}

QuadBatch::QuadBatch( ShaderProgram* s, RenderDepth d ) : Batch(s, d), cbID(0) {
    glGenBuffers( 1, &cbID );
    if( cbID == 0 )
        Error( "Couldn't create a GL vertex buffer" );
}

QuadBatch::~QuadBatch() {
    glDeleteBuffers( 1, &cbID );
}

void QuadBatch::AddDrawable( Drawable& d ) {
    //	Add 4 vertices and 2 colors to our array
    for( u32 i = 0; i < 8; ++i ) {
        vertices.push_back( 0.f );
        colors.push_back( 0.f );
    }
    //	Add the remaining 2 colors (just doing some cpu economy)
    for( u32 i = 0; i < 8; ++i )
        colors.push_back( 0.f );

    drawables.push_back( &d );
    drawables.back()->SetBatchID( drawables.size() - 1 );
    drawables.back()->SetBatch( this );

    SetDrawablePosition( drawables.size() - 1, d.GetPosition() );
    SetDrawableSize( drawables.size() - 1, d.GetSize() );

    Quad* q = dynamic_cast< Quad* >(&d);
    SetQuadColor( drawables.size() - 1, q->GetColor() );
    needsUpdate = true;
}

void QuadBatch::RemoveDrawable( int id ) {
    if( (u32)id >= drawables.size() )
        Error( "Trying to remove an invalid Quad (id = %d)", id );

    drawables[id]->SetBatchID( -1 );
    if( (u32)id != drawables.size() - 1 ) {
        if( drawables.size() > 1 ) {
            drawables[id] = drawables.back();
            drawables[id]->SetBatch( this );
            drawables[id]->SetBatchID( id );

            //	This basically replaces the values of the deleted quad with those of the last
            //	quad in our array
            u32 vsz = vertices.size();
            u32 csz = colors.size();
            for( u32 i = 0; i < 8; ++i ) {
                vertices[id * 8 + 7 - i] = vertices[vsz - 1 - i];
                colors[id * 16 + 15 - i] = colors[csz - 1 - i];
                vertices.pop_back();
                colors.pop_back();
            }

            //	We only replaced 4 vertices and 2 colors, so we need to replace the 2
            //	remaining colors (8 values)
            csz = colors.size();
            for( u32 i = 0;	i < 8; ++i ) {
                colors[id * 16 + 7 - i] = colors[csz - 1 - i];
                colors.pop_back();
            }
        }
    }
    else {
        for( u32 i = 0; i < 8; ++i ) {
            vertices.pop_back();
            colors.pop_back();
        }
        for( u32 i = 0; i < 8; ++i )
            colors.pop_back();
    }

    drawables.pop_back();
    needsUpdate = true;
}

void QuadBatch::SetQuadColor( int id, const Color& col ) {
    if( (u32)id >= drawables.size() )
        Error( "Trying to access an invalid quad's color (id = %d)", id );
    for( u32 i = 0; i < 4; ++i ) {
        colors[id * 16 + i * 4] = col.r / 255.f;
        colors[id * 16 + 1 + i * 4] = col.g / 255.f;
        colors[id * 16 + 2 + i * 4] = col.b / 255.f;
        colors[id * 16 + 3 + i * 4] = col.a / 255.f;
    }
    needsUpdate = true;
}

void QuadBatch::Update() {
    glBindBuffer( GL_ARRAY_BUFFER, vbID );
    glBufferData( GL_ARRAY_BUFFER, vertices.size() * sizeof( float ), &vertices[0], GL_DYNAMIC_DRAW );
    glBindBuffer( GL_ARRAY_BUFFER, cbID );
    glBufferData( GL_ARRAY_BUFFER, colors.size() * sizeof( float ), &colors[0], GL_DYNAMIC_DRAW );
    needsUpdate = false;
}

void QuadBatch::Render() {
    if( drawables.size() > 0 ) {
        if( needsUpdate )
            Update();

        shader->Use();

        //  Send the projection matrix to the shader
        int index = glGetUniformLocation( shader->GetID(), "projMat" );
        if( index == -1 )
            Error( "No uniform 'projMat' in shader" );
        glUniformMatrix3fv( index, 1, GL_FALSE, Global::projMatrix );

        if( shader->UsesCamera() ) {
            index = glGetUniformLocation( shader->GetID(), "camPos" );
            if( index == -1 )
                Error( "No uniform 'camPos' in shader" );
            glUniform2f( index, Global::camera->GetGLPosition().x, Global::winSz->y - Global::camera->GetGLPosition().y );
        }

        //  Send the vertex coordinates to the shader
        glEnableVertexAttribArray(0);
        glBindBuffer( GL_ARRAY_BUFFER, vbID );
        glVertexAttribPointer( 0, 2, GL_FLOAT, GL_FALSE, 0, 0 );
        glEnableVertexAttribArray(1);
        glBindBuffer( GL_ARRAY_BUFFER, cbID );
        glVertexAttribPointer( 1, 4, GL_FLOAT, GL_FALSE, 0, 0 );

        glDrawArrays( GL_QUADS, 0, drawables.size() * 4 );

        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
    }
}

