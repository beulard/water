#ifndef WATER_FONT
#define WATER_FONT
#include "GL/glew.h"
#include "ft2build.h"
#include FT_FREETYPE_H
#include "vec2.hpp"
#include <vector>
#include <string>

typedef unsigned int u32;

class Glyph;

class Font
{
    public:
        Font();
        Font( const Font& other );
        void operator=( const Font& other );
        ~Font();

        void Load( const std::string& file, u32 sz );
        const Glyph& GetGlyph( char character );
        //	Returns the font size
        u32 GetSize() const;

        //	This function needs to be called by the
        //	data loader when loading a font, in order
        //	to set the name of the font plus the size
        //	it was loaded with.
        void SetName( const std::string& s );

        //	This function returns the name attributed to
        //	the font when it is loaded, which contains
        //	the data file name (*.ttf) and the size it
        //	was loaded with. It would look like "data/fonts/example.18"
        const std::string& GetName() const;

        const vec2i& GetTextureSize() const;

        GLuint GetID()  const;


    private:
        GLuint texID;
        FT_Face face;
        u32 size;
        std::string name;
        vec2i texSize;
        std::vector< Glyph > glyphs;
};

#endif // WATER_FONT
