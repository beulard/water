#ifndef WATER_TRIANGLE
#define WATER_TRIANGLE
#include "Shape.hpp"

class Triangle : public Shape
{
    public:
        Triangle();
        Triangle( const vec2i& v0, const vec2i& v1, const vec2i& v2 );
        ~Triangle();

		virtual void Draw();
		virtual void StopDrawing();
        virtual void Render();
};

#endif // WATER_TRIANGLE
