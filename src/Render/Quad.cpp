#include "Quad.hpp"
#include "Renderer.hpp"
#include "Common.hpp"
#include "Camera.hpp"

Quad::Quad(){
    SetRenderMode( RM_WorldColored );
    type = CT_Quad;
}

Quad::Quad( const vec2i& pos, const vec2ui& sz ) {
    SetPosition( pos );
    SetSize( sz );
    SetRenderMode( RM_WorldColored );
    type = CT_Quad;
}

Quad::Quad( const Quad& other ) : Colored() {
    *this = other;
}

void Quad::operator=( const Quad& other ) {
    if( drawn )
        StopDrawing();
    SetPosition( other.position );
    SetSize( other.size );
    SetRenderMode( other.mode );
    SetColor( other.color );
    type = other.type;
}

Quad::~Quad(){
    if( drawn )
        StopDrawing();
}

void Quad::Move( const vec2i& offset ) {
    position += offset;
    if( batchID != -1 )
        batch->SetDrawablePosition( batchID, position );
}

void Quad::SetSize( const vec2ui& sz ) {
    size = sz;
}

void Quad::AddToBatch() {
    batch->AddDrawable( *this );
}

void Quad::RemoveFromBatch() {
    batch->RemoveDrawable( batchID );
}

void Quad::Draw() {
    if( !drawn )
        Global::renderer->Draw( *this );
    else
        Warning( "Trying to add a quad to the renderer twice" );
    drawn = true;
}

void Quad::StopDrawing() {
    if( drawn )
        Global::renderer->StopDrawing( *this );
    else
        Warning( "Trying to remove a quad from the renderer but it is not in" );
    drawn = false;
}

/*void Quad::Render() {
    if( needsUpdate )
        UpdateGLVertices();
    shader->Use();


    //  Send the projection matrix to the shader
    int index = glGetUniformLocation( shader->GetID(), "projMat" );
    if( index == -1 )
        Error( "No uniform 'projMat' in shader !" );
    glUniformMatrix3fv( index, 1, GL_FALSE, Global::projMatrix );

    if( mode == RM_WorldColored ) {
        index = glGetUniformLocation( shader->GetID(), "camPos" );
        if( index == -1 )
            Error( "No uniform 'camPos' in shader" );
        glUniform2f( index, Global::camera->GetGLPosition().x, Global::winSz->y - Global::camera->GetGLPosition().y );
    }

    index = glGetUniformLocation( shader->GetID(), "color" );
    if( index == -1 )
        Error( "No uniform 'color' in shader !" );
    glUniform4f( index, color.r / 255.f, color.g / 255.f, color.b / 255.f, color.a / 255.f );

    //  Send the vertex and color attributes to the shader
    glEnableVertexAttribArray(0);
    glBindBuffer( GL_ARRAY_BUFFER, glID );
    glVertexAttribPointer( 0, 2, GL_FLOAT, GL_FALSE, 0, 0 );

    glDrawArrays( GL_QUADS, 0, 4 );

    glDisableVertexAttribArray(0);

}*/
