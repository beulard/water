#include "Anim.hpp"
#include "Common.hpp"
#include "MasterAnim.hpp"

Anim::Anim() : rotation( 0.f ), running(true), currentFrame( 0 ), animID( -1 ) {
    SetRenderMode( RM_None );
    timer.SetLoop( true );
}

Anim::Anim( const Anim& anim ) : Drawable( anim ) {
    animID = -1;
    *this = anim;
}

void Anim::operator=( const Anim& anim ) {
    if( animID != -1 )
        StopDrawing();
    animID = -1;
    timer.SetLoop( true );
    position = anim.position;
    rotation = anim.rotation;
    size = anim.size;
    running = anim.running;
    currentFrame = anim.currentFrame;
    if( anim.frameTimes.size() > 0 )
        timer.SetTimer( anim.frameTimes[0] );
    for( u32 i = 0; i < anim.sprites.size(); ++i ) {
        sprites.push_back( Sprite( anim.sprites[i] ) );
        frameTimes.push_back( anim.frameTimes[i] );
    }
}


Anim::~Anim(){
    if( drawn )
        StopDrawing();
}

void Anim::AddFrame( Sprite& s, float time, float rotation, vec2i offset ) {
    sprites.push_back( Sprite(s) );
    if( sprites.back().GetSize().x > size.x && sprites.back().GetSize().y > size.y ) {
        size = sprites.back().GetSize();
    }
    frameTimes.push_back( time );
    sprites.back().Rotate( rotation );
    sprites.back().SetPosition( position + offset );
}

const vec2i& Anim::GetPosition() const{
    return position;
}

void Anim::Move( const vec2i& offset ) {
    position += offset;
    sprites[currentFrame].SetPosition( position );
}

void Anim::SetPosition( const vec2i& pos ) {
    vec2i offset = pos - position;
    Move( offset );
}

void Anim::SetSize( const vec2ui& sz ) {
    for( u32 i = 0; i < sprites.size(); ++i )
        sprites[i].SetSize( sz );
    size = sz;
}

u32 Anim::GetFrameNumber()  const {
    return sprites.size();
}

u32 Anim::GetCurrentFrame() const {
    return currentFrame;
}

void Anim::Draw() {
    if( !drawn ) {
        drawn = true;
        if( animID == -1 ) {
            Global::masterAnim->AddAnim( *this );
            timer.Start();
        }
        else
            Warning( "Trying to add an anim to the master anim but it is already in" );
        sprites[currentFrame].Draw();
    }
    else
        Warning( "Trying to add an anim to the renderer twice" );
}

void Anim::StopDrawing() {
    if( drawn ) {
        drawn = false;
        if( animID != -1 ) {
            Global::masterAnim->RemoveAnim( animID );
            timer.Stop();
        }
        else
			Warning( "Trying to remove an anim from the master anim but it is not in" );
        sprites[currentFrame].StopDrawing();
    }
    else
        Warning( "Trying to remove an anim from the renderer but it is not in" );

}



void Anim::Rotate( float angle ) {
    for( u32 i = 0; i < sprites.size(); ++i )
        sprites[i].Rotate( angle );
    rotation = angle;
}

float Anim::GetRotation()   const {
    return rotation;
}

bool Anim::HasChangedFrame() {
    return changedFrame;
}

void Anim::Update() {
    if( changedFrame )
        changedFrame = false;
    if( timer.HasEnded() ) {
        changedFrame = true;
        u32 oldFrame = currentFrame;
        u32 newFrame = currentFrame;
        if( sprites.size() > 1 ) {
            if( newFrame < sprites.size() - 1 )
                newFrame++;
            else
                newFrame = 0;

            timer.SetTimer( frameTimes[newFrame] );
        }
        if( newFrame != oldFrame ) {
            if( sprites[oldFrame].IsDrawn() )
                sprites[oldFrame].StopDrawing();
            currentFrame = newFrame;
            sprites[currentFrame].SetPosition( position );
            sprites[currentFrame].Draw();
        }
    }
}

void Anim::SetAnimID( int i ) {
    animID = i;
	if( i == -1 )
		drawn = false;
}
