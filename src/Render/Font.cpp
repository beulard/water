#include "Font.hpp"
#include "Game.hpp"
#include "Glyph.hpp"

Font::Font() : texSize( 0, 0 ) {
    glGenTextures( 1, &texID );
	if( texID == 0 )
		Error( "Couldn't generate an OpenGL texture" );
}

Font::Font( const Font& other ) {
	glGenTextures( 1, &texID );
	if( texID == 0 )
		Error( "Couldn't generate an OpenGL texture" );
	*this = other;
}

void Font::operator=( const Font& other ) {
	face = other.face;
	size = other.size;
	name = other.name;
	texSize = other.texSize;
	glyphs = other.glyphs;
}

Font::~Font(){
    glDeleteTextures( 1, &texID );
}

void Font::Load( const std::string& file, u32 sz ) {
    if( FT_New_Face( Global::Water.ft, file.c_str(), 0, &face ) )
        Error( "Couldn't load font '%s' !", file.c_str() );
    size = sz;
    FT_Set_Pixel_Sizes( face, 0, sz );
    glActiveTexture( GL_TEXTURE0 );
    glBindTexture( GL_TEXTURE_2D, texID );

    glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );

    for( u32 i = 32; i < 128; ++i ) {
        if( FT_Load_Char( face, i, FT_LOAD_RENDER ) ) {
            Error( "Could not load character '%c' !", i );
            return;
        }
        texSize.x += face->glyph->bitmap.width;
        texSize.y = std::max( texSize.y, face->glyph->bitmap.rows );
    }
    glTexImage2D( GL_TEXTURE_2D, 0, GL_ALPHA, texSize.x, texSize.y, 0, GL_ALPHA, GL_UNSIGNED_BYTE, 0 );
    for( u32 i = 32, pen = 0; i < 128; ++i ) {
        if( FT_Load_Char( face, i,  FT_LOAD_RENDER ) ) {
            Error( "Couldn't load character '%c' !", i );
            return;
        }
        FT_GlyphSlot g = face->glyph;
        glyphs.push_back( Glyph() );
        glyphs.back().SetAdvance( vec2i( (g->advance.x >> 6), g->advance.y >> 6 ) );
        glyphs.back().SetBearing( vec2i( g->metrics.horiBearingX >> 6, g->metrics.horiBearingY >> 6 ) );
        glyphs.back().SetSize( vec2i( g->bitmap.width, g->bitmap.rows ) );
        glyphs.back().SetChar( (char)i );
        glyphs.back().SetTopLeftOffset( vec2i( g->bitmap_left, g->bitmap_top ) );

        float left = (float) pen / texSize.x;
        float top = (float) g->bitmap.rows / texSize.y;
        float right = (float) ( pen + g->bitmap.width ) / texSize.x;

        glyphs.back().SetUV( 0, vec2f( left, 0.f ) );
        glyphs.back().SetUV( 1, vec2f( right, 0.f ) );
        glyphs.back().SetUV( 2, vec2f( right, top ) );
        glyphs.back().SetUV( 3, vec2f( left, top ) );
        glTexSubImage2D( GL_TEXTURE_2D, 0, pen, 0, g->bitmap.width, g->bitmap.rows, GL_ALPHA, GL_UNSIGNED_BYTE, g->bitmap.buffer );
        pen += g->bitmap.width;
    }
}

const Glyph& Font::GetGlyph( char character ) {
    u32 id = (int)character;
	return glyphs[id - 32];
}

GLuint Font::GetID() const {
    return texID;
}

u32 Font::GetSize() const {
    return size;
}

const std::string& Font::GetName() const {
	return name;
}

void Font::SetName( const std::string& s ) {
	name = s;
}

const vec2i& Font::GetTextureSize() const {
    return texSize;
}

