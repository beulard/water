#ifndef WATER_QUADMESH
#define WATER_QUADMESH
#include <GL/glew.h>
#include "Mesh.hpp"
#include "ShaderProgram.hpp"
#include "vec2.hpp"

class Quadmesh : public Mesh
{
    public:
        Quadmesh();
        virtual ~Quadmesh();

		//	This function sets the size of the mesh
		//	while keeping its rotation
        virtual void SetSize( const vec2ui& sz );

        //  We need to redefine this function for the center to be updated
        virtual void Move( const vec2i& pos );
        virtual void Draw() = 0;
        virtual void StopDrawing() = 0;
        virtual void Render() = 0;
        //  Same function as the Mesh one, except it rotates the quadmesh around its center
        virtual void Rotate( float angle );

    protected :
        //  We keep the position of the center so we can apply rotations around it
        vec2i center;
		//	And the half-extents for resizing etc.
		vec2i halfextents;
};

#endif // WATER_QUADMESH
