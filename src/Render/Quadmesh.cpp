#include "Quadmesh.hpp"
#include "Common.hpp"

Quadmesh::Quadmesh() : center( 5, 5 ), halfextents( 5, 5 ) {
    vertices.resize( 8 );
}

Quadmesh::~Quadmesh(){

}

void Quadmesh::SetSize( const vec2ui& sz ) {
    halfextents = sz / 2;
    vec2i v0 = center - halfextents;
    vec2i v1 = center + vec2i( halfextents.x, -halfextents.y );
    vec2i v2 = center + halfextents;
    vec2i v3 = center + vec2i( -halfextents.x, halfextents.y );
    SetVertex( 0, v0 );
    SetVertex( 1, v1 );
    SetVertex( 2, v2 );
    SetVertex( 3, v3 );
    Rotate( rotation );
    needsUpdate = true;
}

void Quadmesh::Move( const vec2i& off ) {
    Mesh::Move( off );
    center += off;
}


void Quadmesh::Rotate( float angle ) {
    double mat[4] = { cos( angle ), -sin( angle ),
                     sin( angle ), cos( angle ) };
    for( u32 i = 0; i < vertices.size(); i += 2 ) {
        vec2i v = vec2i(vertices[i], vertices[i + 1]) - center;
        vec2i newVert = center + vec2i( v.x * mat[0] + v.y * mat[1], v.x * mat[2] + v.y * mat[3] );
        vertices[i] = newVert.x;
        vertices[i + 1] = Global::winSz->y - newVert.y;
    }
    needsUpdate = true;
}

