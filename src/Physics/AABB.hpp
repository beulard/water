#ifndef WATER_AABB
#define WATER_AABB
#include "vec2.hpp"

enum s_sides {    //  Sides of the boxes that collide together. The first word is the first box.
    Bottom_Top,
    Right_Left,
    Left_Right,
    Top_Bottom
};

struct result {
	vec2f mtv;   // minimum translation vector
    s_sides sides;
};

// 1 dimensional test.
bool segmentOnSegment( float start1, float end1, float start2, float end2 );

enum BoxType{
    BT_StaticSpriteBox,
    BT_DynamicSpriteBox
};

enum CollisionType {
    CT_None,
    CT_Solid,
    CT_Liquid
};

class AABB
{
    public:
        AABB();
        AABB( vec2f pCenter, vec2f pHalfextents );
        ~AABB();

		bool Collides( AABB& b );
        result GetResult( AABB& b );

        void Move( const vec2f& offset );

        void MoveLeft();
        void MoveRight();
        void Jump();

        void SetSpeed( float s );

        float GetNewVelX();

        //  Center of the box
        vec2f center;
        //  Half of the size of the box
        vec2f halfextents;

        //  Physics stuff
        vec2f velocity;
        vec2f acceleration;

        //  Is the box on the ground ?
        bool onGround;
        //  Is the box under water ?
        bool underWater;
        //  Defines what happens when the box collides with other boxes
        CollisionType ct;

        //  Has the box jumped ?
        bool jump;
        //  Is the box jumping ?
        bool jumping;
        //  Does the box move around ?
        bool dynamic;

    private :
        //  This value is the speed at which an entity is moving when it is moving
        float speed;
        //  This vector will define which direction we are going with -1 to 1 int values
        vec2i velMul;
};

#endif // WATER_AABB
