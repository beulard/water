#include "AABB.hpp"
#include "common.hpp"


bool segmentOnSegment( float start1, float end1, float start2, float end2 ) {
    float min = start1 < start2 ? start1 : start2;
    float max = end1 > end2 ? end1 : end2;
    float len = max - min;
    if( ( end1 - start1 ) + ( end2 - start2 ) > len )
        return true;
    return false;
}


AABB::AABB() : center( vec2f( 0.f, 0.f ) ), halfextents( vec2f( 0.f, 0.f ) ), velocity( vec2f( 0.f, 0.f ) ), acceleration( vec2f( 0.f, 0.f ) ), onGround( false ), ct( CT_Solid ), speed( 15.f ), velMul( vec2i(0, 0) )
{

}

AABB::AABB( vec2f pCenter, vec2f pHalfextents ) : center(pCenter), halfextents(pHalfextents), velocity( vec2f( 0.f, 0.f ) ), acceleration( vec2f( 0.f, 0.f ) ), onGround( false ), ct( CT_Solid ), speed( 15.f ), velMul( vec2i(0, 0) )
{

}

AABB::~AABB()
{

}

void AABB::Move( const vec2f& offset ) {
    center += offset;
}

bool AABB::Collides( AABB& b ) {
    vec2f ncenter1 = center + velocity;
    vec2f ncenter2 = b.center + b.velocity;
    vec2f nbox1start = ncenter1 - halfextents;
    vec2f nbox1end = ncenter1 + halfextents;

    vec2f nbox2start = ncenter2 - b.halfextents;
    vec2f nbox2end = ncenter2 + b.halfextents;

    //  nxol stands for Next X OverLap.
    bool nxol = segmentOnSegment( nbox1start.x, nbox1end.x, nbox2start.x, nbox2end.x );
    if( !nxol )
        return false;
    bool nyol = false;

    nyol = segmentOnSegment( nbox1start.y, nbox1end.y, nbox2start.y, nbox2end.y );
    if( !nyol )
        return false;
    return true;
}

result AABB::GetResult( AABB& b ) {
    result r;
    r.mtv = vec2f( 0.f, 0.f );


    //  Values for this frame
    vec2f box1start = center - halfextents;
    vec2f box1end = center + halfextents;

    vec2f box2start = b.center - b.halfextents;
    vec2f box2end = b.center + b.halfextents;


    //  Values for the next frame
    vec2f ncenter1 = center + velocity;
    vec2f ncenter2 = b.center + b.velocity;

    vec2f centers = ncenter2 - ncenter1;


    if( center.x <= b.center.x )
        r.mtv.x = centers.x - halfextents.x - b.halfextents.x;
    else
        r.mtv.x = centers.x + halfextents.x + b.halfextents.x;
    if( center.y <= b.center.y )
        r.mtv.y = centers.y - halfextents.y - b.halfextents.y;
    else
        r.mtv.y = centers.y + halfextents.y + b.halfextents.y;

    if( fabs( r.mtv.x ) < fabs( r.mtv.y ) ) {
        if( center.x <= b.center.x )
            r.sides = Right_Left;
        else
            r.sides = Left_Right;
    }
    else {
        if( center.y <= b.center.y )
            r.sides = Bottom_Top;
        else
            r.sides = Top_Bottom;
    }

    bool xol = segmentOnSegment( box1start.x, box1end.x, box2start.x, box2end.x );
    bool yol = segmentOnSegment( box1start.y, box1end.y, box2start.y, box2end.y );
    if( xol && !yol ) { //  If there was no vertical overlap before but there was a horizontal one, it means the collision will be vertical
        if( b.center.y >= center.y )
            r.sides = Bottom_Top;
        else
            r.sides = Top_Bottom;
    }
    if( !xol && yol ) {
        if( b.center.x >= center.x )
            r.sides = Right_Left;
        else
            r.sides = Left_Right;
    }


    r.mtv += velocity;

    switch( r.sides ) {
        case Bottom_Top :
            if( b.ct == CT_Solid ) {
                velocity.y = 0.f;
                onGround = true;
            }
            if( ct == CT_Solid && b.dynamic )
                b.velocity.y = .5f;
        case Top_Bottom :
            if( ct == CT_Solid ) {
                b.velocity.y = 0.f;
                b.onGround = true;
            }
            if( b.ct == CT_Solid && dynamic )
                velocity.y = .5f;
            r.mtv.x = 0.f;
        break;
        case Right_Left :
        case Left_Right :
            r.mtv.y = 0.f;
            if( b.ct == CT_Solid )
                velocity.x = 0.f;

            if( ct == CT_Solid )
                b.velocity.x = 0.f;
        break;
    }


    return r;
}

void AABB::MoveLeft() {
    if( velMul.x > -1 )
    velMul.x -= 1;
}

void AABB::MoveRight() {
    if( velMul.x < 1 )
        velMul.x += 1;
}

void AABB::Jump() {
    jump = true;
}

void AABB::SetSpeed( float s ) {
    speed = s;
}

float AABB::GetNewVelX() {
    float tmp = velMul.x;
    velMul.x = 0;
    return speed * tmp;
}


