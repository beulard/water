#ifndef WATER_CELL
#define WATER_CELL
#include "Rect.hpp"

class Cell
{
    public :
        Cell();
        Cell( const Recti& r );
        ~Cell();

        void SetRect( const Recti& r );
        const Recti& GetRect() const;

        int GetTop() const;
        int GetBottom() const;
        int GetLeft() const;
        int GetRight() const;

        void SetDrawn( bool d );
        bool IsDrawn() const;

    private :
        Recti rect;
        bool drawn;

};

#endif // WATER_CELL
