#include "Chunk.hpp"
#include "Common.hpp"
#include "Tile.hpp"
#include "World.hpp"

Chunk::Chunk() : modified(false), initialized(false) {

}

Chunk::~Chunk() {
    if( initialized )
        Destroy();
}

void Chunk::Create( u32 sz ) {
    size = sz;
    if( initialized )
        Destroy();
    for( u32 x = 0; x < size; ++x ) {
        layers.back.push_back( std::vector< Tile* >() );
        layers.middle.push_back( std::vector< Tile* >() );
        layers.front.push_back( std::vector< Tile* >() );
        for( u32 y = 0; y < Map::SizeY; ++y ) {
            layers.back.back().push_back( new Tile );
            layers.middle.back().push_back( new Tile );
            layers.front.back().push_back( new Tile );
        }
    }
    initialized = true;
}

void Chunk::Destroy() {
    if( initialized ) {
        while( !layers.back.empty() ) {
            while( !layers.back.back().empty() ) {
                delete layers.back.back().back();
                delete layers.middle.back().back();
                delete layers.front.back().back();
                layers.back.back().pop_back();
                layers.middle.back().pop_back();
                layers.front.back().pop_back();
            }
            layers.back.pop_back();
            layers.middle.pop_back();
            layers.front.pop_back();
        }
    }
    initialized = false;
    modified = false;
}

void Chunk::Save( FileStream& fs ) {
    fs.SeekP( 0 + Map::HeaderSize, std::ios_base::beg );
    long pos = position * Map::SizeY * 3 * sizeof(int);
    fs.SeekP( pos );
    for( u32 x = 0; x < size; ++x ) {
        for( u32 y = 0; y < Map::SizeY; ++y ) {
            int ids[] = { layers.back[x][y]->GetTileID(),
                          layers.middle[x][y]->GetTileID(),
                          layers.front[x][y]->GetTileID() };
            fs.WriteBinary( ids, sizeof(ids) );
        }
    }
    modified = false;
}

void Chunk::Load( FileStream& fs, u32 pos, u32 sz ) {
    //  Recreate the chunk with right dimensions if they changed
    if( size != sz && initialized )
        Destroy();
    Create( sz );
    size = sz;
    position = pos;
    fs.SeekG( 0 + Map::HeaderSize, std::ios_base::beg );
    fs.SeekG( position * Map::SizeY * 3 * sizeof(int) );
    for( u32 x = 0; x < size; ++x ) {
        for( u32 y = 0; y < Map::SizeY; ++y ) {
            std::vector<int> ids = fs.ReadBinary<int>( 3 );
            *layers.back[x][y] = ids[0];
            *layers.middle[x][y] = ids[1];
            *layers.front[x][y] = ids[2];
            layers.back[x][y]->SetSize( vec2ui( Map::TileSize, Map::TileSize ) );
            layers.middle[x][y]->SetSize( vec2ui( Map::TileSize, Map::TileSize ) );
            layers.front[x][y]->SetSize( vec2ui( Map::TileSize, Map::TileSize ) );
            u32 absx = x + pos;
            layers.back[x][y]->SetPosition( vec2i( absx * Map::TileSize, y * Map::TileSize ) );
            layers.middle[x][y]->SetPosition( vec2i( absx * Map::TileSize, y * Map::TileSize ) );
            layers.front[x][y]->SetPosition( vec2i( absx * Map::TileSize, y * Map::TileSize ) );
            Global::world->SetMapBox( vec2ui(Global::world->map.ToLocal(absx), y), layers.middle[x][y]->GetBox() );
        }
    }
    modified = false;
}

ChunkLayer& Chunk::GetLayer( Layer l ) {
    //  If the layer is requested, we can assume it's gonna be modified
    modified = true;
    if( l == Back )
        return layers.back;
    else if( l == Middle )
        return layers.middle;
    else
        return layers.front;
}

void Chunk::SetPosition( u32 pos ) {
    position = pos;
}

u32 Chunk::GetSize() const {
    return size;
}

bool Chunk::WasModified() const {
    return modified;
}
