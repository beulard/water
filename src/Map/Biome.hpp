#ifndef WATER_BIOME
#define WATER_BIOME

enum TerrainType {
    TT_Flat,
    TT_Rocky,
    TT_Plains,
    TT_Dunes,
    TT_River
};

class Biome
{
    public:
        Biome();
        ~Biome();

        void SetTemperature( int t );
        void SetHumidity( int h );

        void SetTerrainType( TerrainType t );
        TerrainType GetTerrainType() const;

        void SetMinSize( int sz );
        void SetMaxSize( int sz );
        int GetMinSize() const;
        int GetMaxSize() const;

    private:
        int temperature;
        int humidity;
        int minSize;
        int maxSize;
        TerrainType type;
};

#endif // WATER_BIOME
