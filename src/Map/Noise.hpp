#ifndef NOISE_HPP
#define NOISE_HPP
#include <vector>
#include "vec2.hpp"

typedef unsigned char u8;
typedef unsigned int u32;

class Noise
{
    public:
        Noise();
        virtual ~Noise();

        //  Generates an array with one value only
        void GenerateSingle( const vec2ui& sz, float val );
        void GenerateWhite( const vec2ui& sz );
        void GenerateSmooth( const Noise& base, u32 octave );
        //  You can specify a scale by which every value of the noise will be multiplicated
        //  and another value that will be added to everything in the noise
        void GeneratePerlin( const vec2ui& sz, u32 nbrOctaves, float scale = 1.f, int add = 0 );

        const std::vector< float >& operator[]( u32 index ) const;

        static float Interpolate( float x0, float x1, float alpha );

        vec2ui GetSize() const;

    protected:
        std::vector< std::vector< float > > array;
};

#endif // NOISE_HPP
