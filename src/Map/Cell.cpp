#include "Cell.hpp"

Cell::Cell() : drawn(false) {

}

Cell::Cell( const Recti& r ) : rect( r ), drawn(false) {

}

Cell::~Cell() {

}

void Cell::SetRect( const Recti& r ) {
    rect = r;
}

const Recti& Cell::GetRect() const {
    return rect;
}

int Cell::GetTop() const {
    return rect.GetTop();
}

int Cell::GetBottom() const {
    return rect.GetBottom();
}

int Cell::GetLeft() const {
    return rect.GetLeft();
}

int Cell::GetRight() const {
    return rect.GetRight();
}

void Cell::SetDrawn( bool d ) {
    drawn = d;
}

bool Cell::IsDrawn() const {
    return drawn;
}
