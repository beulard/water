#include "Noise.hpp"
#include "Random.hpp"

Noise::Noise() {

}

Noise::~Noise() {

}

void Noise::GenerateSingle( const vec2ui& sz, float val ) {
    for( u32 i = 0; i < sz.x; ++i ) {
        array.push_back( std::vector< float >() );
        for( u32 j = 0; j < sz.y; ++j ) {
            array[i].push_back( val );
        }
    }
}

void Noise::GenerateWhite( const vec2ui& sz ) {
    for( u32 i = 0; i < sz.x; ++i ) {
        array.push_back( std::vector< float >() );
        for( u32 j = 0; j < sz.y; ++j ) {
            array.back().push_back( (float)(Random::GetRandom( 0, Random::Max() )) / Random::Max() );
        }
    }
}

void Noise::GenerateSmooth( const Noise& base, u32 octave ) {
    vec2ui sz = base.GetSize();
    int samplePeriod = 1 << octave;
    float sampleFrequency = 1.f / samplePeriod;
    array.resize( sz.x );
    for( u32 i = 0; i < sz.x; ++i )
        array[i].resize( sz.y );

    for( u32 i = 0; i < array.size(); ++i ) {
        //  Calculate the horizontal sampling indices
        int samplei0 = (i / samplePeriod) * samplePeriod;
        int samplei1 = (samplei0 + samplePeriod) % array.size();
        float horizontalBlend = (i - samplei0) * sampleFrequency;

        for( u32 j = 0; j < array[i].size(); ++j ) {
            //  Calculate the vertical sampling indices
            int samplej0 = (j / samplePeriod) * samplePeriod;
            int samplej1 = (samplej0 + samplePeriod) % array[i].size();
            float verticalBlend = (j - samplej0) * sampleFrequency;

            //  Blend the top two corners
            float top = Interpolate( base[samplei0][samplej0], base[samplei1][samplej0], horizontalBlend );

            //  Blend the bottom two corners
            float bottom = Interpolate( base[samplei0][samplej1], base[samplei1][samplej1], horizontalBlend );

            //  Final blend
            array[i][j] = Interpolate( top, bottom, verticalBlend );
        }
    }
}

void Noise::GeneratePerlin( const vec2ui& sz, u32 nbrOctaves, float scale, int add ) {
    Noise base;
    base.GenerateWhite( sz );
    std::vector< Noise > smooth;

    float persistance = 0.5f;

    //   Generate smooth noise
    for( u32 i = 0; i < nbrOctaves; ++i ) {
        smooth.push_back( Noise() );
        smooth.back().GenerateSmooth( base, i );
    }

    array.resize( sz.x );
    for( u32 i = 0; i < sz.x; ++i )
        array[i].resize( sz.y );

    float amplitude = 1.0f;
    float totalAmplitude = 0.0f;

    //  Blend noise together
    for( int octave = nbrOctaves - 1; octave >= 0; octave-- ) {
        amplitude *= persistance;
        totalAmplitude += amplitude;

        for( u32 i = 0; i < sz.x; ++i )
            for( u32 j = 0; j < sz.y; ++j )
              array[i][j] += smooth[octave][i][j] * amplitude;
    }

    //  Normalisation and apply scale
    for( u32 i = 0; i < sz.x; ++i ) {
        for( u32 j = 0; j < sz.y; j++ ) {
            array[i][j] /= totalAmplitude;
            array[i][j] *= scale;
            array[i][j] += add;
        }
    }
}

const std::vector< float >& Noise::operator[]( u32 index ) const {
    return array[index];
}

float Noise::Interpolate( float x0, float x1, float alpha ) {
    return x0 * (1 - alpha) + alpha * x1;
}

vec2ui Noise::GetSize() const {
    return vec2ui( array.size(), array[0].size() );
}

