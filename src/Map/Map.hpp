#ifndef WATER_MAP
#define WATER_MAP
#include <deque>
#include <array>
#include <list>
#include <functional>
#include "Tile.hpp"
#include "Cell.hpp"
#include "Chunk.hpp"

class UILoadingScreen;
class FileStream;

typedef std::function< void( float ) > UpdateFunc;

class Map
{
    public:
        Map();
        ~Map();

        //  Size of the save file header
        static const u32 HeaderSize;
        //  Size of the side of one tile (default = 25)
        static const u32 TileSize;
        //  Vertical size of the map (it's constant)
        static const u32 SizeY;
        //  Horizontal size of one chunk
        static const u32 ChunkSize;

        void Clean();

        //  Display a loading screen while generating the map
        void OnUpdate( UILoadingScreen& screen, float progress );

        //  Those function work in conjunction with a loading screen so that the user gets information about what's
        //  happening while we are doing the job. That's why we use UpdateFuncs
        void GenerateWithScreen();
        void Generate( const UpdateFunc& uf );
        void ClearTiles( const UpdateFunc& uf );
        void UpdateTiles( const UpdateFunc& uf );
        //  Write info to the map file header
        void WriteHeader( FileStream& fs );
        //  Read info from the map header
        void ReadHeader( FileStream& fs );

        //  Save/Load functions
        void SetFolder( const std::string& f );
        //  Finds the next folder to save to for a newly generated map
        std::string GetNewMapFolder();

        //  Returns the global position (uses tile units)
        vec2ui ToGlobal( const vec2i& coords );
        vec2ui ToGlobal( int x, int y );
        u32    ToGlobal( int x );
        //  Does the reverse
        vec2i ToLocal( const vec2ui& coords );
        vec2i ToLocal( u32 x, u32 y );
        int   ToLocal( u32 x );

        //  Returns the offset from the beginning of the map to the currently loaded part
        u32 GetCurrentOffset() const;

        Rectui GetRenderZone();
        void Update();

        void SetRenderZoneSize( const vec2i& sz );

        const vec2ui& GetSize()	const;
        //  Returns a specific tile that's already loaded in one of the chunks
        //  i and j are in global tile coordinates
        Tile* GetTile( u32 x, u32 y, Layer l );
        //  Same with local coords
        Tile* GetTileLocal( u32 x, u32 y, Layer l );

        //  Will update a tile's appearance depending of the tiles around it
        void UpdateTileLocal( u32 x, u32 y, Layer l );
        void UpdateTile( u32 x, u32 y, Layer l );


    private:
        //  Directory attached to the map for saving/loading
        std::string folder;

        //  Offset of the currently loaded map with the absolute top left corner in tile units
        u32 offset;

        //  Currently loaded chunks. There will be 3 chunks most of the time (but depends on screen size)
        //  except on map's very edge
        std::deque< Chunk > chunks;

        //  In this vector will be stored the height of the ground for each x coordinate
        std::vector< u32 > heightMap;

        //  Cells are used to tell which part of the map has to be rendered
        vec2ui size;
        u32 nbrZones;
        bool generated;
        //  Is it the first time the map needs to be drawn in the game ?
        bool firstDraw;

        //  Number of total chunks needed to hold the whole map
        u32 totalChunks;
        //  Horizontal size of the last chunk in tile units
        u32 lastChunkSz;
        //  First and last chunks that are loaded in memory in chunk coords
        u32 currentFirstChunk;
        u32 currentLastChunk;

        //  Last current position of the player on the map
        vec2i lastCurrentPos;
        //  Render zone from the last frame
        Rectui lastRenderZone;
        //  Size of the render zone
        vec2ui renderZoneSz;
        //  Number of chunks that need to be loaded at all times in order to display on the whole screen and avoid clipping
        u32 nbrChunksLoaded;

};

#endif // WATER_MAP
