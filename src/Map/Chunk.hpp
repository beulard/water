#ifndef CHUNK_HPP
#define CHUNK_HPP
#include <vector>
#include "Rect.hpp"
#include "FileStream.hpp"


class Tile;

typedef std::vector< std::vector< Tile* > > ChunkLayer;

enum Layer {
    Back = 0,
    Middle = 1,
    Front = 2
};

enum PosInChunk {
    TopLeft,
    TopRight,
    BotLeft,
    BotRight
};


//  A chunk is a map structure that holds a maximum of Map::ChunkSize * Map::SizeY * 3 tiles
//  It can be saved and loaded to/from a file
class Chunk
{
    public:
        Chunk();
        ~Chunk();

        //  Initialize/allocate the chunk with its current 'size'
        //  Destroys the chunk if it hasn't been already
        void Create( u32 sz );
        //  Destroy/deallocate every tile from the chunk
        void Destroy();

        void Save( FileStream& fs );
        void Load( FileStream& fs, u32 position, u32 size );

        //  Simple utility function to get the right layer with an index
        ChunkLayer& GetLayer( Layer l );

        //  Set the new position of the chunk
        //  It should be loaded right after
        void SetPosition( u32 pos );

        //  Returns the horizontal size of the chunk
        u32 GetSize() const;

        bool WasModified() const;

    private:
        //  3 layers of tiles
        struct {
            std::vector< std::vector< Tile* > > back;
            std::vector< std::vector< Tile* > > middle;
            std::vector< std::vector< Tile* > > front;
        } layers;
        //  Did the chunk get modified between the moment it
        //  got loaded and the moment it needs to be destroyed ?
        bool modified;
        //  Absolute horizontal offset of the chunk with the map's beginning
        u32 position;
        //  Size is most likely Map::ChunkSize except if we are on map's edge
        u32 size;
        //  Has the chunk been initialized yet ?
        bool initialized;
};

#endif // CHUNK_HPP
