#include "Biome.hpp"

Biome::Biome() : temperature(0), humidity(0) {

}

Biome::~Biome(){

}

void Biome::SetTemperature( int t ) {
    temperature = t;
}

void Biome::SetHumidity( int h ) {
    humidity = h;
}

void Biome::SetTerrainType( TerrainType t ) {
    type = t;
}

TerrainType Biome::GetTerrainType() const {
    return type;
}

void Biome::SetMinSize( int sz ) {
    minSize = sz;
}

void Biome::SetMaxSize( int sz ) {
    maxSize = sz;
}

int Biome::GetMinSize() const {
    return minSize;
}

int Biome::GetMaxSize() const {
    return maxSize;
}
