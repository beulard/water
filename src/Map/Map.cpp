#include "Map.hpp"
#include <cmath>
#include "dirent.h"
#include "Game.hpp"
#include "Noise.hpp"

const u32 Global::tileSize = 25;
const u32 Map::HeaderSize = 7 * sizeof(char) + 2 * sizeof(u32);
const u32 Map::TileSize = 25;
const u32 Map::SizeY = 100;
const u32 Map::ChunkSize = 32;

Map::Map() : offset(0), size( vec2ui(0, 100) ), nbrZones(8), generated( false ), firstDraw(true), lastCurrentPos(vec2i(0, 0)), lastRenderZone( 0, 0, 0, 0 ), renderZoneSz( vec2i( 9, 7 ) ) {

}

Map::~Map(){

}

void Map::Clean() {
    Info( "Cleaning map..." );
    if( generated )
        Append( " Deallocating %d chunks...", chunks.size() );
    while( !chunks.empty() ) {
        chunks.back().Destroy();
        chunks.pop_back();
    }

    Append( " Done." );
}

void Map::OnUpdate( UILoadingScreen& screen, float progress ) {
    screen.SetProgress( progress );
    Global::renderer->Clear();
    Global::renderer->Render();
}

void Map::GenerateWithScreen() {
    if( Global::Water.soup.IsDrawn() )
        Global::Water.soup.StopDrawing();
    UILoadingScreen screen;
    UpdateFunc uf = std::bind( &Map::OnUpdate, std::ref(*this), std::ref(screen), std::placeholders::_1 );
    screen.Draw();
    //  TODO When generating a new map over an old one, tiles don't seem to be removed from rendering correctly
    //  TODO Game crashes when going at either end
    //  TODO Update tiles that get loaded
    if( generated ) {
        screen.SetInfo( "Clearing map" );
        ClearTiles( uf );
    }
    screen.SetInfo( "Generating map" );
    Generate( uf );
    screen.SetInfo( "Rearranging tiles" );
    UpdateTiles( uf );
    screen.StopDrawing();
}

void Map::Generate( const UpdateFunc& uf ) {
    renderZoneSz = *Global::winSz / Map::TileSize;
    Info( "Generating a new map" );
    SetFolder( GetNewMapFolder() );
    Info( "Save folder is '%s'", folder.c_str() );

    std::map< Wash, Biome >& biomes = Global::dataLoader->GetBiomes();

    //  Total size of the map
    u32 totalSize = 0;
    //  Current position in the map
    u32 currentPos = 0;
    //  The filestream to our save file
    FileStream fs( folder + "map", OM_Write | OM_Binary );
    //  Used for storing tiles while generating
    Chunk chunk;
    totalChunks = 0;

    const Tile& air = Global::dataLoader->GetTile( "air" );
    const Tile& earth = Global::dataLoader->GetTile( "earth" );
    const Tile& water = Global::dataLoader->GetTile( "water" );
    const Tile& voidTile = Global::dataLoader->GetTile( "void" );

    //  Array that represents the geography of the map
    //  It's represented by 0's for ground and 1's for air
    //  It's intended to be dumped into a .pgm file
    std::vector< std::vector< u8 > > geography;

    //  We keep all the unsaved tiles IDs in this vector until we get to
    //  either 32 tiles (one chunk) or the end of the map
    std::vector< std::vector< int > > tileStack;


    //  We are going to generate a set of sizes that zones will have
    //  So that we have the total size of the map for easier generation and save
    std::vector< const Biome* > zoneBiomes;
    std::vector< u32 > zoneSizes;
    for( u32 i = 0; i < nbrZones; ++i ) {
        //  Determine which biome will be used
        std::map<Wash, Biome>::iterator it = biomes.begin();
        //u32 id = Random::GetRandom( 2, biomes.size() - 1 );
        u32 id = 0;
        Info( "BIOME ID : %d", id );
        for( u32 j = 0; j < id; ++j )
            it++;
        zoneBiomes.push_back( &it->second );
        zoneSizes.push_back( Random::GetRandom(it->second.GetMinSize(), it->second.GetMaxSize()) );
        totalSize += zoneSizes.back();
    }

    lastChunkSz = zoneSizes.back();

    for( u32 i = 0; i < nbrZones; ++i ) {
        //  Offset from the beginning of the zone to the save position
        const Biome* b = zoneBiomes[i];

        //vec2ui sz( Random::GetRandom( b.GetMinSize(), b.GetMaxSize() ), this->size.y );
        vec2ui sz( zoneSizes[i], this->size.y );

        //  2D array of bools : 1 for solid, 0 for void
        bool tiles[sz.x][sz.y];

        //  The system works this way : a zone is generated.
        //  We then go through the zone, saving every 32 tiles,
        //  and keeping track of the number of remaining tiles
        //  when we have reached the end of the zone so that we
        //  can save them together with the next zone's tiles.

        ///     ZONE GENERATION
        //  The more octaves the smoother the terrain gets
        //  The higher the scale, the more irregular the terrain gets

        //  Here we generate the heightmap of the zone which is one dimension
        //  and then add it to the global heightmap
        Noise n;
        if( b->GetTerrainType() == TT_Rocky ) {
            //  Rocky, irregular terrain, begins somewhat high
            n.GeneratePerlin( vec2ui( sz.x, 1 ), 4, 16, 10 );
        }
        else if( b->GetTerrainType() == TT_Plains ) {
            //  Little scale and lot of octaves : we need a flat terrain that starts low
            n.GeneratePerlin( vec2ui( sz.x, 1 ), 8, 3, 20 );
        }
        else if( b->GetTerrainType() == TT_Flat ) {
            //  This one is special : it is a flat terrain for testing purposes
            n.GenerateSingle( vec2ui( sz.x, 1 ), sz.y / 2 );
        }
        else if( b->GetTerrainType() == TT_Dunes ) {
            n.GeneratePerlin( vec2ui( sz.x, 1 ), 16 );
        }
        else if( b->GetTerrainType() == TT_River ) {
            n.GeneratePerlin( vec2ui( sz.x, 1 ), 16 );
        }
        for( u32 j = 0; j < sz.x; ++j ) {
            //  Calculate the altitude for every column
            u32 altitude = (u32)(n[j][0]);
            heightMap.push_back( altitude );
            for( u32 k = 0; k < altitude; ++k ) {
                tiles[j][k] = 0;
            }
            for( u32 k = altitude; k < sz.y; ++k ) {
                tiles[j][k] = 1;
            }
            geography.push_back( std::vector< u8 >() );
            for( u32 k = 0; k < size.y; ++k ) {
                geography.back().push_back( (u8)(tiles[j][k]) * 255 );
            }
        }
        uf( (i + 1 / 3.f) / (float)nbrZones );
        //  We now have an array that tells us which tile is solid or not in the zone
        //  Let's add a little horizontal turbulence to it to have a nicer result
        bool newTiles[sz.x][sz.y];
        Noise turbulence;
        turbulence.GeneratePerlin( sz, 5 );
        for( u32 j = 0; j < sz.x; ++j ) {
            tileStack.push_back( std::vector< int >() );
            for( u32 k = 0; k < sz.y; ++k ) {
                float power = 5;
                float noise = turbulence[j][k];
                u32 px = j + noise * power;
                if( px >= sz.x )
                    px = sz.x;
                //  Not functional : TODO add turbulence
                //newTiles[j][k] = tiles[px][k];
                newTiles[j][k] = tiles[j][k];
                tileStack.back().push_back( newTiles[j][k] );
            }
        }
        uf( (i + 2 / 3.f) / (float)nbrZones );

        //  We can now assign tiles and save
        if( tileStack.size() >= Map::ChunkSize || currentPos + tileStack.size() == totalSize ) {
            //  Calculate the number of chunks we need to save for this zone
            u32 chunksToSave = tileStack.size() / Map::ChunkSize;
            if( currentPos + tileStack.size() == totalSize && totalSize % Map::ChunkSize != 0 )
                chunksToSave++;
            u32 tilesSaved = 0;
            for( u32 j = 0; j < chunksToSave; ++j ) {
                bool lastChunk = (j == chunksToSave - 1);
                u32 chunkSz = Map::ChunkSize;
                //  If we arrived at the very end of the map,
                //  We need to adjust the size of the chunk.
                if( i == nbrZones - 1 && j == chunksToSave - 1 )
                    chunkSz = totalSize % Map::ChunkSize;

                //  Allocate a chunk
                chunk.SetPosition( currentPos / Map::ChunkSize * Map::ChunkSize );
                chunk.Create( chunkSz );

                //  Assign the tile we generated to the chunk
                ChunkLayer& back = chunk.GetLayer( Back );
                ChunkLayer& middle = chunk.GetLayer( Middle );
                ChunkLayer& front = chunk.GetLayer( Front );
                for( u32 k = 0; k < chunkSz; ++k ) {
                    for( u32 l = 0; l < sz.y; ++l ) {
                        if( tileStack[k][l] == 0 ) {
                            *back[k][l] = air;
                            *middle[k][l] = voidTile;
                            *front[k][l] = voidTile;
                        }
                        else {
                            *back[k][l] = voidTile;
                            *middle[k][l] = earth;
                            *front[k][l] = voidTile;
                        }
                    }
                }
                //  Save the chunk and reset the current position
                chunk.Save( fs );
                //  Destroy the saved chunk
                chunk.Destroy();
                totalChunks++;

                tilesSaved += chunkSz;
                if( lastChunk ) {
                    if( tileStack.size() == tilesSaved )
                        tileStack.clear();
                    else {
                        for( u32 k = tilesSaved; k < tileStack.size(); ++k )
                            for( u32 l = 0; l < sz.y; ++l )
                                tileStack[k - tilesSaved][l] = tileStack[k][l];
                        tileStack.resize( tileStack.size() - tilesSaved );
                    }
                }
                currentPos += chunkSz;
            }
        }

        //  Update the loading screen
        uf( (i + 1) / (float)nbrZones );
    }

    size.x = totalSize;
    WriteHeader( fs );

    fs.Close();

    Info( "Geography array size : %d", geography.size() * geography.back().size() * sizeof(u8) );
    FileStream::WriteToPPM( folder + "map_geography.pgm", geography );

    generated = true;

    Info( "Map generated and saved (%d chunks), size = (%d, %d), render zone size = (%d, %d)", totalChunks, size.x, size.y, renderZoneSz.x, renderZoneSz.y );

    fs.Open( folder + "map", OM_Read | OM_Binary );
    ReadHeader( fs );
    totalChunks = size.x / Map::ChunkSize + 1;
    lastChunkSz = size.x % Map::ChunkSize;
    if( lastChunkSz == 0 ) lastChunkSz = Map::ChunkSize;
    chunks.clear();
    u32 startingPosX = Random::GetRandom( 0, size.x );

    nbrChunksLoaded = (renderZoneSz.x + 31) / Map::ChunkSize + 2;
    u32 firstChunk = startingPosX / Map::ChunkSize - nbrChunksLoaded / 2;
    offset = firstChunk * Map::ChunkSize;
    Info( "startPos : %d, / 32 = %d, nbrChunksLoaded : %d, first : %d", startingPosX, startingPosX / 32, nbrChunksLoaded, firstChunk );
    for( u32 i = firstChunk; i < firstChunk + nbrChunksLoaded; ++i ) {
        chunks.push_back( Chunk() );
        Info( "Loading chunk %d", i );
        u32 sz = Map::ChunkSize;
        if( i == totalChunks - 1 )
            sz = lastChunkSz;

        Global::world->AddChunkBoxes( Right, sz );
        chunks.back().Load( fs, i * Map::ChunkSize, sz );
    }
    u32 startingPosY = heightMap[startingPosX] - 4;
    currentFirstChunk = firstChunk;
    currentLastChunk = firstChunk + nbrChunksLoaded - 1;
    Info( "offset %d", offset );

    Info( "start %d %d", startingPosX, startingPosY );
    lastCurrentPos = vec2i( startingPosX, startingPosY );

    Global::Water.soup.SetPosition( lastCurrentPos * Map::TileSize );

    fs.Close();
}

void Map::ClearTiles( const UpdateFunc& uf ) {
    if( chunks.size() > 0 ) {
        Info( "Deallocating current map" );
        while( !chunks.empty() ) {
            chunks.back().Destroy();
            chunks.pop_back();
            uf( (float)(nbrChunksLoaded - chunks.size()) / (float)(nbrChunksLoaded) );
        }
        Global::world->ClearStaticBoxes();
        Global::world->ClearMapBoxes();
        Global::world->ClearDynamicBoxes();
    }
}

void Map::UpdateTiles( const UpdateFunc& uf ) {
    for( u32 i = 0; i < chunks.size(); ++i ) {
        for( u32 lay = 0; lay < 3; ++lay ) {
            for ( u32 x = 0; x < chunks[i].GetSize(); ++x ) {
                for( u32 y = 0; y < size.y; ++y ) {
                    UpdateTileLocal( x + i * 32, y, (Layer)lay );
                }
                uf( (i + (lay + x / (float)chunks[i].GetSize()) / 3.f) / (float)chunks.size() );
            }
        }
    }
}

void Map::WriteHeader( FileStream& fs ) {
    char header1[] = "water:D";
    u32 header2[] = { size.x, size.y };
    fs.SeekP( 0, std::ios_base::beg );
    fs.WriteBinary( header1, 7 * sizeof(char) );
    fs.WriteBinary( header2, 2 * sizeof(u32) );
}

void Map::ReadHeader( FileStream& fs ) {
    std::vector<char> header1 = fs.ReadBinary<char>( 7 );
    std::vector<char> cmp = { 'w', 'a', 't', 'e', 'r', ':', 'D' };
    for( u32 i = 0; i < 7; ++i ) {
        if( header1[i] != cmp[i] )
            Error( "The map file you are trying to read is not a valid Water map file" );
    }
    std::vector<u32> header2 = fs.ReadBinary<u32>( 2 );
    size.x = header2[0];
    size.y = header2[1];
}

void Map::SetFolder( const std::string& f ) {
    folder = f;
    if( !FileStream::Exists( folder + "map" ) )
        FileStream::Create( folder + "map" );
}

std::string Map::GetNewMapFolder(){
    string dirpath = "data/saves/";
    DIR* pDir = NULL;
    dirent* pDirent = NULL;
    std::list< int > names;
    pDir = opendir( dirpath.c_str() );
    if( !pDir )
        Error( "Could not open directory '%s'", dirpath.c_str() );
    while( (pDirent = readdir( pDir )) ) {
        if( !pDirent )
            Error( "Error while reading directory '%s'", dirpath.c_str() );
        string name = pDirent->d_name;
        if( name != "." && name != ".." )
            names.push_back( StringToInt( pDirent->d_name ) );
    }
    names.sort();
    string name;
    if( names.empty() )
        name = "0";
    else
        name = IntToString( names.back() + 1 );
    mkdir( (dirpath + name).c_str() );
    closedir( pDir );
    return dirpath + name + '/';
}

vec2ui Map::ToGlobal( const vec2i& coords ) {
    return vec2ui(coords.x + offset, coords.y);
}

vec2ui Map::ToGlobal( int x, int y ) {
    return vec2ui(x + offset, y);
}

u32 Map::ToGlobal( int x ) {
    return x + offset;
}

vec2i Map::ToLocal( const vec2ui& coords ) {
    return vec2i(coords.x - offset, coords.y);
}

vec2i Map::ToLocal( u32 x, u32 y ) {
    return vec2i(x - offset, y);
}

int Map::ToLocal( u32 x ) {
    return x - offset;
}

u32 Map::GetCurrentOffset() const {
    return offset;
}

const vec2ui& Map::GetSize() const {
    return size;
}

Tile* Map::GetTile( u32 x, u32 y, Layer l ) {
    return GetTileLocal( ToLocal(x), y, l );
}

Tile* Map::GetTileLocal( u32 x, u32 y, Layer l ) {
    u32 chunk = x / 32;
    if( chunk >= nbrChunksLoaded )
        Error( "Trying to access a tile from a non-loaded chunk (%d %d)", x, y );
    return chunks[chunk].GetLayer(l)[x % 32][y];
}

void Map::UpdateTile( u32 x, u32 y, Layer l ) {
    UpdateTileLocal( ToLocal(x), y, l );
}

void Map::UpdateTileLocal( u32 x, u32 y, Layer l ) {
    if( x < size.x && y < size.y ) {
        Tile* t = GetTileLocal( x, y, l );
        t->placement = TP_None;
        int tileID = t->GetTileID();
        std::vector< vec2i > toCheck;
        if( x != 0 && x != size.x - 1 && y != 0 && y != size.y - 1) {
            toCheck.push_back( vec2i( x - 1, y ) );
            toCheck.push_back( vec2i( x, y - 1 ) );
            toCheck.push_back( vec2i( x, y + 1 ) );
            toCheck.push_back( vec2i( x + 1, y ) );
        }
        else {
            if( x == 0 )
                toCheck.push_back( vec2i( x + 1, y ) );
            else if( x == size.x - 1 )
                toCheck.push_back( vec2i( x - 1, y ) );
            else {
                toCheck.push_back( vec2i( x + 1, y ) );
                toCheck.push_back( vec2i( x - 1, y ) );
            }

            if( y == 0 )
                toCheck.push_back( vec2i( x, y + 1 ) );
            else if( y == size.y - 1 )
                toCheck.push_back( vec2i( x, y - 1 ) );
            else {
                toCheck.push_back( vec2i( x, y + 1 ) );
                toCheck.push_back( vec2i( x, y - 1 ) );
            }
        }
        for( u32 i = 0; i < toCheck.size(); ++i ) {
            if( toCheck[i].x < 32 * nbrChunksLoaded && toCheck[i].x > 0 ) {
                if( GetTileLocal(toCheck[i].x, toCheck[i].y, l )->GetTileID() == tileID ) {
                    if( toCheck[i].x == (int)x - 1 )
                        t->placement |= TP_Left;
                    if( toCheck[i].x == (int)x + 1 )
                        t->placement |= TP_Right;
                    if( toCheck[i].y == (int)y - 1 )
                        t->placement |= TP_Top;
                    if( toCheck[i].y == (int)y + 1 )
                        t->placement |= TP_Bottom;
                }
            }
        }
        //  Final step, do the actual appearance change
        t->SetPlacement();
    }
    else
        Warning( "Trying to access a non-existing tile (%d, %d)", x, y );
}

Rectui Map::GetRenderZone() {
    vec2i curPos = (Global::camera->GetPosition() + *Global::winSz / 2) / Map::TileSize;
    vec2i halfSz(renderZoneSz / 2);
    Rectui renderZone( curPos - halfSz, curPos + halfSz );
    if( curPos.x - halfSz.x < 0 ) {
        renderZone.SetLeft( 0 );
        renderZone.SetRight( curPos.x + halfSz.x );
    }
    if( curPos.y - halfSz.y < 0 ) {
        renderZone.SetTop( 0 );
        renderZone.SetBottom ( curPos.y + halfSz.y );
    }
    if( curPos.x + halfSz.x >= size.x ) {
        renderZone.SetRight( size.x - 1 );
        renderZone.SetLeft( curPos.x - halfSz.x );
    }
    if( curPos.y + halfSz.y >= size.y ) {
        renderZone.SetBottom( size.y - 1 );
        renderZone.SetTop( curPos.y - halfSz.y );
    }
    return renderZone;
}

void Map::Update() {
    Rectui renderZone = GetRenderZone();
    vec2i curPos = (Global::camera->GetPosition() + *Global::winSz / 2) / Map::TileSize;

    //  First we need to determine if new chunks should be loaded and others unloaded
    int chunkOffset = curPos.x / 32 - lastCurrentPos.x / 32;
    u32 newChunkSz = 32;
    //  Check if we are already at the beginning or at the end of the map, to avoid loading problems
    bool atLastChunk = (currentLastChunk == totalChunks - 1);
    bool atFirstChunk = (currentFirstChunk == 0);

    if( chunkOffset > 0 ) {
        //  This means we have moved one chunk to the right
        if( currentLastChunk + chunkOffset >= totalChunks ) {
            currentLastChunk = totalChunks - 1;
            newChunkSz = lastChunkSz;
        }
        FileStream fs( folder + "map", OM_Modify );
        if( currentLastChunk - currentFirstChunk + 1 == nbrChunksLoaded ) {
            if( chunks.front().WasModified() ) {
                Info( "Saving chunk %d", currentFirstChunk );
                chunks.front().Save( fs );
            }
            Info( "Unloading chunk %d", currentFirstChunk );
            Global::world->RemoveChunkBoxes( Left, Map::ChunkSize );
            chunks.front().Destroy();
            chunks.pop_front();
            offset += 32;
            currentFirstChunk += chunkOffset;
        }
        if( !atLastChunk ) {
            //  Make sure we are not already at the last chunk then load a new one on the right
            Global::world->AddChunkBoxes( Right, newChunkSz );
            currentLastChunk += chunkOffset;
            Info( "Loading chunk %d", currentLastChunk );
            chunks.push_back( Chunk() );
            chunks.back().Load( fs, currentLastChunk * 32, newChunkSz );
        }
    }
    else if( chunkOffset < 0 ) {
        //  This means we have moved one chunk to the left
        u32 rightChunkSz = 32;
        if( atLastChunk )
            rightChunkSz = lastChunkSz;
        if( (int)currentFirstChunk + chunkOffset < 0 )
            currentFirstChunk = 0;
        FileStream fs( folder + "map", OM_Modify );
        if( currentLastChunk - currentFirstChunk + 1 == nbrChunksLoaded ) {
            if( chunks.back().WasModified() ) {
                Info( "Saving chunk %d", currentLastChunk )
                chunks.back().Save( fs );
            }
            Info( "Unloading chunk %d", currentLastChunk );
            Global::world->RemoveChunkBoxes( Right, rightChunkSz );
            chunks.back().Destroy();
            chunks.pop_back();
            currentLastChunk += chunkOffset;
        }
        if( !atFirstChunk ) {
            Global::world->AddChunkBoxes( Left, Map::ChunkSize );
            //  Make sure we are not already at the first chunk and load a new one on the left
            currentFirstChunk += chunkOffset;
            Info( "Loading chunk %d", currentFirstChunk );
            chunks.push_front( Chunk() );
            offset -= 32;
            chunks.front().Load( fs, currentFirstChunk * 32, 32 );
        }
    }

    //  Offset from the last position to the current one in tile units
    vec2i off(curPos - lastCurrentPos);

    bool horizontal = false, vertical = false;
    if( off.x != 0 )
        horizontal = true;
    if( off.y != 0 )
        vertical = true;


    //  This function calculates which tiles should be drawn and which ones should stop.
    //  If we moved horizontally :
    u32 top = renderZone.GetTop();
    u32 bottom = renderZone.GetBottom();
    u32 left = renderZone.GetLeft();
    u32 right = renderZone.GetRight();
    u32 oldTop = lastRenderZone.GetTop();
    u32 oldBottom = lastRenderZone.GetBottom();
    u32 oldLeft = lastRenderZone.GetLeft();
    u32 oldRight = lastRenderZone.GetRight();

    //  Handle the case where the player moved too fast/teleported
    if( (u32)abs(off.x) > renderZoneSz.x / 2 || (u32)abs(off.y) > renderZoneSz.y / 2 || firstDraw) {
        if( !firstDraw ) {
            for( u32 i = oldLeft; i <= oldRight; ++i ) {
                for( u32 j = oldTop; j <= oldBottom; ++j ) {
                    GetTile( i, j, Back )->StopDrawing();
                    GetTile( i, j, Middle )->StopDrawing();
                    GetTile( i, j, Front )->StopDrawing();
                }
            }
        }
        else
            firstDraw = false;

        for( u32 i = renderZone.GetLeft(); i <= renderZone.GetRight(); ++i ) {
            for( u32 j = renderZone.GetTop(); j <= renderZone.GetBottom(); ++j ) {
                GetTile( i, j, Back )->Draw();
                GetTile( i, j, Middle )->Draw();
                GetTile( i, j, Front )->Draw();
            }
        }
    }

    //  Handle the case of a normal relatively slow movement of the player
    else {
        //  If we moved horizontally
        if( horizontal ) {
            if( off.x > 0 ) {
                //  If we moved to the right
                //  Take all the tiles to the left of the current render zone that are drawn
                if( (int)(left) - off.x >= 0 ) {
                    for( u32 i = left - off.x; i < left; ++i ) {
                        for( u32 j = oldTop; j <= oldBottom; ++j ) {
                            //  And stop drawing them
                            GetTile( i, j, Back )->StopDrawing();
                            GetTile( i, j, Middle )->StopDrawing();
                            GetTile( i, j, Front )->StopDrawing();
                        }
                    }
                }
                //  Take all the tiles in our current render zone that aren't drawn
                //  Making sure we are not already on the right edge of the map
                if( (int)right + off.x < size.x || (right - left == renderZoneSz.x) ) {
                    for( u32 i = right - off.x + 1; i <= right; ++i ) {
                        for( u32 j = oldTop; j <= oldBottom; ++j ) {
                            //  And draw them
                            GetTile( i, j, Back )->Draw();
                            GetTile( i, j, Middle )->Draw();
                            GetTile( i, j, Front )->Draw();
                        }
                    }
                }
            }
            else {
                //  If we moved to the left
                if( left < size.x - renderZoneSz.x - 1 ) {
                    //  Take all the tiles to the right of the current render zone that are drawn
                    for( u32 i = right + 1; i <= right - off.x; ++i ) {
                        for( u32 j = oldTop; j <= oldBottom; ++j ) {
                            //  And stop drawing them
                            GetTile( i, j, Back )->StopDrawing();
                            GetTile( i, j, Middle )->StopDrawing();
                            GetTile( i, j, Front )->StopDrawing();
                        }
                    }
                }
                if( left != 0 || right == renderZoneSz.x ) {
                    //  Take all the tiles in our current render zone that aren't drawn
                    for( u32 i = left; i < left - off.x; ++i ) {
                        for( u32 j = oldTop; j <= oldBottom; ++j ) {
                            //  And draw them
                            GetTile( i, j, Back )->Draw();
                            GetTile( i, j, Middle )->Draw();
                            GetTile( i, j, Front )->Draw();
                        }
                    }
                }
            }
        }
        //  If we moved vertically
        if( vertical ) {
            //  If we moved towards the bottom of the map
            if( off.y > 0 ) {
                if( top != 0 || bottom - top == renderZoneSz.y ) {
                    for( u32 i = top - off.y; i < top; ++i ) {
                        for( u32 j = left; j <= right; ++j ) {
                            //  Stop drawing those tiles
                            GetTile( j, i, Back )->StopDrawing();
                            GetTile( j, i, Middle )->StopDrawing();
                            GetTile( j, i, Front )->StopDrawing();
                        }
                    }
                }
                //  Make sure we are not at the bottom of the map
                if( (int)bottom + off.y < size.y ) {
                    for( u32 i = bottom - off.y + 1; i <= bottom; ++i ) {
                        for( u32 j = left; j <= right; ++j ) {
                            //  Draw those tiles
                            GetTile( j, i, Back )->Draw();
                            GetTile( j, i, Middle )->Draw();
                            GetTile( j, i, Front )->Draw();
                        }
                    }
                }
            }
            else {
                //  If we moved towards the top of the map
                for( u32 i = bottom + 1; i <= bottom - off.y; ++i ) {
                    for( u32 j = left; j <= right; ++j ) {
                        //  Stop drawing those tiles
                        GetTile( j, i, Back )->StopDrawing();
                        GetTile( j, i, Middle )->StopDrawing();
                        GetTile( j, i, Front )->StopDrawing();
                    }
                }
                //  Make sure we are not at the top of the map
                if( (int)top + off.y >= 0 ) {
                    for( u32 i = top; i < top - off.y; ++i ) {
                        for( u32 j = left; j <= right; ++j ) {
                            //  Draw those tiles
                            GetTile( j, i, Back )->Draw();
                            GetTile( j, i, Middle )->Draw();
                            GetTile( j, i, Front )->Draw();
                        }
                    }
                }
            }
        }
    }
    lastCurrentPos = curPos;
    lastRenderZone = renderZone;
}

void Map::SetRenderZoneSize( const vec2i& sz ) {
    renderZoneSz = sz;
    if( sz.x % 2 == 0 ) {
        Warning( "Render zone size x should be an odd number" );
        renderZoneSz.x++;
    }
    if( sz.y % 2 == 0 ) {
        Warning( "Render zone size y should be an odd number" );
        renderZoneSz.y++;
    }
    if( sz.x * 10 > (int)size.x ) {
        Warning( "Render zone x is greater than the map size" );
        renderZoneSz.x = size.x / 10;
    }
    if( sz.y * 10 > (int)size.y ) {
        Warning( "Render zone y is greater than the map size" );
        renderZoneSz.y = size.y / 10;
    }
}
