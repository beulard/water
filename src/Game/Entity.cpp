#include "Entity.hpp"
#include "DataLoader.hpp"
#include "Quad.hpp"

Entity::Entity() : type( ET_Sprite ), current(NULL), data(NULL), state(ES_IdleLeft), trueSize(vec2f(0.f, 0.f)), currentIndex(0), initialized( false ) {
    SetRenderMode( RM_None );
    depth = RD_MiddleLayer;
    box.dynamic = true;
    box.halfextents = vec2f( 25, 25 );
    box.center = position + box.halfextents;
}

Entity::Entity( const Entity& e ) : Drawable( e ), initialized(false) {
    *this = e;
}

void Entity::operator=( const Entity& e ) {
    if( drawn )
        StopDrawing();
    if( initialized )
        delete current;
    initialized = false;
    currentIndex = e.currentIndex;
    if( e.initialized )
        SetData( *e.data );
    position = vec2i( 0, 0 );
    type = e.type;

    box = e.box;
    boxOffset = e.boxOffset;
    trueSize = e.trueSize;
    SetPosition( e.position );

}

Entity::~Entity(){
    if( initialized )
        delete current;
}

void Entity::SetData( EntityData& ed ) {
    data = &ed;
    type = data->type;
    if( initialized )
        delete current;
    if( data->type == ET_Sprite )
        current = new Sprite( *dynamic_cast< Sprite* >(data->d[currentIndex]) );
    else if( data->type == ET_Anim )
        current = new Anim( *dynamic_cast< Anim* >(data->d[currentIndex]) );
    else if( data->type == ET_Quad )
        current = new Quad( *dynamic_cast< Quad* >(data->d[currentIndex]) );
    initialized = true;
}

void Entity::SetDrawableIndex( u32 index ) {
    if( initialized ) {
        if( current ) {
            delete current;
        }
        if( type == ET_Sprite )
            current = new Sprite( *dynamic_cast< Sprite* >(data->d[index]) );
        else if( type == ET_Anim )
            current = new Anim( *dynamic_cast< Anim* >(data->d[index]) );
        else if( type == ET_Quad )
            current = new Quad( *dynamic_cast< Quad* >(data->d[index] ) );
        if( drawn )
            current->Draw();
        currentIndex = index;
    }
}

void Entity::Update() {
    if( type == ET_Anim ) {
        Anim* curAnim = dynamic_cast< Anim* >( current );
        u32 curFrame = curAnim->GetCurrentFrame();

        if( curAnim->HasChangedFrame() ) {
            if( data->boxSizes[currentIndex][curFrame].first ) {
                vec2f newExtents = data->boxSizes[currentIndex][curFrame].second / 2.f;
                vec2f diff = box.halfextents - newExtents;
                box.center -= diff;
                box.halfextents = newExtents;
            }
            else
                box.halfextents = trueSize / 2.f;
        }
        if( data->boxOffsets[currentIndex][curFrame].first ) {
            position = box.center - box.halfextents - data->boxOffsets[currentIndex][curFrame].second;
        }

        else
            position = box.center - box.halfextents - boxOffset;

    }
    else
        position = box.center - box.halfextents - boxOffset;

    //  Did the entity change state ?
    EntityState newState = state;
    if( box.velocity.x > 0 ) {
        if( state != ES_RunRight )
            newState = ES_RunRight;
    }
    else if( box.velocity.x < 0 ) {
        if( state != ES_RunLeft )
            newState = ES_RunLeft;
    }
    else {
        if( state != ES_IdleRight || state != ES_IdleLeft ) {
            if( state == ES_RunRight || state == ES_CrouchRight || state == ES_JumpRight )
                newState = ES_IdleRight;
            else if( state == ES_RunLeft || state == ES_CrouchLeft || state == ES_JumpLeft )
                newState = ES_IdleLeft;
        }
    }
    if( newState != state ) {
        state = newState;
        SetDrawableIndex( state );
    }


    current->SetPosition( position );
}

void Entity::Draw() {
    if( !drawn )
        current->Draw();
    else
        Warning( "Trying to add an entity to the renderer twice" );
    drawn = true;
}

void Entity::StopDrawing() {
    if( drawn )
        current->StopDrawing();
    else
        Warning( "Trying to remove an entity from the renderer but it is not in" );
    drawn = false;
}

const vec2i& Entity::GetPosition() const {
    return position;
}

void Entity::SetPosition( const vec2i& pos ) {
    vec2i offset = pos - position;
    Move( offset );
}

void Entity::SetSize( const vec2ui& sz ) {
    box.halfextents = vec2f( sz.x / 2.f, sz.y / 2.f );
    if( initialized )
        current->SetSize( sz );
    trueSize = sz;
    size = sz;
}

void Entity::SetSpeed( float s ) {
    box.SetSpeed( s );
}

void Entity::SetDynamic( bool d ) {
    box.dynamic = d;
}

void Entity::Move( const vec2i& offset ) {
    position += offset;
    box.center = position + box.halfextents;
    if( initialized )
        current->SetPosition( position );
}

void Entity::SetCollisionType( CollisionType t ) {
    box.ct = t;
}

const CollisionType& Entity::GetCollisionType() const {
    return box.ct;
}

EntityType Entity::GetType()    const {
    return type;
}

AABB& Entity::GetBox() {
    return box;
}

void Entity::SetBoxOffset( const vec2i& offset ) {
    boxOffset = offset;
}

const vec2i& Entity::GetBoxOffset() const {
    return boxOffset;
}

