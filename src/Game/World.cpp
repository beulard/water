#include "World.hpp"
#include "Game.hpp"

const float World::Gravity = .5f;

World::World() {
    accumulator.SetLoop( true );
    accumulator.SetTimer( Game::SimTime );
    accumulator.Start();
}

World::~World(){

}

void World::SetPlayerBox( AABB& box ) {
    playerBox = &box;
    AddDynamicBox( box );
}

void World::AddStaticBox( AABB& box ) {
    staticBoxes.push_back( &box );
    allBoxes.push_back( &box );
}

void World::AddDynamicBox( AABB& box ) {
    dynamicBoxes.push_back( &box );
    allBoxes.push_back( &box );
}

void World::AddMapBox( AABB& box ) {
    mapBoxes.push_back( &box );
    staticBoxes.push_back( &box );
}

void World::RemoveStaticBox( AABB& box ) {
    for( u32 i = 0; i < staticBoxes.size(); ++i ) {
        if( staticBoxes[i] == &box ) {
            //  Swap the last item with the one deleted, then delete the last item
            staticBoxes[i] = staticBoxes[staticBoxes.size() - 1];
            staticBoxes.pop_back();
        }
    }
    for( u32 i = 0; i < allBoxes.size(); ++i ) {
        if( allBoxes[i] == &box ) {
            allBoxes[i] = allBoxes[allBoxes.size() - 1];
            allBoxes.pop_back();
        }
    }
}

void World::RemoveMapBox( AABB& box ) {
    for( u32 i = 0; i < mapBoxes.size(); ++i ) {
        if( mapBoxes[i] == &box ) {
            mapBoxes[i] = mapBoxes[mapBoxes.size() - 1];
            mapBoxes.pop_back();
        }
    }
    for( u32 i = 0; i < staticBoxes.size(); ++i ) {
        if( staticBoxes[i] == &box ) {
            staticBoxes[i] = staticBoxes[staticBoxes.size() - 1];
            staticBoxes.pop_back();
        }
    }
}

void World::RemoveDynamicBox( AABB& box ) {
    for( u32 i = 0; i < dynamicBoxes.size(); ++i ) {
        if( staticBoxes[i] == &box ) {
            dynamicBoxes[i] = dynamicBoxes[dynamicBoxes.size() - 1];
            dynamicBoxes.pop_back();
        }
    }
    for( u32 i = 0; i < allBoxes.size(); ++i ) {
        if( allBoxes[i] == &box ) {
            allBoxes[i] = allBoxes[allBoxes.size() - 1];
            allBoxes.pop_back();
        }
    }
}

void World::ClearStaticBoxes() {
    staticBoxes.clear();
}

void World::ClearMapBoxes() {
    mapBoxes.clear();
}

void World::ClearDynamicBoxes() {
    dynamicBoxes.clear();
    //  Reset the player box : it should not change
    SetPlayerBox( Global::Water.soup.GetBox() );
}

void World::AddChunkBoxes( Side s, u32 sz ) {
    if( s == Right ) {
        for( u32 i = 0; i < sz * Map::SizeY; ++i ) {
            mapBoxes.push_back( NULL );
        }
    }
    else {
        for( u32 i = 0; i < sz * Map::SizeY; ++i ) {
            mapBoxes.push_front( NULL );
        }
    }
}

void World::RemoveChunkBoxes( Side s, u32 sz ) {
    if( s == Right ) {
        for( u32 i = 0; i < sz * Map::SizeY; ++i ) {
            mapBoxes.pop_back();
        }
    }
    else {
        for( u32 i = 0; i < sz * Map::SizeY; ++i ) {
            mapBoxes.pop_front();
        }
    }
}

void World::SetMapBox( const vec2ui& coords, AABB& box ) {
    SetMapBox( coords.y + coords.x * Map::SizeY, box );
}

void World::SetMapBox( u32 id, AABB& box ) {
    mapBoxes[id] = &box;
}

void World::Clean() {
    map.Clean();
}

void World::Update() {
    if( accumulator.HasEnded() ) {
        for( u32 i = 0; i < dynamicBoxes.size(); ++i ) {
            dynamicBoxes[i]->velocity.y += World::Gravity;

            dynamicBoxes[i]->onGround = false;
            dynamicBoxes[i]->underWater = false;

//            if( dynamicBoxes[i]->state & BS_MoveRight )
//                dynamicBoxes[i]->acceleration.x = 1.f;
//            if( dynamicBoxes[i]->state & BS_MoveLeft )
//                dynamicBoxes[i]->acceleration.x = -1.f;
//            if( !( dynamicBoxes[i]->state & BS_MoveLeft ) && !( dynamicBoxes[i]->state & BS_MoveRight ) )
//                dynamicBoxes[i]->acceleration.x = 0.f;
//            else if( ( dynamicBoxes[i]->state & BS_MoveLeft ) && ( dynamicBoxes[i]->state & BS_MoveRight ) )
//                dynamicBoxes[i]->acceleration.x = 0.f;

            dynamicBoxes[i]->velocity.x = dynamicBoxes[i]->GetNewVelX();

            //if( dynamicBoxes[i]->acceleration.x == 0.f )
            //    dynamicBoxes[i]->velocity.x /= 1.09f;

            //dynamicBoxes[i]->velocity += dynamicBoxes[i]->acceleration;
        }

        for( u32 i = 0; i < dynamicBoxes.size(); ++i ) {
            AABB* b1 = dynamicBoxes[i];
            AABB nextBox( b1->center + b1->velocity, b1->halfextents );
            vec2i start( vec2i(nextBox.center - nextBox.halfextents) / Map::TileSize - vec2i( 1, 1 ) );
            vec2i end( vec2i(nextBox.center + nextBox.halfextents) / Map::TileSize + vec2i( 1, 1 ) );
            for( int j = start.y; j < end.y; ++j ) {
                if( j >= 0 && j < (int)map.GetSize().y ) {
                    for( int k = start.x; k < end.x; ++k ) {
                        if( k >= 0 && k < (int)map.GetSize().x ) {
                            AABB* b2 = mapBoxes[j + map.GetSize().y * map.ToLocal(k)];
                            if( b1->ct != CT_None && b2->ct != CT_None ) {
                                if( b1->Collides( *b2 ) ) {
                                    result coll = b1->GetResult( *b2 );

                                    if( b2->ct == CT_Liquid )
                                        b1->underWater = true;
                                    else
                                        b1->center += coll.mtv;
                                }
                            }
                        }
                    }
                }
            }
            for( u32 j = 0; j < dynamicBoxes.size(); ++j ) {
                if( i != j ) {
                    AABB* b2 = dynamicBoxes[j];
                    if( b1->Collides( *b2 ) ) {
                        if( b1->ct != CT_None && b2->ct != CT_None ) {
                            result coll = b1->GetResult( *b2 );

                            if( b2->ct == CT_Liquid )
                                b1->underWater = true;
                            else
                                b1->center += coll.mtv;
                        }
                    }
                }
            }
        }



        for( u32 i = 0; i < dynamicBoxes.size(); ++i ) {
            //  If we detect the object was not on the ground, it goes free falling and can't jump anymore.
            if( !dynamicBoxes[i]->onGround ) {
                if( !dynamicBoxes[i]->jumping )
                    dynamicBoxes[i]->jumping = true;
            }
            if( dynamicBoxes[i]->onGround && dynamicBoxes[i]->jumping )
                dynamicBoxes[i]->jumping = false;

            if( !dynamicBoxes[i]->jumping )
                dynamicBoxes[i]->velocity.y = 0.f;

            if( dynamicBoxes[i]->underWater )
                dynamicBoxes[i]->velocity /= 1.05f;

            dynamicBoxes[i]->center += dynamicBoxes[i]->velocity;

            if( dynamicBoxes[i]->jump ) {
                dynamicBoxes[i]->jump = false;
                if( !dynamicBoxes[i]->jumping ) {
                    dynamicBoxes[i]->jumping = true;
                    dynamicBoxes[i]->velocity.y = -15.f;
                }
            }
        }
    }
}
