#ifndef WATER_TILE
#define WATER_TILE
#include "Drawable.hpp"
#include "AABB.hpp"
#include "Entity.hpp"
#include <map>

//  This will define if tiles of the same type are placed on any side of this one
enum TilePlacement {
    TP_None =  0x0,
    TP_Left =  0x1,
    TP_Top =   0x2,
    TP_Right = 0x4,
    TP_Bottom =   0x8
};

class Tile : public Entity {
    public:
        Tile();
        void operator=( const Tile& t );
        //  Recreate the tile with a tile ID
        void operator=( u32 id );
        Tile( const Tile& tile );
        virtual ~Tile();

        void SetTileID( int id );
        int GetTileID() const;

        void SetMinTemp( int t );
        void SetMaxTemp( int t );
        void SetMinHum( int h );
        void SetMaxHum( int h );

        int GetMinTemp()    const;
        int GetMaxTemp()    const;
        int GetMinHum() const;
        int GetMaxHum() const;

        //  Sets the appearance of the tile depending on the placement variable
        void SetPlacement();

        int placement;

        //  All tiles with the ID it is associated to when loading it
        static std::map< u32, Tile* > Tiles;

    protected:

        //  Identifies the type of the tile (earth, air, ...)
        int tileID;

        int minTemp;
        int maxTemp;
        int minHum;
        int maxHum;
};

#endif // WATER_TILE
