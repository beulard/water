#ifndef WATER_MASTERANIM
#define WATER_MASTERANIM
#include "Anim.hpp"

class MasterAnim {
	public:
		MasterAnim();
		~MasterAnim();

        void Clean();

		void AddAnim( Anim& anim );
		void RemoveAnim( int id );

		void Update();

	private:
		std::vector< Anim* > anims;
};

#endif // WATER_MASTERANIM
