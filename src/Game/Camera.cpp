#include "Camera.hpp"
#include "Common.hpp"

Camera::Camera() {

}

Camera::~Camera(){

}

void Camera::Move( const vec2i& offset ) {
    glPosition.x += offset.x;
    glPosition.y += offset.y;
    position.x += offset.x;
    position.y -= offset.y;
}

void Camera::SetPosition( const vec2i& pos ) {
    glPosition.x = pos.x;
    glPosition.y = pos.y + Global::winSz->y;
    position.x = pos.x;
    position.y = pos.y;
}

void Camera::SetCenter( const vec2i& pos ) {
    SetPosition( pos - *Global::winSz / 2 );
}

const vec2i& Camera::GetPosition()   const {
    return position;
}

const vec2i& Camera::GetGLPosition() const {
    return glPosition;
}
