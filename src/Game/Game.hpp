#ifndef WATER_GAME
#define WATER_GAME

#include "DataLoader.hpp"
#include "Renderer.hpp"
#include "Mouse.hpp"
#include "MasterTimer.hpp"
#include "MasterAnim.hpp"
#include "RootWidget.hpp"
#include "Camera.hpp"
#include "InputManager.hpp"
#include "World.hpp"
#include "Sprite.hpp"

enum GameState {
    GS_Game,
    GS_Menu,
    GS_PreLoading,
    GS_Loading,
    GS_PostLoading,
    GS_MainMenu
};

class Game
{
    public:
        Game();
        ~Game();

        //  Returns 0 on success, 1 on failure
        bool Init();

        //  Returns 0 on a normal game close, 1 on an unexpected close
        bool Run();

        //  Will try to free all allocated memory and destroy every object used by the game
        void Clean();

        //  Will try to exit the game instantly. Must be used in the case an error occured, not at the end of the game
        void Exit();

        static bool Success;
        static bool Failure;

        //  Screen update rate
        const static float FrameTime;
        //  Physics simulation update rate
        const static float SimTime;

        const vec2i& GetWindowSize() const;
        const vec2i& GetCameraPosition() const;
        const vec2i& GetCameraGLPosition() const;

        DataLoader      dataLoader;
        Renderer        renderer;
        Mouse           mouse;
        MasterTimer     masterTimer;
        MasterAnim      masterAnim;
        RootWidget      rootWidget;
        Camera          camera;
        InputManager    inputMgr;
        World           world;
        Entity          soup;

        struct {
            short Major,
                  Minor,
                  Revision;
        } Version;

        #if defined WATER_WINDOWS
        void* handle;

        #elif defined WATER_LINUX
        unsigned long handle;
        #endif


        //  Projection matrix used for rendering.
        GLfloat projMatrix[9];

        FT_Library ft;

        GameState state;
        GameState lastState;
        bool running;

    private:
        vec2i windowSize;

};

#endif // WATER_GAME
