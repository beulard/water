#ifndef WATER_TIMER
#define WATER_TIMER

class Timer
{
    public:
        Timer();
        Timer( float t );
        ~Timer();

        //  This function parameter takes a time in seconds
        void SetTimer( float t );
        void Pause();
        void Start();
        void Stop();
        void Reset();
        void Toggle();

        //  If the timer is looped, the ended flag will be set for exactly one loop iteration. This is used in the World class
        bool HasEnded() const;
        float GetTime() const;
        void SetLoop( bool l );

        void SetID( int i );

        //  Takes the time since the last call to MasterTimer::Update
        void Update( float time );

    private:
        float startTime;
        float endTime;
        //  Unlike startTime and endTime, this is a relative value
        float currentTime;

        bool running;
        bool ended;
        bool loop;

        //  ID in the MasterTimer class
        int id;
};

#endif // WATER_TIMER
