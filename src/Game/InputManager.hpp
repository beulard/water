#ifndef WATER_INPUTMANAGER
#define WATER_INPUTMANAGER
#include <map>
#include "vec2.hpp"

typedef unsigned int u32;

enum InputType {
    IT_None,
    IT_Press,
    IT_Release
};

class State {
    public :
        State();
        State( InputType cur );

        InputType currentState;
        InputType lastState;
};


enum KeyCode {
    KC_a = 'a',
    KC_b = 'b',
    KC_c = 'c',
    KC_d = 'd',
    KC_e = 'e',
    KC_f = 'f',
    KC_g = 'g',
    KC_h = 'h',
    KC_i = 'i',
    KC_j = 'j',
    KC_k = 'k',
    KC_l = 'l',
    KC_m = 'm',
    KC_n = 'n',
    KC_o = 'o',
    KC_p = 'p',
    KC_q = 'q',
    KC_r = 'r',
    KC_s = 's',
    KC_t = 't',
    KC_u = 'u',
    KC_v = 'v',
    KC_w = 'w',
    KC_x = 'x',
    KC_y = 'y',
    KC_z = 'z',

    KC_A = 'A',
    KC_B = 'B',
    KC_C = 'C',
    KC_D = 'D',
    KC_E = 'E',
    KC_F = 'F',
    KC_G = 'G',
    KC_H = 'H',
    KC_I = 'I',
    KC_J = 'J',
    KC_K = 'K',
    KC_L = 'L',
    KC_M = 'M',
    KC_N = 'N',
    KC_O = 'O',
    KC_P = 'P',
    KC_Q = 'Q',
    KC_R = 'R',
    KC_S = 'S',
    KC_T = 'T',
    KC_U = 'U',
    KC_V = 'V',
    KC_W = 'W',
    KC_X = 'X',
    KC_Y = 'Y',
    KC_Z = 'Z',

    KC_Space = 32,
    KC_Escape = 257,
    KC_F1 = 258,
    KC_F2 = 259,
    KC_F3 = 260,
    KC_F4 = 261,
    KC_F5 = 262,
    KC_F6 = 263,
    KC_F7 = 264,
    KC_F8 = 265,
    KC_F9 = 266,
    KC_F10 = 267,
    KC_F11 = 268,
    KC_F12 = 269,
    KC_F13 = 270,
    KC_F14 = 271,
    KC_F15 = 272,
    KC_F16 = 273,
    KC_F17 = 274,
    KC_F18 = 275,
    KC_F19 = 276,
    KC_F20 = 277,
    KC_F21 = 278,
    KC_F22 = 279,
    KC_F23 = 280,
    KC_F24 = 281,
    KC_F25 = 282,

    KC_Up = 283,
    KC_Down = 284,
    KC_Left = 285,
    KC_Right = 286,
    KC_LeftShift = 287,
    KC_RightShift = 288,
    KC_LeftControl = 289,
    KC_RightControl = 290,
    KC_LeftAlt = 291,
    KC_RightAlt = 292,
    KC_Tab = 293,
    KC_Enter = 294,
    KC_Backspace = 295,
    KC_Insert = 296,
    KC_Delete = 297,
    KC_PageUp = 298,
    KC_PageDown = 299,
    KC_Home = 300,
    KC_End = 301,

    KC_NumPad0 = 302,
    KC_NumPad1 = 303,
    KC_NumPad2 = 304,
    KC_NumPad3 = 305,
    KC_NumPad4 = 306,
    KC_NumPad5 = 307,
    KC_NumPad6 = 308,
    KC_NumPad7 = 309,
    KC_NumPad8 = 310,
    KC_NumPad9 = 311,
    KC_NumPadDiv = 312,
    KC_NumPadMul = 313,
    KC_NumPadSub = 314,
    KC_NumPadAdd = 315,
    KC_NumPadPoint = 316,
    KC_NumPadEqual = 317,
    KC_NumPadEnter = 318,
    KC_NumPadLock = 319,

    KC_CapsLock = 320,
    KC_ScrollLock = 321,
    KC_Pause = 322,
    KC_LeftSuper = 323,
    KC_RightSuper = 324,
    KC_Menu = 325
};


const u32 keys[122] = {
    KC_a,
    KC_b,
    KC_c,
    KC_d,
    KC_e,
    KC_f,
    KC_g,
    KC_h,
    KC_i,
    KC_j,
    KC_k,
    KC_l,
    KC_m,
    KC_n,
    KC_o,
    KC_p,
    KC_q,
    KC_r,
    KC_s,
    KC_t,
    KC_u,
    KC_v,
    KC_w,
    KC_x,
    KC_y,
    KC_z,

    KC_A,
    KC_B,
    KC_C,
    KC_D,
    KC_E,
    KC_F,
    KC_G,
    KC_H,
    KC_I,
    KC_J,
    KC_K,
    KC_L,
    KC_M,
    KC_N,
    KC_O,
    KC_P,
    KC_Q,
    KC_R,
    KC_S,
    KC_T,
    KC_U,
    KC_V,
    KC_W,
    KC_X,
    KC_Y,
    KC_Z,

    KC_Space,
    KC_Escape,
    KC_F1,
    KC_F2,
    KC_F3,
    KC_F4,
    KC_F5,
    KC_F6,
    KC_F7,
    KC_F8,
    KC_F9,
    KC_F10,
    KC_F11,
    KC_F12,
    KC_F13,
    KC_F14,
    KC_F15,
    KC_F16,
    KC_F17,
    KC_F18,
    KC_F19,
    KC_F20,
    KC_F21,
    KC_F22,
    KC_F23,
    KC_F24,
    KC_F25,

    KC_Up,
    KC_Down,
    KC_Left,
    KC_Right,
    KC_LeftShift,
    KC_RightShift,
    KC_LeftControl,
    KC_RightControl,
    KC_LeftAlt,
    KC_RightAlt,
    KC_Tab,
    KC_Enter,
    KC_Backspace,
    KC_Insert,
    KC_Delete,
    KC_PageUp,
    KC_PageDown,
    KC_Home,
    KC_End,

    KC_NumPad0,
    KC_NumPad1,
    KC_NumPad2,
    KC_NumPad3,
    KC_NumPad4,
    KC_NumPad5,
    KC_NumPad6,
    KC_NumPad7,
    KC_NumPad8,
    KC_NumPad9,
    KC_NumPadDiv,
    KC_NumPadMul,
    KC_NumPadSub,
    KC_NumPadAdd,
    KC_NumPadPoint,
    KC_NumPadEqual,
    KC_NumPadEnter,
    KC_NumPadLock,

    KC_CapsLock,
    KC_ScrollLock,
    KC_Pause,
    KC_LeftSuper,
    KC_RightSuper,
    KC_Menu
};

enum MouseCode {
    MC_Left,
    MC_Right,
    MC_Middle
};

const u32 buttons[3] = {
    MC_Left,
    MC_Right,
    MC_Middle
};

class InputManager
{
    public:
        InputManager();
        ~InputManager();

        void Init();
        void Update();
        InputType GetState( KeyCode code )  const;
        InputType GetState( MouseCode code )    const;
        const vec2i& GetMousePos()   const;
        int GetWheelPos()   const;
        char GetTypedChar() const;

        static void GLFWKeyboardCallback( int code, int input );
        static void GLFWMouseButtonCallback( int code, int input );
        static void GLFWMousePosCallback( int x, int y );
        static void GLFWMouseWheelCallback( int pos );
        static void GLFWCharCallback( int code, int input );
        bool Shift()    const;
        bool IsWindowOpened()   const;

    private:
        static std::map< KeyCode, State > keyboard;
        static std::map< MouseCode, State > mouse;

        static char typedChar;
        static bool release;

        //  If the window got closed, the flag is set
        bool closed;
        //  If any of the shift buttons are presssed :
        bool caps;
        //  Mouse position
        static vec2i mousePos;
        static int wheelPos;
};

#endif // WATER_INPUTMANAGER
