#ifndef WATER_RECT
#define WATER_RECT

#include "vec2.hpp"

template<class T>
class Rect
{
    public:
        inline Rect<T>() : topLeft( vec2<T>( 0, 0 ) ), bottomRight( vec2<T>( 0, 0 ) ) {}
        template< class U > inline Rect<T>( const Rect<U>& r ) : topLeft( vec2<T>( (T)r.GetTopLeft().x, (T)r.GetTopLeft().y ), bottomRight( (T)r.GetBottomRight().x, (T)r.GetBottomRight().y ) ) {}
        inline Rect<T>( T l, T t, T r, T b ) : topLeft( l, t ), bottomRight( r, b ) {}
        inline Rect<T>( const vec2<T>& tl, const vec2<T>& br ) : topLeft( tl ), bottomRight( br ) {}
        inline ~Rect() {}

        inline void SetTopLeft( const vec2<T>& tl ) { topLeft = tl; }
        inline void SetBottomRight( const vec2<T>& br ) { bottomRight = br; }
        inline void SetLeft( T left ) { topLeft.x = left; }
        inline void SetRight( T right ) { bottomRight.x = right; }
        inline void SetTop( T top ) { topLeft.y = top; }
        inline void SetBottom( T bottom ) { bottomRight.y = bottom; }

        inline const vec2<T>& GetTopLeft() const { return topLeft; }
        inline const vec2<T>& GetBottomRight() const { return bottomRight; }
        inline vec2<T> GetTopRight() const { return vec2<T>( bottomRight.x, topLeft.y ); }
        inline vec2<T> GetBottomLeft() const { return vec2<T>( topLeft.x, bottomRight.y ); }

        inline const T GetLeft()    const { return topLeft.x; }
        inline const T GetTop()		const { return topLeft.y; }
        inline const T GetRight()   const { return bottomRight.x; }
        inline const T GetBottom()  const { return bottomRight.y; }

        template< class U > inline bool Contains( const vec2<U>& point ) { return ( (T)point.x >= topLeft.x && (T)point.x <= bottomRight.x && (T)point.y >= topLeft.y && (T)point.y <= bottomRight.y ); }

    private:
        vec2<T> topLeft;
        vec2<T> bottomRight;
};

typedef Rect<float> Rectf;
typedef Rect<int> Recti;
typedef Rect<unsigned int> Rectui;
typedef Rect<double> Rectd;

#endif // WATER_RECT
