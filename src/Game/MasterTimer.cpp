#include "MasterTimer.hpp"
#include "Common.hpp"
#include <GL/glfw.h>

MasterTimer::MasterTimer() : lastTime( 0 ), paused( true ) {

}

MasterTimer::~MasterTimer(){

}

void MasterTimer::Clean() {
    Info( "Cleaning master timer..." );
    while( !timers.empty() ) {
        timers.back()->SetID( -1 );
        timers.pop_back();
    }
    Append( " Done." );
}

int MasterTimer::AddTimer( Timer* t ) {
    timers.push_back( t );
    return timers.size() - 1;
}

void MasterTimer::RemoveTimer( int id ) {
    if( (u32)id >= timers.size() ) {
        Error( "Trying to remove an invalid timer (id = %d)", id );
        return;
    }

    //  Swap the last item with the one deleted, then delete the last item
    timers[id]->SetID( -1 );
    if( timers.size() > 1 && (u32)id != timers.size() - 1 ) {
        timers[id] = timers.back();
        timers[id]->SetID( id );
    }

    timers.pop_back();
}

void MasterTimer::Update() {
    if( paused ) {
        lastTime = glfwGetTime();
    }
    else {
        float time = glfwGetTime();
        float offset = time - lastTime;
        lastTime = time;
        for( u32 i = 0; i < timers.size(); ++i )
            timers[i]->Update( offset );
    }
}

void MasterTimer::Pause() {
    for( u32 i = 0; i < timers.size(); ++i )
        timers[i]->Pause();
    paused = true;
}

void MasterTimer::Start() {
    for( u32 i = 0; i < timers.size(); ++i )
        timers[i]->Start();
    paused = false;
}

double MasterTimer::GetCurrentTime() {
    return glfwGetTime();
}

