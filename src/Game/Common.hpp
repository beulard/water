#ifndef WATER_COMMON
#define WATER_COMMON

#include <string>

typedef unsigned char u8;
typedef unsigned int u32;
#define dcast dynamic_cast
#define scast static_cast

enum ColorType {
    CT_RGB,
    CT_RGBA
};

//  Those declarations allow other classes of the game to not include "Game.hpp".
//  It's good because the compiler doesn't have to recompile every class that
//  includes "Game.hpp" (which is a lot of classes) every time I make a change in one of those.
class Game;
template< class T > class vec2;
class Renderer;
class DataLoader;
class Mouse;
class MasterTimer;
class RootWidget;
class MasterAnim;
class Camera;
class InputManager;
class World;

namespace Global {

extern Game Water;
extern const vec2<int>* winSz;
extern const u32 tileSize;
extern Renderer* const renderer;
extern DataLoader* dataLoader;
extern Mouse* mouse;
extern MasterTimer* masterTimer;
extern MasterAnim* masterAnim;
extern RootWidget* rootWidget;
extern Camera* camera;
extern InputManager* inputMgr;
extern World* world;
extern float* projMatrix;

}

//  Game Log
extern FILE* Log;

std::string IntToString( int i );
std::string FloatToString( float f );
int StringToInt( const std::string& str );

void Exit();

#define _Log(...) fprintf(Log, __VA_ARGS__)
#define _Csl(...) printf(__VA_ARGS__)

#define _InfoStr	"\n[INFO] : "
#define _WarningStr "\n[WARNING] : "
#define _ErrorStr	"\n[ERROR] : "

#define _TraceInfo "\n\nIn function %s from file %s at line %d :", __func__, __FILE__, __LINE__

#define _InfoLog(...)		_Log(_InfoStr); _Log(__VA_ARGS__);
#define _WarningLog(...)	_Log(_WarningStr); _Log(__VA_ARGS__);
#define _ErrorLog(...)		_Log(_TraceInfo); _Log(_ErrorStr); _Log(__VA_ARGS__);
#define _AppendLog(...)		_Log(__VA_ARGS__)

#define _InfoCsl(...)		_Csl(_InfoStr); _Csl(__VA_ARGS__);
#define _WarningCsl(...)	_Csl(_WarningStr); _Csl(__VA_ARGS__);
#define _ErrorCsl(...)		_Csl(_TraceInfo); _Csl(_ErrorStr); _Csl(__VA_ARGS__);

#define _AppendCsl(...)		_Csl(__VA_ARGS__)

#ifdef WATER_DEBUG
    #define Info(...)		if(1){_InfoLog(__VA_ARGS__);	_InfoCsl(__VA_ARGS__);}
    #define Warning(...)	if(1){_WarningLog(__VA_ARGS__);	_WarningCsl(__VA_ARGS__);}
    #define Error(...)		if(1){_ErrorCsl(__VA_ARGS__);	_ErrorLog(__VA_ARGS__); Exit();}
    #define Append(...)		if(1){_AppendLog(__VA_ARGS__);	_AppendCsl(__VA_ARGS__);}
#else
    #define Info(...)		_InfoLog(__VA_ARGS__)
    #define Warning(...)	_WarningLog(__VA_ARGS__)
    #define Error(...)		if(1){_ErrorLog(__VA_ARGS__); Exit();}
    #define Append(...)		_AppendLog(__VA_ARGS__)
#endif

#if defined(_WIN32) || defined(__WIN32__)
    #define WATER_WINDOWS
#elif defined(linux) || defined(__linux)
    #define WATER_LINUX
#elif defined(__APPLE__) || defined(MACOSX) || defined(macintosh) || defined(Macintosh)
    #error OS not yet supported
    #define WATER_APPLE
#else
    #error OS not supported
#endif


#endif // WATER_COMMON
