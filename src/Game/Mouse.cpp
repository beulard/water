#include "Mouse.hpp"
#include "Game.hpp"
#include <windows.h>

Mouse::Mouse() : state( CS_Normal ) {
    #ifdef WATER_LINUX
    display = XOpenDisplay( NULL );
    #endif
}

Mouse::~Mouse() {

}

void Mouse::Free() {
    #ifdef WATER_LINUX
    XFreeCursor( Water.Xdisplay, cursor );
    delete display;
    #endif
}

void Mouse::SetCursor( CursorState s ) {
    #ifdef WATER_WINDOWS
    switch( s ) {
        case CS_Normal :
            cursor = LoadCursor( NULL, IDC_ARROW );
        break;
        case CS_ResizeNE :
        case CS_ResizeSW :
            cursor = LoadCursor( NULL, IDC_SIZENESW );
        break;
        case CS_ResizeNW :
        case CS_ResizeSE :
            cursor = LoadCursor( NULL, IDC_SIZENWSE );
        break;
        case CS_ResizeS :
        case CS_ResizeN :
            cursor = LoadCursor( NULL, IDC_SIZENS );
        break;
        case CS_ResizeW :
        case CS_ResizeE :
            cursor = LoadCursor( NULL, IDC_SIZEWE );
        break;
        case CS_Text :
            cursor = LoadCursor( NULL, IDC_IBEAM );
        break;
    }
    SetClassLongPtr( (HWND)Global::Water.handle, GCLP_HCURSOR, reinterpret_cast<LONG_PTR>( cursor ) );
    #elif defined WATER_LINUX
    switch( s ) {
        case CS_Normal :
            cursor = XCreateFontCursor( Global::Water.Xdisplay, XC_left_ptr );
        break;
        case CS_ResizeNE :
            cursor = XCreateFontCursor( Global::Water.Xdisplay, XC_top_right_corner );
        break;
        case CS_ResizeSW :
            cursor = XCreateFontCursor( Global::Water.Xdisplay, XC_bottom_left_corner );
        break;
        case CS_ResizeNW :
            cursor = XCreateFontCursor( Global::Water.Xdisplay, XC_top_left_corner );
        break;
        case CS_ResizeSE :
            cursor = XCreateFontCursor( Global::Water.Xdisplay, XC_bottom_right_corner );
        break;
        case CS_ResizeN :
            cursor = XCreateFontCursor( Global::Water.Xdisplay, XC_top_side );
        break;
        case CS_ResizeS :
            cursor = XCreateFontCursor( Global::Water.Xdisplay, XC_bottom_side );
        break;
        case CS_ResizeW :
            cursor = XCreateFontCursor( Global::Water.Xdisplay, XC_left_side );
        break;
        case CS_ResizeE :
            cursor = XCreateFontCursor( Global::Water.Xdisplay, XC_right_side );
        break;
        case CS_Text :
            cursor = XCreateFontCursor( Global::Water.Xdisplay, XC_xterm );
        break;
    }
    XDefineCursor( display, Global::Water.handle, Cursor );
    XFlushDisplay( display );
    #endif
}
