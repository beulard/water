#ifndef WATER_MASTERTIMER
#define WATER_MASTERTIMER
#include <vector>
#include "Timer.hpp"

class MasterTimer
{
    public:
        MasterTimer();
        ~MasterTimer();

        void Clean();

        int AddTimer( Timer* t );
        void RemoveTimer( int id );

        void Update();
        void Pause();
        void Start();

        double GetCurrentTime();

    private:
        std::vector< Timer* > timers;
        float lastTime;
        bool paused;
};

#endif // WATER_MASTERTIMER
