#include "Game.hpp"
#include <GL/glew.h>
#include <GL/glfw.h>
#include <iostream>
#include "UIMainMenu.hpp"
#include "Quad.hpp"
#include "Triangle.hpp"
#include "Tile.hpp"

FILE* Log;

bool Game::Success = 0;
bool Game::Failure = 1;

const float Game::FrameTime = 1.f / 60.f;
const float Game::SimTime = 1.f / 100.f;

Game::Game(){
    running = false;
    Version.Major = 0;
    Version.Minor = 3;
    Version.Revision = 6;
}

Game::~Game(){

}

bool Game::Init() {
    //  Open the log file in append mode
    Log = fopen( "log", "w" );
    if( !Log ) {
        #ifdef WATER_DEBUG
            std::cout << "Could not open the log file." << std::endl;
        #endif
        return Game::Failure;
    }

    #ifdef WATER_DEBUG
        Append( "[:::::::::: Water Debug Output ::::::::::]\n\n" );
    #else
        Append( "[:::::::::: Water Release Output :::::::::]" );
    #endif

    //  Load the config file and read the values for OpenGL context initialization
    wJSON root( "data/config.json" );
    u32 width = root.GetItem( "width" ).Int();
    u32 height = root.GetItem( "height" ).Int();
    bool fullscreen = root.GetItem( "fullscreen" ).Bool();
    u32 antialiasing = root.GetItem( "antialiasing" ).Int();
    int glfwMode = fullscreen ? GLFW_FULLSCREEN : GLFW_WINDOW;

    windowSize = vec2i( width, height );

    if( glfwInit() == GL_TRUE ) {
        Info("GLFW initialized correctly.");
        glfwOpenWindowHint( GLFW_WINDOW_NO_RESIZE, GL_TRUE );
        glfwOpenWindowHint( GLFW_OPENGL_VERSION_MAJOR, 2 );
        glfwOpenWindowHint( GLFW_OPENGL_VERSION_MINOR, 1 );
        glfwOpenWindowHint( GLFW_FSAA_SAMPLES, antialiasing );
        if( glfwOpenWindow( width, height, 8, 8, 8, 8, 8, 0, glfwMode ) ) {
            Info( "Window opened correctly." );
            if( fullscreen )
                glfwEnable( GLFW_MOUSE_CURSOR );
            handle = glfwGetWindowHandle();
            glfwSetWindowTitle( "Water" );
            glfwSwapInterval( 0 );
            if( glewInit() == GLEW_OK ) {
                Info( "GLEW initialized correctly." );
                if( !FT_Init_FreeType( &ft ) ) {
                    Info( "FreeType initialized correctly." );
                    running = true;
                    Info( "Everything initialized correctly.");
                    Info( "Using OpenGL version %i.%i", glfwGetWindowParam( GLFW_OPENGL_VERSION_MAJOR ), glfwGetWindowParam( GLFW_OPENGL_VERSION_MINOR ) );
                    Info( "Using GLFW version %i.%i.%i", GLFW_VERSION_MAJOR, GLFW_VERSION_MINOR, GLFW_VERSION_REVISION );
                    Info( "Using GLEW version %s.%s.%s", glewGetString( GLEW_VERSION_MAJOR ), glewGetString( GLEW_VERSION_MINOR ), glewGetString( GLEW_VERSION_MICRO ) );
                    Info( "Using FreeType2\n" );
                    glClearColor( 0, 0, 0, 0 );
                    glEnable( GL_BLEND );
                    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
                    glEnableClientState( GL_VERTEX_ARRAY );

                    projMatrix[0] = 2.f / windowSize.x;
                    projMatrix[1] = 0.f;
                    projMatrix[2] = 0.f;
                    projMatrix[3] = 0.f;
                    projMatrix[4] = 2.f / windowSize.y;
                    projMatrix[5] = 0.f;
                    projMatrix[6] = -1;
                    projMatrix[7] = -1;
                    projMatrix[8] = 0;


                    state = GS_PreLoading;
                    //  The data files here can not be added or removed by the user
                    //  They are the mandatory resources the game needs to launch and load other data
                    dataLoader.Add( "uisprite", DT_Shader );
                    dataLoader.Add( "uicolored", DT_Shader );
                    dataLoader.Add( "uitext", DT_Shader );
                    dataLoader.Add( "sprite", DT_Shader );
                    dataLoader.Add( "colored", DT_Shader );
                    dataLoader.Add( "text", DT_Shader );


                    dataLoader.Add( "loading", DT_Texture );
                    dataLoader.Add( "loading_bar_outline", DT_Sprite );
                    dataLoader.Add( "loading_bar_inside", DT_Sprite );
                    dataLoader.Add( "dejavusans", DT_Font );
                    //  Load the basic data to be able to render the loading screen
                    dataLoader.LoadSilent();

                    //  Set the shaders required to render the loading screen
                    Renderer::UISpriteShader = &dataLoader.GetShaderProgram( "uisprite" );
                    Renderer::UIColoredShader = &dataLoader.GetShaderProgram( "uicolored" );
                    Renderer::UITextShader = &dataLoader.GetShaderProgram( "uitext" );
                    Renderer::worldSpriteShader = &dataLoader.GetShaderProgram( "sprite" );
                    Renderer::worldColoredShader = &dataLoader.GetShaderProgram( "colored" );
                    Renderer::worldTextShader = &dataLoader.GetShaderProgram( "text" );


                    inputMgr.Init();
                    renderer.Init();

                    //  We first need to load game data, so let's initialize the loading screen
                    state = GS_Loading;

                    //  We will now add every directory to the loading list but remove data files we have already loaded for the loading screen
                    //  Shaders can not be added/removed by users for now so we don't add the 'shaders' folder
                    dataLoader.AddDir( "textures", DT_Texture );
                    dataLoader.Remove( "loading", DT_Texture );

                    dataLoader.AddDir( "fonts", DT_Font );
                    dataLoader.Remove( "dejavusans", DT_Font );

                    dataLoader.AddDir( "sprites", DT_Sprite );
                    dataLoader.Remove( "loading_bar_outline", DT_Sprite );
                    dataLoader.Remove( "loading_bar_inside", DT_Sprite );

                    dataLoader.AddDir( "anims", DT_Anim );

                    dataLoader.AddDir( "tiles", DT_Tile );

                    dataLoader.AddDir( "biomes", DT_Biome );

                    dataLoader.AddDir( "entities", DT_Entity );

                    //  Display a loading screen while game data is being read
                    dataLoader.LoadWithScreen();

                    camera.SetPosition( vec2i( 0, 0 ) );

                    masterTimer.Start();

                    return Game::Success;
                }
                else
                    Error( "Couldn't initialize FreeType." );
            }
            else
                Error( "Couldn't initialize GLEW." );
        }
        else
            Error( "Couldn't open the window." );
    }
    else
        Error( "Couldn't initialize GLFW." );



    return Game::Failure;
}

bool Game::Run() {

    //mouse.SetCursor( CS_Normal );
    UIMainMenu mainMenu;
    //  The main menu has no direct parent, so we set it to the root widget so that
    //  it can handle user input (mouse clicks)
    mainMenu.AddToRoot();

    Random::SetSeed( 23123 );
    //world.map.Generate();

    lastState = GS_Loading;
    state = GS_MainMenu;

    soup = dataLoader.GetEntity( "soup" );
    world.SetPlayerBox( soup.GetBox() );
    camera.SetCenter( soup.GetPosition() );
    //soup.SetPosition( vec2i( 100, 100 ) );

    Font& dvs12 = dataLoader.GetFont( "dejavusans", 12 );
    Font& dvs16 = dataLoader.GetFont( "dejavusans", 16 );
    Font& dvs124 = dataLoader.GetFont( "dejavusans", 124 );


    //Tile hero2 = dataLoader.GetTile( "water" );
    //world.AddBox( hero2.GetBox() );
    //hero2.Move( vec2i( 100, 0 ) );
    //Tile hero3 = dataLoader.GetTile( "earth" );
    //world.AddBox( hero3.GetBox() );


    Text t2( "WATER", &dvs124, Color( 120, 255, 39, 150 ) );
    t2.SetRenderMode( RM_UIText );
    t2.Move( vec2i( 810, 0 ) );



    Timer frameTimer;
    frameTimer.Start();
    float frameTime = 0.f;
    UIText frameTimeText;
    frameTimeText.SetFont( &dvs16);


    Timer timer1sec( 1.f );
    timer1sec.SetLoop( true );
    timer1sec.Start();

    frameTimeText.SetString( "FPS" );
    //frameTimeText.SetPosition( vec2i( 0, 10 ) );
    frameTimeText.Draw();



    while( running ) {
        if( timer1sec.HasEnded() )
            frameTimeText.SetString( IntToString( (int)frameTime ) );
        double startTime = masterTimer.GetCurrentTime();


        inputMgr.Update();
        if( inputMgr.IsWindowOpened() == false )
            running = false;

        masterTimer.Update();

        if( inputMgr.GetState( MC_Left ) == IT_None )
            rootWidget.OnHover();
        if( inputMgr.GetState( MC_Left ) == IT_Press )
            rootWidget.OnPress();
        if( inputMgr.GetState( MC_Left ) == IT_Release )
            rootWidget.OnRelease();

        //mouse.Update();

        renderer.Clear();

        if( state != lastState ) {
            if( state == GS_MainMenu ) {
                mainMenu.Activate();
            }

            if( lastState == GS_MainMenu ) {
                mainMenu.Sleep();
            }

            if( lastState == GS_Game ) {
                //Soup.StopDrawing();
            }

            if( state == GS_Game ) {
                if( !soup.IsDrawn() )
                    soup.Draw();
            }

            lastState = state;
        }

        if( state == GS_MainMenu ) {

        }

        else if( state == GS_Game ) {
            world.Update();
            masterAnim.Update();

            if( inputMgr.GetState( KC_Escape ) == IT_Press )
                state = GS_MainMenu;

            if( inputMgr.Shift() )
                soup.SetSpeed( 20.f );
            else
                soup.SetSpeed( 10.f );

            if( inputMgr.GetState( KC_D ) == IT_Press )
                soup.GetBox().MoveRight();

            if( inputMgr.GetState( KC_W ) == IT_Press )
                soup.GetBox().Jump();

            if( inputMgr.GetState( KC_S ) == IT_Press )
                soup.GetBox().velocity += vec2f( 0, 2 );

            if( inputMgr.GetState( KC_A ) == IT_Press )
                soup.GetBox().MoveLeft();

            if( inputMgr.GetState( KC_R ) == IT_Release ) {
                soup.SetPosition( vec2i( 0, 0 ) );
                soup.GetBox().velocity.y = 0;
            }

            if( inputMgr.GetState( MC_Left ) == IT_Release ) {

            }
            if( inputMgr.GetState( MC_Right ) == IT_Release ) {

            }

            camera.SetCenter( soup.GetBox().center );

            renderer.Clear();
            //renderer.Draw( t2 );
            soup.Update();
            //renderer.Draw( hero2 );
            //renderer.Draw( hero3 );
            //world.map.Draw();
            world.map.Update();
            //renderer.Draw( Soup );
        }
        renderer.Render();

        double endTime = masterTimer.GetCurrentTime();
        frameTime = 1 / ( endTime - startTime );

    }

    return Game::Success;
}

void Game::Clean() {
    //  For linux : free the cursor and delete the X display
    mouse.Free();

    glfwTerminate();

    Append( "\n" );
    Info( "Started cleaning..." );
    world.Clean();
    rootWidget.Clean();
    dataLoader.Clean();
    masterTimer.Clean();
    masterAnim.Clean();
    renderer.Clean();

    Info( "Done cleaning." );
}

void Game::Exit() {
    Clean();
    Append( "\n\n\n[:::::::::::: End of Output ::::::::::::]\n\n\n" );

    //  Close the log file and exit
    fclose( Log );
    exit(1);
}

const vec2i& Game::GetWindowSize()   const {
    return windowSize;
}

const vec2i& Game::GetCameraPosition()  const {
    return camera.GetPosition();
}

const vec2i& Game::GetCameraGLPosition() const {
    return camera.GetGLPosition();
}

//  Set the values for our pointers declared in Common.hpp
const vec2<int>* Global::winSz = &Global::Water.GetWindowSize();
Renderer* const Global::renderer = &Global::Water.renderer;
DataLoader* Global::dataLoader = &Global::Water.dataLoader;
Mouse* Global::mouse = &Global::Water.mouse;
MasterTimer* Global::masterTimer = &Global::Water.masterTimer;
MasterAnim* Global::masterAnim = &Global::Water.masterAnim;
RootWidget* Global::rootWidget = &Global::Water.rootWidget;
Camera* Global::camera = &Global::Water.camera;
InputManager* Global::inputMgr = &Global::Water.inputMgr;
World* Global::world = &Global::Water.world;
float* Global::projMatrix = Global::Water.projMatrix;

