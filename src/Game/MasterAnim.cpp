#include "MasterAnim.hpp"
#include "Common.hpp"

MasterAnim::MasterAnim() {

}

MasterAnim::~MasterAnim() {

}

void MasterAnim::Clean() {
    Info( "Cleaning master anim..." );
    while( !anims.empty() ) {
        anims.back()->SetAnimID( -1 );
        anims.pop_back();
    }
    Append( " Done." );
}

void MasterAnim::AddAnim( Anim& anim ) {
    anims.push_back( &anim );
    anims.back()->SetAnimID( anims.size() - 1 );
}

void MasterAnim::RemoveAnim( int id ) {
    if( (u32)id >= anims.size() ) {
        Error( "Trying to delete an invalid anim (id = %d)", id );
        return;
    }

    anims[id]->SetAnimID( -1 );
    if( anims.size() > 1 && (u32)id != anims.size() - 1 ) {
        anims[id] = anims.back();
        anims[id]->SetAnimID( id );
    }

    anims.pop_back();
}

void MasterAnim::Update() {
    for( u32 i = 0; i < anims.size(); ++i )
        anims[i]->Update();
}
