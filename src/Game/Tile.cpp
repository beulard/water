#include "Tile.hpp"
#include "Anim.hpp"
#include "Quad.hpp"
#include "Sprite.hpp"
#include "DataLoader.hpp"
#include "Map.hpp"

std::map< u32, Tile* > Tile::Tiles;

Tile::Tile() : placement(TP_None), tileID(-1), minTemp(0), maxTemp(100), minHum(0), maxHum(100) {
    box.center = vec2i( 12.5f, 12.5f );
    box.halfextents = vec2i( 25, 25 );
    SetRenderMode( RM_None );
}

Tile::Tile( const Tile& t ) : Entity( t ) {
    *this = t;
}

void Tile::operator=( const Tile& t ) {
    if( drawn )
        StopDrawing();
    if( initialized )
        delete current;
    initialized = false;
    if( t.initialized )
        SetData( *t.data );
    tileID = t.tileID;
    currentIndex = t.currentIndex;
    box = t.box;
    trueSize = t.trueSize;

    placement = TP_None;
    minTemp = t.minTemp;
    maxTemp = t.maxTemp;
    minHum = t.minHum;
    maxHum = t.maxHum;
}

void Tile::operator=( u32 id ) {
    *this = *Tiles[id];
}

Tile::~Tile(){

}

void Tile::SetTileID( int id ) {
    tileID = id;
}

int Tile::GetTileID() const {
    return tileID;
}

void Tile::SetMinTemp( int t ) {
    minTemp = t;
}

void Tile::SetMaxTemp( int t ) {
    maxTemp = t;
}

void Tile::SetMinHum( int h ) {
    minHum = h;
}

void Tile::SetMaxHum( int h ) {
    maxHum = h;
}

int Tile::GetMinTemp()  const {
    return minTemp;
}

int Tile::GetMaxTemp()  const {
    return maxTemp;
}

int Tile::GetMinHum()   const {
    return minHum;
}

int Tile::GetMaxHum()   const {
    return maxHum;
}

void Tile::SetPlacement() {
    if( data->d.size() != 1 ) {
        if( placement & TP_Left && placement & TP_Top && placement & TP_Bottom && placement & TP_Right )
            currentIndex = 4;
        else if( placement & TP_Right && placement & TP_Bottom && placement & TP_Left )
            currentIndex = 1;
        else if( placement & TP_Top && placement & TP_Right && placement & TP_Bottom )
            currentIndex = 3;
        else if( placement & TP_Top && placement & TP_Right && placement & TP_Left )
            currentIndex = 7;
        else if( placement & TP_Left && placement & TP_Top && placement & TP_Bottom )
            currentIndex = 5;
        else if( placement & TP_Right && placement & TP_Bottom )
            currentIndex = 0;
        else if( placement & TP_Left && placement & TP_Bottom )
            currentIndex = 2;
        else if( placement & TP_Top && placement & TP_Right )
            currentIndex = 6;
        else if( placement & TP_Top && placement & TP_Left )
            currentIndex = 8;
        else if( placement & TP_Left && placement & TP_Right )
            currentIndex = 13;
        else if( placement & TP_Top && placement & TP_Bottom )
            currentIndex = 14;
        else if( placement & TP_Right )
            currentIndex = 9;
        else if( placement & TP_Left )
            currentIndex = 10;
        else if( placement & TP_Bottom )
            currentIndex = 11;
        else if( placement & TP_Top )
            currentIndex = 12;
        else
            currentIndex = 15;
        SetDrawableIndex( currentIndex );
    }
    else {
        if( currentIndex != 0 ) {
            SetDrawableIndex( 0 );
        }
        placement = TP_None;
        currentIndex = 0;
    }
    current->SetSize( vec2ui( Map::TileSize, Map::TileSize ) );
    current->SetPosition( position );
}

