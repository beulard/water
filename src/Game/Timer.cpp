#include "Timer.hpp"
#include "Common.hpp"
#include <GL/glfw.h>
#include "MasterTimer.hpp"

Timer::Timer() : startTime( 0.f ), endTime( 0.f ), running( false ), loop( false ), id( -1 ) {

}

Timer::Timer( float t ) : startTime( glfwGetTime() ), endTime( 0.f ), running( false ), loop( false ), id( -1 ) {
    SetTimer( t );
}

Timer::~Timer() {
    if( id != -1 )
        Global::masterTimer->RemoveTimer( id );
}

void Timer::SetTimer( float t ) {
    startTime = glfwGetTime();
    endTime = glfwGetTime() + t;
    currentTime = 0.f;
}

void Timer::Start() {
    if( !running ) {
        running = true;
        ended = false;
        id = Global::masterTimer->AddTimer( this );
    }
}

void Timer::Pause() {
    if( running ) {
        running = false;
        Global::masterTimer->RemoveTimer( id );
    }
}

void Timer::Stop() {
    if( running ) {
        currentTime = 0.f;
        running = false;
        ended = true;
        Global::masterTimer->RemoveTimer( id );
    }
}

void Timer::Reset() {
    if( running ) {
        ended = true;
        float offset = endTime - startTime;
        startTime = glfwGetTime();
        endTime = startTime + offset;
        currentTime = 0.f;
    }
}

void Timer::Toggle() {
    if( running )
        Pause();
    else
        Start();
}

void Timer::Update( float time ) {
    if( running ) {
        if( ended )
            ended = false;
        currentTime += time;
        if( startTime + currentTime >= endTime && endTime != 0.f ) {
            currentTime = endTime - startTime;
            if( loop )
                Reset();
            else
                Stop();
        }
    }
    else {
        endTime += time;
        startTime += time;
    }
}

bool Timer::HasEnded()  const {
    return ended;
}

float Timer::GetTime()  const {
    return currentTime;
}

void Timer::SetID( int i ) {
    id = i;
}

void Timer::SetLoop( bool l ) {
    loop = l;
}

