#ifndef WATER_RANDOM
#define WATER_RANDOM
#include <cstdlib>

typedef unsigned int u32;

class Random
{
    public:
        inline static u32 GetRandom( u32 start, u32 end ) {
            if( start <= end ) return rand() % (end - start + 1) + start;
            else return 0;
        }
        inline static float Max() { return (float)RAND_MAX; }
        inline static void SetSeed( u32 s ) { srand( s ); }
};

#endif // WATER_RANDOM
