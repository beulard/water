#ifndef WATER_MOUSE
#define WATER_MOUSE
#include "Common.hpp"

#ifdef WATER_WINDOWS
//#include <windows.h>
#endif

enum CursorState {
    CS_Normal,
    CS_ResizeNE,
    CS_ResizeSW,
    CS_ResizeNW,
    CS_ResizeSE,
    CS_ResizeS,
    CS_ResizeE,
    CS_ResizeW,
    CS_ResizeN,
    CS_Text
};

class Mouse
{
    public:
        Mouse();
        ~Mouse();

        void Free();

        void SetCursor( CursorState s );

    private :
        #ifdef WATER_WINDOWS
        void* cursor;

        #elif defined WATER_LINUX
        Display* display
        XID cursor;
        #endif

        CursorState state;
};

#endif // WATER_MOUSE
