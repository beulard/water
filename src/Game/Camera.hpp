#ifndef WATER_CAMERA
#define WATER_CAMERA
#include "vec2.hpp"

//  This class could be replaced by a simple vec2i but I want to implement
//  some special functions like centering and shaking
class Camera
{
    public:
        Camera();
        ~Camera();

        void Move( const vec2i& offset );
        void SetPosition( const vec2i& pos );
        void SetCenter( const vec2i& pos );
        const vec2i& GetPosition()   const;
		const vec2i& GetGLPosition() const;
    private:
        vec2i position;
		vec2i glPosition;
};

#endif // WATER_CAMERA
