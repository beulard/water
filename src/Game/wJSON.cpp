#include "wJSON.hpp"
#include "Common.hpp"
#include "FileStream.hpp"
#include <cstring>

wJSON::wJSON() : data(NULL), parent(NULL), allocated(false), exists(false) {

}

wJSON::wJSON( const std::string& file ) : data(NULL), parent(NULL), allocated(false), exists(false) {
    FileStream fs( file, OM_Read );
    std::string buf = fs.ReadAll();
    fs.Close();
    data = cJSON_Parse( buf.c_str() );
    if( !data ) {
        const char* first = &buf[0];
        const char* last = &buf.back();
        const char* err = cJSON_GetErrorPtr();
        const char* backward = err;
        const char* forward = err;
        int startLine = 0;
        int endLine = 0;
        int nbrLines = 0;

        //  Counts the number of lines to the error
        while( first != err ) {
            if( *first == '\n' )
                nbrLines++;
            first++;
        }
        first = &buf[0];
        //  Looks for the first character of the line where the error occured
        while( *backward != '\n' && backward != first )
            backward--;
        //  Same thing for the last character of the line
        while( *forward != '\n' && forward != last )
            forward++;

        first = &buf[0];
        //  Count the number of characters to the start of the line
        while( first != backward ) {
            first++;
            startLine++;
        }
        endLine = startLine;
        //  Count the number of characters to the end of the line
        while( first != forward ) {
            first++;
            endLine++;
        }
        //  Put the bad line in a char array without the ending '\n' and print it
        char str[endLine - startLine];
        strncpy( str, backward + 1, endLine - startLine - 1 );
        str[endLine - startLine - 1] = '\0';
        Info( "Could not parse file '%s', syntax error at line %d :", file.c_str(), nbrLines );
        Error( "%s", str );
    }
    else {
        name = file;
        allocated = true;
        exists = true;
    }
}

void wJSON::operator=( const wJSON& json ) {
    data = json.data;
    allocated = json.allocated;
    if( data )
        exists = true;
}

wJSON::~wJSON() {
    if( allocated && exists )
        cJSON_Delete( data );
}

void wJSON::PrintTrace() {
    wJSON* current = this;
    while( current ) {
        Append( current->GetName().c_str() );
        current = current->parent;
        if( current )
            Append( " of " );
    }
}

void wJSON::SetParent( wJSON& r ) {
    parent = &r;
}

wJSON wJSON::GetItem( const std::string& name, bool forceExist ) {
    wJSON json;
    json.SetParent( *this );
    json.SetData( cJSON_GetObjectItem( data, name.c_str() ) );
    if( forceExist ) {
        if( json.GetData() == NULL ) {
            Info( "In " );
            PrintTrace();
            Error( "Couldn't find '%s'", name.c_str() );
        }
    }
    json.SetName( "'" + name + "'" );
    return json;
}

wJSON wJSON::GetItem( u32 index, bool forceExist ) {
    wJSON json;
    json.SetParent( *this );
    json.SetData( cJSON_GetArrayItem( data, index ) );
    json.SetName( "'element " + IntToString(index) + "'" );
    if( forceExist ) {
        if( json.GetData() == NULL ) {
            Info( "In " );
            PrintTrace();
            Error( "Couldn't find '%s'", name.c_str() );
        }
    }
    return json;
}

u32 wJSON::GetSize() const {
    return cJSON_GetArraySize( data );
}

bool wJSON::Exists() const {
    return exists;
}

const std::string& wJSON::GetName() const {
    return name;
}

void wJSON::SetName( const std::string& n ) {
    name = n;
}

const char* wJSON::String() {
    return data->valuestring;
}

int wJSON::Int() {
    return data->valueint;
}

u32 wJSON::Uint() {
    return (u32)data->valueint;
}

float wJSON::Float() {
    return data->valuedouble;
}

bool wJSON::Bool() {
    return (bool)data->valueint;
}

//  Private

cJSON* wJSON::GetData() const {
    return data;
}

void wJSON::SetData( cJSON* json ) {
    data = json;
    if( data )
        exists = true;
}


