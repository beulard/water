#include "InputManager.hpp"
#include "Game.hpp"
#include "GL/glfw.h"

std::map< KeyCode, State > InputManager::keyboard;
std::map< MouseCode, State > InputManager::mouse;
vec2i InputManager::mousePos = vec2i( 0, 0 );
int InputManager::wheelPos = 0;
char InputManager::typedChar = '\0';
bool InputManager::release = true;


State::State() : currentState( IT_None ), lastState( IT_None ) {

}

State::State( InputType cur ) : currentState( cur ), lastState( IT_None ) {

}

InputManager::InputManager(){
    closed = false;
}

InputManager::~InputManager(){

}

void InputManager::Init() {
    for( u32 i = 0; i < 122; ++i )
        keyboard.insert( std::pair< KeyCode, State >( (KeyCode)keys[i], State( IT_None ) ) );
    for( u32 i = 0; i < 3; ++i )
        mouse.insert( std::pair< MouseCode, State >( (MouseCode)buttons[i], State( IT_None ) ) );
    glfwEnable( GLFW_KEY_REPEAT );

    glfwSetMouseButtonCallback( (GLFWmousebuttonfun)InputManager::GLFWMouseButtonCallback );
    glfwSetMousePosCallback( (GLFWmouseposfun)InputManager::GLFWMousePosCallback );
    glfwSetKeyCallback( (GLFWcharfun)InputManager::GLFWKeyboardCallback );
    glfwSetMouseWheelCallback( (GLFWmousewheelfun)InputManager::GLFWMouseWheelCallback );
    glfwSetCharCallback( (GLFWcharfun)InputManager::GLFWCharCallback );
}

void InputManager::Update() {
    if( release )
        typedChar = '\0';
    else
        release = true;
    caps = false;
    for( std::map< KeyCode, State >::iterator it = keyboard.begin(); it != keyboard.end(); it++ ) {
        if( it->first == KC_LeftShift || it->first == KC_RightShift ) {
            if( it->second.currentState == IT_Press )
                caps = true;
        }

        if( it->second.currentState == IT_Release && it->second.lastState == IT_Release )
            it->second.currentState = IT_None;
        it->second.lastState = it->second.currentState;
    }

    for( std::map< MouseCode, State >::iterator it = mouse.begin(); it != mouse.end(); it++ ) {
        if( it->second.currentState == IT_Release && it->second.lastState == IT_Release )
            it->second.currentState = IT_None;
        it->second.lastState = it->second.currentState;
    }

    if( glfwGetWindowParam( GLFW_OPENED ) == GL_FALSE )
        closed = true;
}

char InputManager::GetTypedChar()   const {
    return typedChar;
}

void InputManager::GLFWKeyboardCallback( int code, int input ) {
    std::map< KeyCode, State >::iterator it = keyboard.find( (KeyCode)code );
    if( it->first == (KeyCode)code ) {
        switch( input ) {
            case GLFW_PRESS :
                it->second.currentState = IT_Press;
            break;
            case GLFW_RELEASE :
                it->second.currentState = IT_Release;
            break;
        }
    }
}

void InputManager::GLFWMouseButtonCallback( int code, int input ) {
    std::map< MouseCode, State >::iterator it = mouse.find( (MouseCode)code );
    if( it->first == (MouseCode)code ) {
        switch( input ) {
            case GLFW_PRESS :
                it->second.currentState = IT_Press;
            break;
            case GLFW_RELEASE :
                it->second.currentState = IT_Release;
            break;
        }
    }
}

void InputManager::GLFWMousePosCallback( int x, int y ) {
    mousePos.x = x;
    mousePos.y = y;
}

void InputManager::GLFWMouseWheelCallback( int pos ) {
    wheelPos = pos;
}

void InputManager::GLFWCharCallback( int code, int input ) {
    if( input == GLFW_PRESS ) {
        typedChar = code;
        release = false;
    }
}

bool InputManager::Shift()  const {
    return caps;
}

const vec2i& InputManager::GetMousePos() const {
    return mousePos;
}

InputType InputManager::GetState( KeyCode code )    const {
    std::map< KeyCode, State >::iterator it = keyboard.find( code );
    if( it != keyboard.end() )
        return it->second.currentState;
    return IT_None;
}

InputType InputManager::GetState( MouseCode code )  const {
    std::map< MouseCode, State >::iterator it = mouse.find( code );
    if( it != mouse.end() )
        return it->second.currentState;
    return IT_None;
}

int InputManager::GetWheelPos() const {
    return wheelPos;
}

bool InputManager::IsWindowOpened() const {
    return !closed;
}
