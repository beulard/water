#ifndef WATER_WJSON
#define WATER_WJSON
#include <string>
#include "cJSON.h"

typedef unsigned int u32;

class wJSON
{
    public:
        wJSON();
        wJSON( const std::string& file );
        void operator=( const wJSON& json );
        ~wJSON();

        void PrintTrace();

        void SetParent( wJSON& r );
        //  Get an abstract item in a json object
        //  The forceExist param tells if data in the json object has to exist or not (if it has to, but does not, it will Error and print the trace)
        //  Default is true to avoid null pointers
        wJSON GetItem( const std::string& n, bool forceExist = true );
        //  Or get a value from an array object
        wJSON GetItem( u32 index, bool forceExist = true );
        //  Returns the size of the json object if it is an array
        u32 GetSize() const;

        bool Exists() const;


        const std::string& GetName() const;
        void SetName( const std::string& n );


        //  Returns the value held by our json object
        const char* String();
        int         Int();
        u32         Uint();
        float       Float();
        bool        Bool();


    private:
        std::string name;
        cJSON* data;
        wJSON* parent;
        //  Was the data allocated by cJSON_Parse() ?
        bool allocated;
        //  Is just a way to know if our object is usable for stuff or if it wasn't loaded or failed to load
        bool exists;
        cJSON* GetData() const;
        void SetData( cJSON* data );
};

#endif // WATER_WJSON
