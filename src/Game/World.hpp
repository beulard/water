#ifndef WATER_WORLD
#define WATER_WORLD
#include <vector>
#include "AABB.hpp"
#include "Timer.hpp"
#include "Map.hpp"

enum Side {
    Right,
    Left
};

class World
{
    public:
        World();
        ~World();

        void Update();
        static const float Gravity;
        void SetPlayerBox( AABB& box );
        void AddStaticBox( AABB& box );
        void AddMapBox( AABB& box );
        void AddDynamicBox( AABB& box );
        void RemoveStaticBox( AABB& box );
        void RemoveMapBox( AABB& box );
        void RemoveDynamicBox( AABB& box );
        void ClearStaticBoxes();
        void ClearMapBoxes();
        void ClearDynamicBoxes();


        //  Make space in the mapBoxes deque for a new chunk, on the left or the right
        void AddChunkBoxes( Side s, u32 sz );
        //  Remove all boxes from a chunk from the mapBoxes deque, either on the left or the right
        void RemoveChunkBoxes( Side s, u32 sz );
        //  Manually set the value of a box from the mapBoxes with local coordinates from the map
        void SetMapBox( const vec2ui& coords, AABB& box );
        //  Manually set the value of a box from the mapBoxes deque with its deque id
        void SetMapBox( u32 id, AABB& box );

        void Clean();

        Map map;

        AABB* playerBox;
        //	Contains every dynamic box plus the player box
        std::vector< AABB* > dynamicBoxes;
        //	Contains every static box, including map boxes
        std::vector< AABB* > staticBoxes;
        //	Contains every map box
        std::deque< AABB* > mapBoxes;
        //	Contains every box
        std::vector< AABB* > allBoxes;
    protected:
    private:
        Timer accumulator;
};

#endif // WATER_WORLD
