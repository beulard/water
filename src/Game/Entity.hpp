#ifndef WATER_ENTITY
#define WATER_ENTITY
#include "Drawable.hpp"
#include "Random.hpp"
#include "AABB.hpp"
#include "Wash.hpp"
#include <vector>
#include <utility>

enum EntityType {
    ET_Sprite,
    ET_Anim,
    ET_Quad
};

enum EntityState {
    ES_IdleRight,
    ES_IdleLeft,
    ES_RunRight,
    ES_RunLeft,
    ES_JumpRight,
    ES_JumpLeft,
    ES_CrouchRight,
    ES_CrouchLeft
};

class Drawable;
struct EntityData;

class Entity : public Drawable
{
    public:
        Entity();
        Entity( const Entity& e );
        virtual void operator=( const Entity& e );
        virtual ~Entity();

        void SetData( EntityData& ed );
        void SetDrawableIndex( u32 index );

        virtual void Update();

        virtual void Draw();
        virtual void StopDrawing();

        virtual const vec2i& GetPosition()  const;

        virtual void SetPosition( const vec2i& pos );
        virtual void Move( const vec2i& offset );
        void SetDynamic( bool d );
        virtual void SetSize( const vec2ui& sz );
        void SetSpeed( float s );

        void SetCollisionType( CollisionType t );
        const CollisionType& GetCollisionType()    const;

        //  Set the general box offset
        void SetBoxOffset( const vec2i& offset );
        //  Get the general box offset
        const vec2i& GetBoxOffset() const;

        AABB& GetBox();

        EntityType GetType()  const;

    protected:
        EntityType type;
        AABB box;
        //  General box offset for every frame
        vec2i boxOffset;

        //  The current drawn stuff
        Drawable* current;
        EntityData* data;
        EntityState state;
        vec2f trueSize;
        u32 currentIndex;
        //  True if the entity's drawables were initialized and allocated
        bool initialized;
};

#endif // WATER_ENTITY
