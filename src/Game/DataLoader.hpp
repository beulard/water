#ifndef WATER_DATALOADER
#define WATER_DATALOADER
#include <map>
#include <string>
#include <vector>
#include <fstream>
#include "Font.hpp"
#include "Wash.hpp"
#include "ShaderProgram.hpp"
#include "Texture.hpp"
#include "FileStream.hpp"
#include "wJSON.hpp"
#include "UILoadingScreen.hpp"
#include "Anim.hpp"
#include "Tile.hpp"
#include "Biome.hpp"

using namespace std;

enum DataType {
    DT_Texture,
    DT_Font,
    DT_Shader,
    DT_Sprite,
    DT_Anim,
    DT_Tile,
    DT_Biome,
    DT_Entity
};

//  Represents the actual stuff an entity needs to be drawn
//  We keep it in one struct object per entity to use the least
//  Amount of memory we can
struct EntityData {
        //  Type of the entity (sprite or anim)
        EntityType type;
        //  Drawn stuff, can be sprites or animations or anything
        std::vector< Drawable* > d;
        //  If a frame wants another offset, the pair will contain a true bool and this offset
        std::vector< std::vector< std::pair< bool, vec2i > > > boxOffsets;
        //  If a frame has a different box size, the pair has a true bool and the size
        std::vector< std::vector< std::pair< bool, vec2f > > > boxSizes;
};

bool operator==( string s1, string s2 );
string RemoveJSONExtension( const string& s );
string GetRealPath( const string& file, DataType dt );

typedef pair< string, DataType > DataInfo;

class DataLoader
{
    public:
        DataLoader();
        ~DataLoader();

        void Clean();

        //  The same thing actually applies for everything : if a data file has dependencies, you must
        //  Add them to the loading list beforehand
        void Add( const string& file, DataType dt );
        void AddList( const string& file, DataType dt );
        void AddDir( const string& dir, DataType dt );
        //  Can remove a single data object from the 'toLoad' vector.
        void Remove( const string& name, DataType dt );

        void LoadSilent();
        void LoadWithScreen();
        void OnUpdate( UILoadingScreen& screen, float progress );
        void Load( const UpdateFunc& uf );

        //  Those data object are shared, and thus should not be copied
        //  You should always use these functions to store the address in a reference
        //  e.g. Font& f = Global::dataLoader->GetFont( "soup" );
        //       Text t( "soup", &f, Color::Blue );
        ShaderProgram&      GetShaderProgram( const string& name ) const;
        Texture&            GetTexture( const string& name ) const;
        Font&               GetFont( const string& name, u32 size ) const;

        //  All these functions return the true reference to the loaded object,
        //  that should be copied by the caller
        const Sprite&       GetSprite( const string& name ) const;
        const Anim&         GetAnim( const string& name ) const;
        const Tile&         GetTile( const string& name ) const;
        const Biome&        GetBiome( const string& name ) const;
        const Entity&       GetEntity( const string& name ) const;
        //  Get the map with all loaded biomes
        map< Wash, Biome >& GetBiomes();

    private:
        vector< DataInfo > toLoad;
        //  Those are shared data, not to be copied
        map< Wash, ShaderProgram* > shaderPrograms;
        map< Wash, Texture* > textures;
        map< Wash, Font* > fonts;
        map< Wash, EntityData* > entityData;

        //  Those are unique data, that have to be copied
        map< Wash, Sprite > sprites;
        map< Wash, Anim > anims;
        map< Wash, Tile > tiles;
        map< Wash, Biome > biomes;
        map< Wash, Entity > entities;
};

#endif // WATER_DATALOADER
