#ifndef WATER_WASH
#define WATER_WASH
#include <string>

typedef unsigned int u32;

//  Input a string, get a hashed u32.
class Wash
{
    public:
        Wash( const std::string& s );
        ~Wash();

        static Wash GetWash( const std::string& s );

        bool operator<( const Wash& wash ) const;
        bool operator==( const Wash& wash ) const;


        u32 washed;
    private:
        u32 GetWashed( const std::string& s );
};

#endif // WATER_WASH
