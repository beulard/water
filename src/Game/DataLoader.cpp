#include "DataLoader.hpp"
#include "Game.hpp"
#include "Quad.hpp"
#include "SpriteBatch.hpp"
#include <dirent.h>

using namespace std;

bool operator==( string s1, string s2 ) {
    return ( s1.compare( s2 ) == 0 ) ? 1 : 0;
}

string RemoveJSONExtension( const string& s ) {
    string path = s.substr( 0, s.find( ".json" ) );
    return path;
}

string GetRealPath( const string& file, DataType dt ) {
    string realPath;
    switch( dt ) {
        case DT_Shader :
            realPath = "data/shaders/" + file + ".json";
        break;
        case DT_Texture :
            realPath = "data/textures/" + file + ".json";
        break;
        case DT_Font :
            realPath = "data/fonts/" + file + ".json";
        break;
        case DT_Sprite :
            realPath = "data/sprites/" + file + ".json";
        break;
        case DT_Anim :
            realPath = "data/anims/" + file + ".json";
        break;
        case DT_Tile :
            realPath = "data/tiles/" + file + ".json";
        break;
        case DT_Biome :
            realPath = "data/biomes/" + file + ".json";
        break;
        case DT_Entity :
            realPath = "data/entities/" + file + ".json";
        break;
    }
    return realPath;
}

DataLoader::DataLoader() {

}

DataLoader::~DataLoader() {

}

void DataLoader::Clean() {
    Info( "Cleaning data loader..." );
    toLoad.clear();

    Info( "  Deallocating %d shader programs", shaderPrograms.size() );
    for( auto it = shaderPrograms.begin(); it != shaderPrograms.end(); it++ )
        delete it->second;
    Info( "  Deallocating %d entities/tiles", entityData.size() );
    for( auto it = entityData.begin(); it != entityData.end(); it++ ) {
        for( u32 i = 0; i < it->second->d.size(); ++i )
            delete it->second->d[i];
        delete it->second;
    }
    Info( "  Deallocating %d textures", textures.size() );
    for( auto it = textures.begin(); it != textures.end(); it++ )
        delete it->second;
    Info( "  Deallocating %d fonts", fonts.size() );
    for( auto it = fonts.begin(); it != fonts.end(); it++ )
        delete it->second;

    Info( "Done." );
}

void DataLoader::Add( const string& file, DataType dt ) {
    toLoad.push_back( DataInfo( GetRealPath( file, dt ), dt ) );
}

void DataLoader::AddList( const string& file, DataType dt ) {
    string path = GetRealPath( file, dt );
    FileStream fs( path, OM_Read );
    string json = fs.ReadAll();
    fs.Close();
    if( json == "" )
        return;
    wJSON root( path );

    wJSON items = root.GetItem( "items" );
    u32 sz = items.GetSize();
    for( u32 i = 0; i < sz; ++i ) {
        Add( items.GetItem( i ).String(), dt );
    }
}

void DataLoader::AddDir( const string& dir, DataType dt ) {
    string dirpath = "data/" + dir;
    DIR* pDir = NULL;
    dirent* pDirent = NULL;
    pDir = opendir( dirpath.c_str() );
    if( !pDir )
        Error( "Could not open directory '%s'", dirpath.c_str() );
    while( (pDirent = readdir( pDir )) ) {
        if( !pDirent )
            Error( "Error while reading directory '%s'", dirpath.c_str() );
        string name = pDirent->d_name;
        if( name != "." && name != ".." ) {
            //  We get the real name of the file
            string fileName = GetRealPath( name, dt );
            //  The function GetRealPath has added an useless '.json' at the end of the file so we need to get it off
            for( u32 i = 0; i < 5; ++i )
                fileName.pop_back();
            //  Then we can add the data file to our loading list
            toLoad.push_back( DataInfo( fileName, dt ) );
        }
    }
    closedir( pDir );
}

void DataLoader::Remove( const string& name, DataType dt ) {
    string realName = GetRealPath( name, dt );
    bool found = false;
    for( u32 i = 0; i < toLoad.size(); ++i ) {
        if( toLoad[i].first == realName ) {
            toLoad[i] = toLoad.back();
            toLoad.pop_back();
            found = true;
        }
    }
    if( !found )
        Warning( "Trying to remove inexistant file '%s' from loading list", realName.c_str() );
}

void DataLoader::LoadSilent() {
    UpdateFunc updatefunc = []( float ){};
    Load( updatefunc );
}

void DataLoader::LoadWithScreen() {
    UILoadingScreen screen;
    UpdateFunc updatefunc = std::bind( &DataLoader::OnUpdate, std::ref(*this), std::ref(screen), std::placeholders::_1 );
    screen.SetInfo( "Loading game data" );
    screen.Draw();
    Load( updatefunc );
    screen.StopDrawing();
    Info( "Done loading\n\n" );
}

void DataLoader::OnUpdate( UILoadingScreen& screen, float progress ) {
    if( Global::Water.state == GS_Loading ) {
        screen.SetProgress( progress );
        Global::renderer->Clear();
        Global::renderer->Render();
    }
}

void DataLoader::Load( const UpdateFunc& uf ) {
    for( u32 i = 0; i < toLoad.size(); ++i ) {
        wJSON root( toLoad[i].first );

        if( root.Exists() ) {

            //  Get the file name without the .json extension
            string path = RemoveJSONExtension( toLoad[i].first.c_str() );
            Wash id( path );

            const char* name = strrchr( path.c_str(), '/' ) + 1;

            /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// ///
            /// /// /// /// /// /// /// /// /// /// SHADER /// /// /// /// /// /// /// /// /// /// ///
            /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// ///

            if( toLoad[i].second == DT_Shader ) {
                Info( "Loading shader '%s'", name );
                VertexShader* vsh = new VertexShader;
                FragmentShader* fsh = new FragmentShader;
                ShaderProgram* prg = new ShaderProgram;
                prg->SetVS( *vsh );
                prg->SetFS( *fsh );

                string file = name;

                bool camera = root.GetItem( "camera" ).Bool();
                FileStream fs( "data/raw/" + file + ".vs" );
                string vshSrc = fs.ReadAll();
                fs.Close();
                fs.Open( "data/raw/" + file + ".fs" );
                string fshSrc = fs.ReadAll();
                fs.Close();
                vsh->Load( vshSrc.c_str() );
                fsh->Load( fshSrc.c_str() );
                prg->Link();
                prg->SetCameraUse( camera );
                shaderPrograms.insert( pair< Wash, ShaderProgram* >( id, prg ) );
            }

            /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// ///
            /// /// /// /// /// /// /// /// /// /// /// TEXTURE /// /// /// /// /// /// /// /// /// ///
            /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// ///

            else if( toLoad[i].second == DT_Texture ) {
                Info( "Loading texture '%s'", name );
                Texture* texture = NULL;
                string tex = root.GetItem( "texture" ).String();
                string shader = root.GetItem( "shader" ).String();

                int depth = root.GetItem( "depth" ).Uint();

                if( depth > 5 ) {
                    Warning( "Trying to load a texture with depth > 5 ! Setting it to 5" );
                    depth = 5;
                }
                if( depth < 0 ) {
                    Warning( "Trying to load a texture with depth < 0 ! Setting it to 0" );
                    depth = 0;
                }
                ShaderProgram& sp = GetShaderProgram( shader );
                string realPath = "data/raw/" + tex + ".png";


                GLuint texid = 0;
                texture = FileStream::ReadPng( realPath );
                glActiveTexture( GL_TEXTURE0 );
                glGenTextures( 1, &texid );
                glBindTexture( GL_TEXTURE_2D, texid );
                glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );
                glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
                glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );

                if( texture->GetType() == CT_RGB )
                    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB, texture->GetSize().x, texture->GetSize().y, 0, GL_RGB, GL_UNSIGNED_BYTE, &texture->buffer[0] );
                else
                    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, texture->GetSize().x, texture->GetSize().y, 0, GL_RGBA, GL_UNSIGNED_BYTE, &texture->buffer[0] );

                texture->SetID( texid );
                texture->buffer.clear();
                texture->SetName( name );

                textures.insert( pair< Wash, Texture* >(id, texture) );
                Global::Water.renderer.AddSpriteBatch( name, new SpriteBatch( texture, &sp, (RenderDepth)depth ) );
            }

            /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// ///
            /// /// /// /// /// /// /// /// /// /// /// FONT /// /// /// /// /// /// /// /// /// ///
            /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// ///

            else if( toLoad[i].second == DT_Font ) {
                string file = root.GetItem( "file" ).String();

                //  Get all the font sizes specified in the json file
                wJSON sizes = root.GetItem( "sizes" );
                vector< u32 > s( sizes.GetSize() );
                for( u32 j = 0; j < s.size(); ++j )
                    s[j] = sizes.GetItem( j ).Uint();

                //  Then load all those fonts
                for( u32 j = 0; j < s.size(); ++j ) {
                    Info( "Loading font '%s' with size %d", name, s[j] );

                    Font* font = new Font;
                    string realPath = "data/raw/" + file + ".ttf";

                    //  Get the file name without the .json extension
                    string sz = IntToString( s[j] );
                    string spath = path;
                    //  Add the size to the identifier string, separated by a dot(.)
                    spath += '.' + sz;
                    //  Set the new washed value to use in our font map
                    id = Wash( spath );

                    font->Load( realPath.c_str(), s[j] );
                    font->SetName( spath );
                    fonts.insert( pair< Wash, Font* >( id, font ) );
                }
            }

            /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// ///
            /// /// /// /// /// /// /// /// /// /// /// SPRITE /// /// /// /// /// /// /// /// /// ///
            /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// ///

            else if( toLoad[i].second == DT_Sprite ) {
                Info( "Loading sprite '%s'", name );

                string tex = root.GetItem( "texture" ).String();

                wJSON subRect = root.GetItem( "subRect" );

                int rect[4];
                bool null = true;
                for( u32 j = 0; j < 4; ++j ) {
                    rect[j] = subRect.GetItem( j ).Int();
                    if( rect[j] != 0 )
                        null = false;
                }

                Texture& texture = GetTexture( tex );

                if( null ) {
                    rect[0] = 0;
                    rect[1] = 0;
                    rect[2] = texture.GetSize().x;
                    rect[3] = texture.GetSize().y;
                }

                Sprite sprite( &texture, Recti( vec2i( rect[0], rect[1] ), vec2i( rect[2], rect[3] ) ) );

                sprites.insert( pair< Wash, Sprite >( id, sprite ) );
            }

            /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// ///
            /// /// /// /// /// /// /// /// /// /// /// ANIM /// /// /// /// /// /// /// /// /// ///
            /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// ///

            else if( toLoad[i].second == DT_Anim ) {
                Info( "Loading anim '%s'", name );

                string tex = root.GetItem( "texture" ).String();

                wJSON subRects = root.GetItem( "subRects" );
                wJSON times = root.GetItem( "times" );
                wJSON rotations = root.GetItem( "rotations" );
                wJSON offsets = root.GetItem( "offsets" );


                u32 subRectsSz = subRects.GetSize();
                u32 timesSz = times.GetSize();
                u32 rotationsSz = rotations.GetSize();
                u32 offsetsSz = offsets.GetSize();


                vector<Recti> sr( subRectsSz );
                vector<float> t( timesSz );
                vector<float> r( rotationsSz );
                vector<vec2i> os( offsetsSz );

                Texture& texture = GetTexture( tex );

                for( u32 j = 0; j < subRectsSz; ++j ) {
                    bool null = true;
                    wJSON rect = subRects.GetItem( j );


                    int values[4];
                    for( u32 k = 0; k < 4; ++k ) {
                        values[k] = rect.GetItem( k ).Int();
                        if( values[k] != 0 )
                            null = false;
                    }
                    if( null ) {
                        values[2] = texture.GetSize().x;
                        values[3] = texture.GetSize().y;
                    }
                    sr[j] = Recti( values[0], values[1], values[2], values[3] );
                }

                for( u32 j = 0; j < timesSz; ++j ) {
                    t[j] = times.GetItem( j ).Float();
                }

                for( u32 j = 0; j < rotationsSz; ++j ) {
                    r[j] = rotations.GetItem( j ).Float();
                }

                for( u32 j = 0; j < offsetsSz; ++j ) {
                    wJSON offset = offsets.GetItem( j );
                    os[j] = vec2i( offset.GetItem( 0 ).Int(), offset.GetItem( 1 ).Int() );
                }

                auto it = anims.insert( pair< Wash, Anim >( id, Anim() ) ).first;

                for( u32 j = 0; j < sr.size(); ++j ) {
                    Sprite s( &texture, sr[j] );
                    it->second.AddFrame( s, t[j], r[j], os[j] );
                }
            }

            /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// ///
            /// /// /// /// /// /// /// /// /// /// /// TILE /// /// /// /// /// /// /// /// /// ///
            /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// ///

            else if( toLoad[i].second == DT_Tile ) {
                Info( "Loading tile '%s'", name );
                string type = root.GetItem( "type" ).String();
                wJSON info = root.GetItem( "tile_info" );

                EntityData* ed = new EntityData;

                Tile& t = tiles.insert( pair< Wash, Tile >( id, Tile() ) ).first->second;

                int tileID = info.GetItem( "id" ).Int();
                if( tileID < 0 ) {
                    Warning( "Loading a tile with a negative ID" );
                    tileID = 0;
                }

                t.SetTileID( tileID );
                Tile::Tiles.insert( std::pair< u32, Tile* >( (u32)tileID, &t ) );

                string ct = info.GetItem( "coll_type" ).String();
                int min_temp = info.GetItem( "min_temp" ).Int();
                int max_temp = info.GetItem( "max_temp" ).Int();
                int min_hum = info.GetItem( "min_hum" ).Int();
                int max_hum = info.GetItem( "max_hum" ).Int();
                if( ct == "solid" )
                    t.SetCollisionType( CT_Solid );
                else if( ct == "liquid" )
                    t.SetCollisionType( CT_Liquid );
                else
                    t.SetCollisionType( CT_None );

                t.SetMinTemp( min_temp );
                t.SetMaxTemp( max_temp );
                t.SetMinHum( min_hum );
                t.SetMaxHum( max_hum );

                if( type == "color" ) {
                    wJSON color = root.GetItem( "color" );
                    ed->type = ET_Quad;
                    ed->d.push_back( new Quad );
                    dynamic_cast< Quad* >( ed->d.back() )->SetColor( Color( color.GetItem( 0 ).Uint(), color.GetItem( 1 ).Uint(), color.GetItem( 2 ).Uint(), color.GetItem( 3 ).Uint() ) );
                }

                else if( type == "sprite" ) {
                    ed->type = ET_Sprite;
                    string tex = root.GetItem( "texture" ).String();

                    Texture& texture = GetTexture( tex );

                    if( texture.GetSize().x == 1 && texture.GetSize().y == 1 ) {
                        //  For tiles that have a unique color (air, void, ...)
                        ed->d.push_back( new Sprite( &texture, Recti( 0, 0, 1, 1 ) ) );
                        dynamic_cast< Sprite* >( ed->d.back() )->SetSize( vec2i( 25, 25 ) );
                    }

                    else {
                        ed->d.resize( 16 );
                        //  The texture is used in this order for tiles
                        //  Blocks on right and bottom
                        ed->d[0] = new Sprite( &texture, Recti( 50, 0, 100, 50 ) );
                        //  Blocks on left, right and bottom
                        ed->d[1] = new Sprite( &texture, Recti( 100, 0, 150, 50 ) );
                        //  Blocks on left and bottom
                        ed->d[2] = new Sprite( &texture, Recti( 50, 0, 100, 50 ) );
                        dynamic_cast< Sprite* >( ed->d[2] )->FlipX();
                        //  Blocks on top, right and bottom
                        ed->d[3] = new Sprite( &texture, Recti( 50, 50, 100, 100 ) );
                        //  Blocks on each side
                        ed->d[4] = new Sprite( &texture, Recti( 100, 50, 150, 100 ) );
                        //  Blcoks on top, left and bottom
                        ed->d[5] = new Sprite( &texture, Recti( 50, 50, 100, 100 ) );
                        dynamic_cast< Sprite* >( ed->d[5] )->FlipX();
                        //  Blocks on top and right
                        ed->d[6] = new Sprite( &texture, Recti( 50, 100, 100, 150 ) );
                        //  Blcosk on top, left and right
                        ed->d[7] = new Sprite( &texture, Recti( 100, 100, 150, 150 ) );
                        //  Blocks on top and left
                        ed->d[8] = new Sprite( &texture, Recti( 50, 100, 100, 150 ) );
                        dynamic_cast< Sprite* >( ed->d[8] )->FlipX();
                        //  Block on right
                        ed->d[9] = new Sprite( &texture, Recti( 0, 0, 50, 50 ) );
                        //  Block on left
                        ed->d[10] = new Sprite( &texture, Recti( 0, 0, 50, 50 ) );
                        dynamic_cast< Sprite* >( ed->d[10] )->FlipX();
                        //  Block below
                        ed->d[11] = new Sprite( &texture, Recti( 0, 50, 50, 100 ) );
                        //  Block above
                        ed->d[12] = new Sprite( &texture, Recti( 0, 50, 50, 100 ) );
                        dynamic_cast< Sprite* >( ed->d[12] )->FlipY();
                        //  Blocks below and above
                        ed->d[13] = new Sprite( &texture, Recti( 150, 0, 200, 50 ) );
                        //  Blocks on right and left
                        ed->d[14] = new Sprite( &texture, Recti( 150, 50, 200, 100 ) );
                        //  No blocks on any side
                        ed->d[15] = new Sprite( &texture, Recti( 0, 100, 50, 150 ) );
                    }
                }
                else if( type == "anim" ) {
                    ed->d.resize( 16 );
                    ed->type = ET_Anim;
                    string tex = root.GetItem( "texture" ).String();
                    Texture& texture = GetTexture( tex );

                    u32 nbrFrames[16];
                    vector<float> frameTimes[16];
                    wJSON n = root.GetItem( "frameNbr" );
                    wJSON t = root.GetItem( "frameTimes" );

                    for( u32 j = 0; j < 16; ++j )
                        nbrFrames[j] = n.GetItem( j ).Int();

                    for( u32 j = 0; j < 16; ++j ) {
                        for( u32 k = 0; k < nbrFrames[j]; ++k ) {
                            wJSON tms = t.GetItem( j );
                            frameTimes[j].push_back( tms.GetItem( k ).Float() );
                        }
                    }

                    for( u32 j = 0; j < 16; ++j ) {
                        ed->d[j] = new Anim;
                        Anim* anim = dynamic_cast< Anim* >( ed->d[j] );
                        u32 x = j;

                        if( j == 2 )
                            x = 0;
                        if( j > 2 )
                            x = j - 1;

                        if( j == 5 )
                            x = 2;
                        if( j > 5 )
                            x = j - 2;

                        if( j == 8 )
                            x = 4;
                        if( j > 8 )
                            x = j - 3;

                        if( j >= 12 )
                            x = j - 4;
                        if( j >= 14 )
                            x = j - 5;



                        //  TODO NEEDS TESTING
                        for( u32 k = 0; k < nbrFrames[j]; ++k ) {
                            Sprite s( &texture, Recti( vec2i( 50 * k, 50 * x ), vec2i( 50 * k + 50, 50 * x + 50 ) ) );
                            if( j == 2 || j == 5 || j == 8 || j == 12 )
                                s.FlipX();
                            if( j == 14 )
                                s.FlipY();
                            anim->AddFrame( s, frameTimes[j][k] );
                        }
                    }
                }
                t.SetData( *ed );
                entityData.insert( pair< Wash, EntityData* >( id, ed ) );
            }

            /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// ///
            /// /// /// /// /// /// /// /// /// /// /// BIOME /// /// /// /// /// /// /// /// /// ///
            /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// ///

            else if( toLoad[i].second == DT_Biome ) {
                Info( "Loading biome '%s'", name );
                Biome biome;
                int minsz = root.GetItem( "min_size" ).Int();
                int maxsz = root.GetItem( "max_size" ).Int();
                string type = root.GetItem( "terrain_type" ).String();
                if( type == "rocky" )
                    biome.SetTerrainType( TT_Rocky );
                else if( type == "flat" )
                    biome.SetTerrainType( TT_Flat );
                else if( type == "plains" )
                    biome.SetTerrainType( TT_Plains );
                else if( type == "dunes" )
                    biome.SetTerrainType( TT_Dunes );
                else if( type == "river" )
                    biome.SetTerrainType( TT_River );
                else
                    Error( "Not a valid terrain type" );
                int temperature = root.GetItem( "temperature" ).Int();
                int humidity = root.GetItem( "humidity" ).Int();
                biome.SetMinSize( minsz );
                biome.SetMaxSize( maxsz );
                biome.SetTemperature( temperature );
                biome.SetHumidity( humidity );
                biomes.insert( pair< Wash, Biome >( id, biome ) );
            }

            /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// ///
            /// /// /// /// /// /// /// /// /// /// /// ENTITY /// /// /// /// /// /// /// /// /// ///
            /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// ///

            else if( toLoad[i].second == DT_Entity ) {
                Info( "Loading entity '%s'", name );
                Entity ent;
                EntityData* ed = new EntityData;
                ed->type = ET_Anim;
                string tex = root.GetItem( "texture" ).String();
                Texture& texture = GetTexture( tex );

                wJSON anims = root.GetItem( "anims" );
                wJSON boxOffset = root.GetItem( "box_offset" );
                wJSON boxSize = root.GetItem( "box_size" );

                vec2i bo( boxOffset.GetItem( 0 ).Int(), boxOffset.GetItem( 1 ).Int() );
                vec2f bs( boxSize.GetItem( 0 ).Float(), boxSize.GetItem( 1 ).Float() );

                u32 nbrAnims = anims.GetSize();
                //  This vec2 represents where the "reading cursor" is
                vec2i cursor;
                for( u32 j = 0; j < nbrAnims; ++j ) {
                    ed->d.push_back( new Anim );
                    Anim* a = dynamic_cast< Anim* >( ed->d.back() );
                    ed->d.push_back( new Anim );
                    Anim* reversed = dynamic_cast< Anim* >( ed->d.back() );
                    wJSON anim = anims.GetItem( j );
                    wJSON size = anim.GetItem( "size" );
                    wJSON frameTimes = anim.GetItem( "frame_times" );
                    //  These arrays are facultative, so we have false as the second param
                    wJSON os = anim.GetItem( "box_offsets", false );
                    wJSON s = anim.GetItem( "box_sizes", false );

                    u32 frameNbr = frameTimes.GetSize();
                    ed->boxOffsets.push_back( vector< pair< bool, vec2i > >() );
                    ed->boxOffsets.push_back( vector< pair< bool, vec2i > >() );
                    ed->boxSizes.push_back( vector< pair< bool, vec2f > >() );
                    ed->boxSizes.push_back( vector< pair< bool, vec2f > >() );

                    vec2i sz( size.GetItem( 0 ).Int(), size.GetItem( 1 ).Int() );

                    for( u32 k = 0; k < frameNbr; ++k ) {
                        u32 index = ed->boxOffsets.size() - 1;
                        ed->boxOffsets[index - 1].push_back( pair< bool, vec2i >( false, vec2i( 0, 0 ) ) );
                        ed->boxOffsets[index].push_back( pair< bool, vec2i >( false, vec2i( 0, 0 ) ) );
                        ed->boxSizes[index - 1].push_back( pair< bool, vec2f >( false, vec2f( 0, 0 ) ) );
                        ed->boxSizes[index].push_back( pair< bool, vec2f >( false, vec2f( 0, 0 ) ) );
                        if( os.Exists() ) {
                            boxOffset = os.GetItem( k );
                            ed->boxOffsets[index - 1][k].first = boxOffset.GetItem( 0 ).Bool();
                            ed->boxOffsets[index][k].first = ed->boxOffsets[index - 1][k].first;
                            ed->boxOffsets[index - 1][k].second = vec2i( boxOffset.GetItem( 1 ).Int(), boxOffset.GetItem( 2 ).Int() );
                            ed->boxOffsets[index][k].second = ed->boxOffsets[index - 1][k].second;
                        }
                        if( s.Exists() ) {
                            boxSize = s.GetItem( k );
                            ed->boxSizes[index - 1][k].first = boxSize.GetItem( 0 ).Int();
                            ed->boxSizes[index][k].first = ed->boxSizes[index - 1][k].first;
                            ed->boxSizes[index - 1][k].second = vec2f( boxSize.GetItem( 1 ).Float(), boxSize.GetItem( 2 ).Float() );
                            ed->boxSizes[index][k].second = ed->boxSizes[index - 1][k].second;
                        }
                        Sprite s( &texture, Recti( vec2i( sz.x * k, cursor.y ), vec2i( sz.x + sz.x * k, cursor.y + sz.y ) ) );
                        Sprite rev = s;
                        rev.FlipX();
                        a->AddFrame( s, frameTimes.GetItem( k ).Float());
                        reversed->AddFrame( rev, frameTimes.GetItem( k ).Float() );
                        cursor.x += sz.x;
                    }
                    cursor.y += sz.y;
                }

                ent.SetData( *ed );
                ent.SetBoxOffset( bo );
                ent.SetSize( vec2ui( (u32)bs.x, (u32)bs.y ) );
                entities.insert( pair< Wash, Entity >( id, ent ) );
                entityData.insert( pair< Wash, EntityData* >( id, ed ) );
            }
        }
        //  Execute update function with current progress
        uf( (float)i / (float)toLoad.size() );
    }
    toLoad.clear();
}

ShaderProgram& DataLoader::GetShaderProgram( const string& name ) const {
    string realName = "data/shaders/" + name;
    Wash w( realName );
    for( auto it = shaderPrograms.begin(); it != shaderPrograms.end(); it++ ) {
        if( it->first == w )
            return *it->second;
    }
    Error( "Shader program '%s' not found in game data !", name.c_str() );
    return *shaderPrograms.begin()->second;
}

Texture& DataLoader::GetTexture( const string& name ) const {
    string realName = "data/textures/" + name;
    Wash w( realName );
    for( auto it = textures.begin(); it != textures.end(); it++ ) {
        if( it->first == w )
            return *it->second;
    }
    Error( "Texture '%s' not found in game data !", name.c_str() );
    return *textures.begin()->second;
}

Font& DataLoader::GetFont( const string& name, u32 size ) const {
    string realName = "data/fonts/" + name + '.' + IntToString( size );
    Wash w( realName );
    for( auto it = fonts.begin(); it != fonts.end(); it++ ) {
        if( it->first == w )
            return *it->second;
    }
    Error( "Font '%s' in size %d not found in game data !", name.c_str(), size );
    return *fonts.begin()->second;
}

const Sprite& DataLoader::GetSprite( const string& name ) const {
    string realName = "data/sprites/" + name;
    Wash w( realName );
    for( auto it = sprites.begin(); it != sprites.end(); it++ ) {
        if( it->first == w )
            return it->second;
    }
    Error( "Sprite '%s' not found in game data !", name.c_str() );
    return sprites.begin()->second;
}

const Anim& DataLoader::GetAnim( const string& name ) const {
    string realName = "data/anims/" + name;
    Wash w( realName );
    for( auto it = anims.begin(); it != anims.end(); it++ ) {
        if( it->first == w )
            return it->second;
    }
    Error( "Anim '%s' not found in game data !", name.c_str() );
    return anims.begin()->second;
}

const Tile& DataLoader::GetTile( const string& name ) const {
    string realName = "data/tiles/" + name;
    Wash w( realName );
    for( auto it = tiles.begin(); it != tiles.end(); it++ ) {
        if( it->first == w ) {
            return it->second;
        }
    }
    Error( "Tile '%s' not found in game data !", name.c_str() );
    return tiles.begin()->second;
}

const Biome& DataLoader::GetBiome( const string& name ) const {
    string realName = "data/biomes/" + name;
    Wash w( realName );
    for( auto it = biomes.begin(); it != biomes.end(); it++ ) {
        if( it->first == w )
            return it->second;
    }
    Error( "Biome '%s' not found in game data !", name.c_str() );
    return biomes.begin()->second;
}

const Entity& DataLoader::GetEntity( const string& name ) const {
    string realName = "data/entities/" + name;
    Wash w( realName );
    for( auto it = entities.begin(); it != entities.end(); it++ ) {
        if( it->first == w )
            return it->second;
    }
    Error( "Entity '%s' not found in game data !", name.c_str() );
    return entities.begin()->second;
}

map< Wash, Biome >& DataLoader::GetBiomes() {
    return biomes;
}
