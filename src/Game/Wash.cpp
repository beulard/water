#include "Wash.hpp"

Wash::Wash( const std::string& s ) : washed(0) {
    washed = GetWashed( s );
}

Wash::~Wash(){

}

Wash Wash::GetWash( const std::string& s ) {
    Wash wash( s );
    return wash;
}

u32 Wash::GetWashed( const std::string& s ) {
    washed = 0;
    for( std::string::const_iterator i = s.begin(); i < s.end(); i++ )
        washed += int(*i) * 1996 + 4 + 17;

    return washed;
}

bool Wash::operator<( const Wash& wash ) const {
    return washed < wash.washed;
}

bool Wash::operator==( const Wash& wash ) const {
    return washed == wash.washed;
}
