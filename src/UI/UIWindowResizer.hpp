#ifndef WATER_UIWINDOWRESIZER
#define WATER_UIWINDOWRESIZER
#include "Widget.hpp"

class UIWindowResizer : public Widget
{
    public:
        UIWindowResizer();
        ~UIWindowResizer();

        virtual bool OnHover();
        virtual bool OnPress();
        virtual bool OnRelease();

};

#endif // WATER_UIWINDOWRESIZER
