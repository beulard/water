#include "UIText.hpp"
#include "Common.hpp"

UIText::UIText(){
    mode = RM_None;
    text.SetDepth( RD_UITexts );
    text.SetRenderMode( RM_UIText );
}

UIText::~UIText(){

}

void UIText::Move( const vec2i& offset ) {
    Widget::Move( offset );
    text.Move( offset );
}

void UIText::SetColor( const Color& col ) {
    text.SetColor( col );
}

void UIText::SetFont( Font* f ) {
    text.SetFont( f );
}

void UIText::SetString( const std::string& s ) {
    text.SetString( s );
    size = text.GetSize();
}

void UIText::Sleep() {
    if( drawn )
        StopDrawing();
}

void UIText::Activate() {
    if( !drawn )
        Draw();
}

void UIText::Draw() {
    if( !drawn )
        text.Draw();
    else
        Warning( "Trying to add an ui text to the renderer twice" );
    drawn = true;
}

void UIText::StopDrawing() {
    if( drawn )
        text.StopDrawing();
    else
        Warning( "Trying to remove an ui text from the renderer but it is not in" );
    drawn = false;
}

void UIText::Render() {
    text.Render();
}

void UIText::SetDepth( RenderDepth d ) {
    depth = d;
    text.SetDepth( d );
}
