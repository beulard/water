#include "RootWidget.hpp"

RootWidget::RootWidget() {

}

RootWidget::~RootWidget() {

}

void RootWidget::AddChild( Widget* child ) {
    children.push_back( child );
}

void RootWidget::Clean() {
    Info( "Cleaning root widget..." );
    ClearChildren();
    Append( " Done." );
}

void RootWidget::ClearChildren() {
    while( !children.empty() ) {
        children.pop_back();
    }
}
