#ifndef WATER_ROOTWIDGET
#define WATER_ROOTWIDGET
#include "UIMainMenu.hpp"

//  The root widget is the entity that keeps other parent widgets in its 'children' vector and passes
//  input information and calls callbacks to make the interface work properly.
//  The children system is not the same as for other widgets because here only adresses are kept
//  whereas other classes allocate and delete their children.
class RootWidget : public Widget
{
    public:
        RootWidget();
        ~RootWidget();

        void Clean();

        void AddChild( Widget* child );
        void ClearChildren();
};

#endif // WATER_ROOTWIDGET
