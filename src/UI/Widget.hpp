#ifndef WATER_WIDGET
#define WATER_WIDGET
#include <vector>
#include "vec2.hpp"
#include "ShaderProgram.hpp"
#include "Drawable.hpp"

typedef unsigned int u32;

//  State of the widget when it interacts with the mouse
//  Not all type of widgets will use this
enum WidgetState {
    WS_Normal,
    WS_Hover,
    WS_Press
};

class Widget : public Drawable
{
    public:
        Widget();
        virtual ~Widget();

        virtual void SetPosition( const vec2i& pos );
        virtual void Move( const vec2i& offset );
        const vec2i& GetPosition()   const;

        virtual void Draw();
        virtual void StopDrawing();
        virtual void Render();


        //  Detection of mouse on the widget has to be done by the subclasses to reduce number of tests
        //  Only button pressing information is given by the rootwidget in the Game::Run function
        virtual bool OnHover();
        virtual bool OnPress();
        virtual bool OnRelease();

        //  Lets the outside world access a widget's children
        Widget* GetChild( u32 id );

        void SetParent( Widget* w );

        virtual void SetDepth( RenderDepth d );

        virtual void Sleep();
        virtual void Activate();
        bool IsActive() const;

        bool HasChildren()  const;
        //  This function is supposed to be used on widgets that have no direct parent but that may have children, like windows, or menus, or even simple buttons that need to always be on the screen
        void AddToRoot();

    protected:
        //  Pointer to direct parent, if there is one. Doesn't have to be used, it's just for utility
        Widget* parent;
        std::vector< Widget* > children;
        bool active;
        WidgetState state;
};

#endif // WATER_WIDGET
