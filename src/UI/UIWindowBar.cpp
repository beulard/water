#include "UIWindowBar.hpp"
#include "UIButton.hpp"
#include "UIColored.hpp"
#include "Quad.hpp"
#include "DataLoader.hpp"
#include "InputManager.hpp"

UIWindowBar::UIWindowBar() : mouseOffset(vec2i( 0, 0 )) {
	Font& f = Global::dataLoader->GetFont( "dejavusans", 12 );
    //  Bar background (a simple color)
    children.push_back( new UIColored );
	Colored* quad = new Quad;
    quad->SetColor( Color( 100, 100, 100, 200 ) );
    UIColored* background = dynamic_cast<UIColored*>( children.back() );
	background->SetColored( quad );
    background->SetSize( vec2ui( 100, 10 ) );
    //  Bar icon (top left)
    children.push_back( new UISprite( Global::dataLoader->GetSprite( "baricon" ) ) );
    children.back()->Move( vec2i( 3, 3 ) );

    //  Bar title
    children.push_back( new UIText );
    UIText* title = dynamic_cast<UIText*>( children.back() );
    title->SetFont( &f );
	title->SetString( "<3" );
    children.back()->Move( vec2i( 18, 4 ) );

    //  Bar close button (top right)
    children.push_back( new UIButton );
    UIButton* close = dynamic_cast<UIButton*>( children.back() );
    close->SetNormalSprite( Global::dataLoader->GetSprite( "close_normal" ) );
    close->SetHoverSprite( Global::dataLoader->GetSprite( "close_hover" ) );
    close->SetPressSprite( Global::dataLoader->GetSprite( "close_press" ) );
    close->SetSize( vec2ui( 11, 11 ) );
    close->SetReleaseCallback( [this]() {
        parent->Sleep();
    } );
}

UIWindowBar::~UIWindowBar() {

}

void UIWindowBar::SetTitle( const std::string& title ) {
    dynamic_cast<UIText*>( children[2] )->SetString( title );
}

void UIWindowBar::SetSize( const vec2ui& sz ) {
    Widget::SetSize( sz );
    children[0]->SetSize( sz );
    size = sz;
    children.back()->SetPosition( vec2i(position + vec2i( sz.x, 0 ) - vec2i(children.back()->GetSize().x + 3, -3 ) ) );
}

bool UIWindowBar::OnHover() {
    Recti rect( vec2i( position.x, position.y ), vec2i( position.x + size.x, position.y + size.y ) );
    if( rect.Contains( Global::inputMgr->GetMousePos() ) ) {
        if( !children[3]->OnHover() ) {
            state = WS_Hover;
            return true;
        }
        else {
            state = WS_Normal;
            return true;
        }
    }
    else {
        state = WS_Normal;
        return false;
    }
}

bool UIWindowBar::OnPress() {
    vec2i mousePos = Global::inputMgr->GetMousePos();
    if( state == WS_Hover ) {
        state = WS_Press;
        mouseOffset = vec2i( position - mousePos );
    }
    if( state == WS_Press ) {
        parent->SetPosition( Global::inputMgr->GetMousePos() + mouseOffset );
        return true;
    }
    else {
        if( children[3]->OnPress() )
            return true;
        else
            return false;
    }
}

bool UIWindowBar::OnRelease() {
    if( state == WS_Press ) {
        parent->SetPosition( Global::inputMgr->GetMousePos() + mouseOffset );
        state = WS_Normal;
        return true;
    }
    else {
        if( children[3]->OnRelease() )
            return true;
        else
            return false;
    }
}

