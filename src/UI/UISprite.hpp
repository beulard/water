#ifndef WATER_UISPRITE
#define WATER_UISPRITE
#include "Widget.hpp"
#include "Sprite.hpp"
#include "Texture.hpp"

class UISprite : public Widget
{
    public:
        UISprite();
        UISprite( Texture* t, const vec2i& pos, const vec2ui& size );
        UISprite( const Sprite& s );
        void operator=( const Sprite& s );
        ~UISprite();

        void SetTexture( Texture* t );

        virtual void Move( const vec2i& offset );

        virtual void SetSize( const vec2ui& sz );

		virtual void SetDepth( RenderDepth d );

        void SetSubRect( const Recti& rect );

        virtual void Sleep();
        virtual void Activate();

        virtual void Draw();
		virtual void StopDrawing();

    protected:
        Sprite sprite;
};

#endif // WATER_UISPRITE
