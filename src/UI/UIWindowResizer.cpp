#include "UIWindowResizer.hpp"
#include "UIColored.hpp"
#include "Quad.hpp"
#include "Rect.hpp"
#include "InputManager.hpp"
#include "Common.hpp"

UIWindowResizer::UIWindowResizer() {
    //  Resizer's background
    children.push_back( new UIColored );
    Colored* q = new Quad;
    q->SetColor( Color( 30, 30, 30, 30 ) );
    q->SetDepth( RD_Texts );
    dynamic_cast<UIColored*>( children.back() )->SetColored( q );
}

UIWindowResizer::~UIWindowResizer() {

}

bool UIWindowResizer::OnHover() {
    Recti rect( vec2i( position.x, position.y ), vec2i( position.x + size.x, position.y + size.y ) );
    if( rect.Contains( Global::inputMgr->GetMousePos() ) ) {

    }
    return false;
}

bool UIWindowResizer::OnPress() {
    return false;
}

bool UIWindowResizer::OnRelease() {
    return false;
}
