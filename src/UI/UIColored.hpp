#ifndef WATER_UICOLORED
#define WATER_UICOLORED
#include "Widget.hpp"

class Colored;

class UIColored : public Widget
{
    public:
        UIColored();
        ~UIColored();

        void SetColored( Colored* s );
        virtual void SetSize( const vec2ui& sz );
        virtual void SetDepth( RenderDepth d );

        virtual void Move( const vec2i& offset );

        virtual void Draw();
        virtual void StopDrawing();

        virtual void Sleep();
        virtual void Activate();

private :
        Colored* colored;
};

#endif // WATER_UICOLORED
