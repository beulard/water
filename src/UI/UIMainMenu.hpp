#ifndef WATER_UIMAINMENU
#define WATER_UIMAINMENU
#include "UIButton.hpp"
#include "UIText.hpp"
#include "Rect.hpp"

class UIMainMenu : public Widget
{
    public:
        UIMainMenu();
        ~UIMainMenu();

        virtual void Activate();

};

#endif // WATER_UIMAINMENU
