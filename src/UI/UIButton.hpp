#ifndef WATER_UIBUTTON
#define WATER_UIBUTTON
#include "UIWindow.hpp"
#include "UISprite.hpp"
#include "UIText.hpp"
#include <functional>

typedef std::function< void() > ButtonCallback;

class UIButton : public Widget
{
    public:
        UIButton();
        ~UIButton();

        void SetNormalSprite( const Sprite& s );
        void SetHoverSprite( const Sprite& s );
        void SetPressSprite( const Sprite& s );
        void SetDisabledSprite( const Sprite& s );
        
        void Enable();
        void Disable();

        void SetString( const std::string& s );
		void SetTextColor( const Color& col );
        void SetFont( Font* f );

        virtual bool OnHover();
        virtual bool OnPress();
        virtual bool OnRelease();
        
        //  Overload those functions because buttons don't just tell all their 
        //  children to draw or stop drawing when we call Activate or Sleep
        virtual void Activate();
        virtual void Sleep();

        virtual void SetSize( const vec2ui& sz );

        void SetHoverCallback( const ButtonCallback& cb );
        void SetPressCallback( const ButtonCallback& cb );
        void SetReleaseCallback( const ButtonCallback& cb );


        //  Overloaded drawing functions
        virtual void Draw();
        virtual void StopDrawing();

    protected:
        ButtonCallback hoverCallback;
        ButtonCallback pressCallback;
        ButtonCallback releaseCallback;
        
        bool enabled;

        //  Does the button have a descriptive text on it ?
        bool usesText;
};

#endif // WATER_UIBUTTON
