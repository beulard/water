#ifndef WATER_UIWINDOWBAR
#define WATER_UIWINDOWBAR
#include "Widget.hpp"
#include <string>

class UIWindowBar : public Widget
{
    public:
        UIWindowBar();
        ~UIWindowBar();

        void SetTitle( const std::string& title );

        virtual void SetSize( const vec2ui& sz );

        virtual bool OnHover();
        virtual bool OnPress();
        virtual bool OnRelease();


    protected:
        //  Offset of the mouse with the top left of the bar
        //  Used when the bar is moved
        vec2i mouseOffset;
};

#endif // WATER_UIWINDOWBAR
