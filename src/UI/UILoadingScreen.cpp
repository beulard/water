#include "UILoadingScreen.hpp"
#include "DataLoader.hpp"

UILoadingScreen::UILoadingScreen() {
    SetRenderMode( RM_None );

    //  Bar outline (the rectangle around the loading bar)
    children.push_back( new UISprite );
    UISprite* barOutline = dynamic_cast<UISprite*>(children.back());
    *barOutline = Global::dataLoader->GetSprite( "loading_bar_outline" );
    barOutline->SetSize( vec2ui( Global::winSz->x / 2, 50 ) );
    barOutline->Move( *Global::winSz / 2 - barOutline->GetSize() / 2 );

    //  Loading bar
    children.push_back( new UISprite );
    UISprite* barInside = dynamic_cast<UISprite*>(children.back());
    *barInside = Global::dataLoader->GetSprite( "loading_bar_inside" );
    barInside->SetSize( vec2ui( 0, 42 ) );

    float scale = (float)barOutline->GetSize().x / 150.f;
    barInside->SetPosition( vec2i( barOutline->GetPosition().x + 3.5f * scale / 2, barOutline->GetPosition().y + 4 ) );

    Font& textFont = Global::dataLoader->GetFont( "dejavusans", 32 );

    //  A little string that says Loading...
    children.push_back( new UIText );
    UIText* title = dynamic_cast<UIText*>(children.back());
    title->SetFont( &textFont );
    title->SetString( "Loading..." );
    title->SetPosition( vec2i( Global::winSz->x / 2 - title->GetSize().x / 2, barOutline->GetPosition().y - barOutline->GetSize().y / 2 - title->GetSize().y / 2 - 6 ) );

    //  An info string that gives a little message about what's happening atm
    children.push_back( new UIText );
    UIText* info = dynamic_cast<UIText*>(children.back());
    info->SetFont( &textFont );
    info->SetString( "Info here" );
    info->SetPosition( vec2i( Global::winSz->x / 2 - info->GetSize().x / 2, barOutline->GetPosition().y + barOutline->GetSize().y + 6 ) );
}

UILoadingScreen::~UILoadingScreen(){

}

void UILoadingScreen::SetProgress( float progress ) {
    float scale = (float)children[0]->GetSize().x / 150.f;
    children[1]->SetSize( vec2ui( ( (float)children[0]->GetSize().x - 4 * scale ) * progress, 42 ) );
}

void UILoadingScreen::SetInfo( const std::string& msg ) {
    UIText* info = dynamic_cast<UIText*>(children[3]);
    info->SetString( msg );
    info->SetPosition( vec2i( Global::winSz->x / 2 - info->GetSize().x / 2, children[0]->GetPosition().y + children[0]->GetSize().y + 6 ) );
}
