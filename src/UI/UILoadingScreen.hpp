#ifndef WATER_UILOADINGSCREEN
#define WATER_UILOADINGSCREEN
#include "UISprite.hpp"
#include "UIText.hpp"
#include <functional>

//  The UpdateFunc is a callback function called when something was done during loading
//  and we need to do something outside of the loading, like updating the render
typedef std::function< void( float ) > UpdateFunc;

class UILoadingScreen : public Widget
{
    public:
        UILoadingScreen();
        ~UILoadingScreen();

        //  Sets the bar progress in percent
        void SetProgress( float progress );

        //  Sets the info string that is below the bar
        void SetInfo( const std::string& msg );
    private:

};

#endif // WATER_UILOADINGSCREEN
