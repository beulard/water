#include "UIShape.hpp"
#include "Common.hpp"

UIShape::UIShape() : shape( NULL ) {

}

UIShape::~UIShape() {
    if( shape )
        delete shape;
}

void UIShape::SetShape( Shape* s ) {
    shape = s;
    shape->SetRenderMode( RM_UIColored );
    shape->SetDepth( RD_UISprites );
}

void UIShape::SetSize( const vec2i& sz ) {
    if( shape )
        shape->SetSize( sz );
    size = sz;
}

void UIShape::SetDepth( RenderDepth d ) {
    if( shape )
        shape->SetDepth( d );
    depth = d;
}

void UIShape::Move( const vec2i& offset ) {
    Widget::Move( offset );
    if( shape )
        shape->Move( offset );
}

void UIShape::Sleep() {
    if( drawn )
        StopDrawing();
}

void UIShape::Activate() {
    if( !drawn )
        Draw();
}

void UIShape::Draw() {
    if( !drawn )
        shape->Draw();
    else
        Warning( "Trying to add an ui shape to the renderer twice" );
    drawn = true;
}

void UIShape::StopDrawing() {
    if( drawn )
        shape->StopDrawing();
    else
        Warning( "Trying to remove a ui shape from renderer but it is not in" );
    drawn = false;
}

