#include "UIColored.hpp"
#include "Colored.hpp"
#include "Common.hpp"

UIColored::UIColored() : colored( NULL ) {

}

UIColored::~UIColored() {
    if( colored )
        delete colored;
}

void UIColored::SetColored( Colored* s ) {
    colored = s;
    colored->SetRenderMode( RM_UIColored );
    colored->SetDepth( RD_UISprites );
}

void UIColored::SetSize( const vec2ui& sz ) {
    if( colored )
        colored->SetSize( sz );
    size = sz;
}

void UIColored::SetDepth( RenderDepth d ) {
    if( colored )
        colored->SetDepth( d );
    depth = d;
}

void UIColored::Move( const vec2i& offset ) {
    Widget::Move( offset );
    if( colored )
        colored->Move( offset );
}

void UIColored::Sleep() {
    if( drawn )
        StopDrawing();
}

void UIColored::Activate() {
    if( !drawn )
        Draw();
}

void UIColored::Draw() {
    if( !drawn )
        colored->Draw();
    else
        Warning( "Trying to add an ui shape to the renderer twice" );
    drawn = true;
}

void UIColored::StopDrawing() {
    if( drawn )
        colored->StopDrawing();
    else
        Warning( "Trying to remove a ui shape from renderer but it is not in" );
    drawn = false;
}

