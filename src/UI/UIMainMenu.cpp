#include "UIMainMenu.hpp"
#include "Game.hpp"
#include "UIColored.hpp"
#include "Quad.hpp"
#include "UIWindow.hpp"


UIMainMenu::UIMainMenu(){
    Font& buttonFont = Global::dataLoader->GetFont( "dejavusans", 24 );
    Font& titleFont = Global::dataLoader->GetFont( "dejavusans", 124 );

    //  Main menu's background, a transparent dark gray color
    children.push_back( new UIColored );
    Colored* q = new Quad( vec2i( 0, 0 ), *Global::winSz );
    q->SetColor( Color( 100, 100, 100, 50 ) );
    q->SetDepth( RD_MiddleLayer );
    dynamic_cast< UIColored* >( children.back() )->SetColored( q );

    //  The Play button
    children.push_back( new UIButton );
    UIButton* play = dynamic_cast<UIButton*>( children.back() );
    play->SetFont( &buttonFont );
    play->SetString( "Play" );
    play->SetPosition( vec2i( Global::winSz->x / 2 - play->GetSize().x / 2, Global::winSz->y / 2 - play->GetSize().y * 2 - 5 ) );
    play->SetReleaseCallback( [this]() {
        Global::Water.state = GS_Game;
    } );
    play->Disable();

    //  The Load button
    children.push_back( new UIButton );
    UIButton* load = dynamic_cast<UIButton*>( children.back() );
    load->SetFont( &buttonFont );
    load->SetString( "Load" );
    load->SetPosition( play->GetPosition() + vec2i( 0, play->GetSize().y + 2 ) );

    //  The Generate button
    children.push_back( new UIButton );
    UIButton* generate = dynamic_cast<UIButton*>( children.back() );
    generate->SetFont( &buttonFont );
    generate->SetString( "Generate" );
    generate->SetPosition( load->GetPosition() + vec2i( 0, load->GetSize().y + 2 ) );
    generate->SetReleaseCallback( [this](){
        Sleep();
        Global::world->map.GenerateWithScreen();
        Activate();
        dynamic_cast<UIButton*>(children[1])->Enable();
    } );

    //  The Options button
    children.push_back( new UIButton );
    UIButton* options = dynamic_cast<UIButton*>( children.back() );
    options->SetFont( &buttonFont );
    options->SetString( "Options" );
    //  The position is perfectly in the middle of the screen.
    //  The other buttons will be placed above and below this one
    options->SetPosition( vec2i( Global::winSz->x / 2 - options->GetSize().x / 2, generate->GetPosition().y + generate->GetSize().y + 2 ) );
    options->SetReleaseCallback( [this]() {
         if( children[7]->IsActive() )
             children[7]->Sleep();
         else
             children[7]->Activate();
     } );

    //  The Quit button
    children.push_back( new UIButton );
    UIButton* quit = dynamic_cast<UIButton*>( children.back() );
    quit->SetFont( &buttonFont );
    quit->SetString( "Quit" );
    quit->SetPosition( vec2i( Global::winSz->x / 2 - quit->GetSize().x / 2, options->GetPosition().y + options->GetSize().y + 2 ) );
    quit->SetReleaseCallback( [this]() {
        Global::Water.running = false;
    } );

    //  The menu's title
    children.push_back( new UIText );
    UIText* title = dynamic_cast<UIText*>( children.back() );
    title->SetFont( &titleFont );
    title->SetString( "Water" );
    title->SetPosition( vec2i( Global::winSz->x / 2 - title->GetSize().x / 2, 10 ) );

    //  Options window, activated by the options button
    children.push_back( new UIWindow );
    children.back()->Sleep();
    //UIWindow* opwin = dynamic_cast<UIWindow*>( children.back() );

    children.push_back( new UIText );
    UIText* version = dynamic_cast<UIText*>( children.back() );
    std::string ver = IntToString( Global::Water.Version.Major ) + "." + IntToString( Global::Water.Version.Minor ) + "." + IntToString( Global::Water.Version.Revision );
    version->SetFont( &Global::dataLoader->GetFont( "dejavusans", 16 ) );
    version->SetString( ver );
    version->SetPosition( *Global::winSz - version->GetSize() - vec2i( 1, 1 ) );
}

UIMainMenu::~UIMainMenu(){

}

void UIMainMenu::Activate() {
    //  Hide the options window by default
    for( u32 i = 0; i < children.size(); ++i ) {
        if( i != 7 )
            children[i]->Activate();
    }
    active = true;
}
