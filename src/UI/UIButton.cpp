#include "UIButton.hpp"
#include "DataLoader.hpp"
#include "InputManager.hpp"

UIButton::UIButton() : enabled(true), usesText(false) {
    //  Default size for main menu buttons
	size = vec2ui( Global::winSz->x / 2, 40 );

    //  Default callbacks do nothing
    hoverCallback = [this]() {};
    pressCallback = [this]() {};
    releaseCallback = [this]() {};

    children.push_back( new UISprite );
    children.push_back( new UISprite );
    children.push_back( new UISprite );
    children.push_back( new UISprite );


    SetNormalSprite( Global::dataLoader->GetSprite( "button_normal" ) );
    SetHoverSprite( Global::dataLoader->GetSprite( "button_hover" ) );
    SetPressSprite( Global::dataLoader->GetSprite( "button_press" ) );
    SetDisabledSprite( Global::dataLoader->GetSprite( "button_disabled" ) );
}

UIButton::~UIButton(){

}

void UIButton::SetNormalSprite( const Sprite& s ) {
    *dynamic_cast<UISprite*>(children[0]) = Sprite(s);
    children[0]->SetSize( size );
}

void UIButton::SetHoverSprite( const Sprite& s ) {
    *dynamic_cast<UISprite*>(children[1]) = Sprite(s);
    children[1]->SetSize( size );
}

void UIButton::SetPressSprite( const Sprite& s ) {
    *dynamic_cast<UISprite*>(children[2]) = Sprite(s);
    children[2]->SetSize( size );
}

void UIButton::SetDisabledSprite( const Sprite& s ) {
    *dynamic_cast<UISprite*>(children[3]) = Sprite(s);
    children[3]->SetSize( size );
}

void UIButton::Enable() {
    enabled = true;
    if( drawn ) {
        if( children[3]->IsDrawn() )
            children[3]->StopDrawing();
        for( u32 i = 0; i < 3; ++i )
            if( !children[i]->IsDrawn() )
                children[i]->Draw();
		if( usesText )
			if( !children[4]->IsDrawn() )
				children[4]->Draw();
    }
}

void UIButton::Disable() {
    enabled = false;
    if( drawn ) {
        for( u32 i = 0; i < 3; ++i )
            if( children[i]->IsDrawn() )
                children[i]->StopDrawing();
        if( !children[3]->IsDrawn() )
            children[3]->Draw();
    }
}


void UIButton::SetString( const std::string& s ) {
    UISprite* normal = dynamic_cast<UISprite*>( children[0] );
    UISprite* hover = dynamic_cast<UISprite*>( children[1] );
    UISprite* press = dynamic_cast<UISprite*>( children[2] );
    UISprite* disabled = dynamic_cast<UISprite*>( children[3] );
    if( !usesText )
        children.push_back( new UIText );
    UIText* text = dynamic_cast<UIText*>( children[4] );
    text->SetString( s );
	if( size.y - 40 < text->GetSize().y ) {
		size.y = 40;
		size.x = Global::winSz->x / 2;
		normal->SetSize( size );
		hover->SetSize( size );
		press->SetSize( size );
		disabled->SetSize( size );
		normal->SetPosition( position );
		hover->SetPosition( position );
		press->SetPosition( position );
		disabled->SetPosition( position );
		text->SetPosition( position + size / 2 - text->GetSize() / 2 );
	}
	usesText = true;
}

void UIButton::SetTextColor( const Color& col ) {
	if( usesText )
		dcast<UIText*>(children[4])->SetColor( col );
}

void UIButton::SetFont( Font* f ) {
    if( !usesText )
        children.push_back( new UIText );
    dynamic_cast<UIText*>( children[4] )->SetFont( f );
    usesText = true;
}

void UIButton::SetSize( const vec2ui& sz ) {
    for( u32 i = 0; i < 4; ++i ) {
        children[i]->SetSize( sz );
    }
    size = sz;
}

void UIButton::SetHoverCallback( const ButtonCallback& cb ) {
    hoverCallback = cb;
}

void UIButton::SetPressCallback( const ButtonCallback& cb ) {
    pressCallback = cb;
}

void UIButton::SetReleaseCallback( const ButtonCallback& cb ) {
    releaseCallback = cb;
}

bool UIButton::OnHover() {
    if( enabled ) {
        Recti rect( vec2i( position.x, position.y ), vec2i( position.x + size.x, position.y + size.y ) );
        if( rect.Contains( Global::inputMgr->GetMousePos() ) ) {
            if( state != WS_Hover ) {
                if( children[0]->IsDrawn() )
                    children[0]->StopDrawing();
                if( children[2]->IsDrawn() )
                    children[2]->StopDrawing();
                if( !children[1]->IsDrawn() )
                    children[1]->Draw();
                state = WS_Hover;
                hoverCallback();
            }
            return true;
        }
        else {
            if( !children[0]->IsDrawn() )
                children[0]->Draw();
            if( children[1]->IsDrawn() )
                children[1]->StopDrawing();
            if( children[2]->IsDrawn() )
                children[2]->StopDrawing();
            state = WS_Normal;
        }
    }
    return false;
}

bool UIButton::OnPress() {
    if( enabled ) {
        if( state == WS_Hover ) {
            if( children[1]->IsDrawn() )
                children[1]->StopDrawing();
            children[2]->Draw();
            state = WS_Press;
            pressCallback();
            return true;
        }
    }
    return false;
}

bool UIButton::OnRelease() {
    if( enabled ) {
        Recti rect( vec2i( position.x, position.y ), vec2i( position.x + size.x, position.y + size.y ) );
        if( state == WS_Press && rect.Contains( Global::inputMgr->GetMousePos() ) ) {
            if( children[1]->IsDrawn() )
                children[1]->StopDrawing();
            if( children[2]->IsDrawn() )
                children[2]->StopDrawing();
            children[0]->Draw();
            state = WS_Normal;
            releaseCallback();
            return true;
        }
    }
    return false;
}

void UIButton::Activate() {
    if( !drawn )
        Draw();
    active = true;
}

void UIButton::Sleep() {
    if( drawn )
        StopDrawing();
    active = false;
}

void UIButton::Draw() {
    if( !drawn ) {
        if( enabled ) {
            if( children[3]->IsDrawn() )
                children[3]->StopDrawing();
            switch( state ) {
                case WS_Normal :
                    if( !children[0]->IsDrawn() )
                        children[0]->Draw();
                break;
                case WS_Hover :
                    if( !children[1]->IsDrawn() )
                        children[1]->Draw();
                break;
                case WS_Press :
                    if( !children[2]->IsDrawn() )
                        children[2]->Draw();
                break;
            }
        }
        else {
            for( u32 i = 0; i < 3; ++i ) 
                if( children[i]->IsDrawn() )
                    children[i]->StopDrawing();
            if( !children[3]->IsDrawn() )
                children[3]->Draw();
        }
        if( usesText )
            if( !children[4]->IsDrawn() )
                children[4]->Draw();
        drawn = true;
    }
    else
        Warning( "Trying to add a ui button to the renderer twice" );
}

void UIButton::StopDrawing() {
    if( drawn ) {
        for( u32 i = 0; i < children.size(); ++i )
            if( children[i]->IsDrawn() )
                children[i]->StopDrawing();
    }
    else
        Warning( "Trying to remove an ui button from the renderer but it is not in" );
    drawn = false;
}
