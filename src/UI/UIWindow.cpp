#include "UIWindow.hpp"
#include "UIWindowBar.hpp"
#include "UIColored.hpp"
#include "Quad.hpp"

UIWindow::UIWindow() {
    //  Window's bar
    children.push_back( new UIWindowBar );
    children.back()->SetSize( vec2ui( 200, 18 ) );
    dynamic_cast<UIWindowBar*>(children.back())->SetTitle( "Options" );
    children.back()->SetParent( this );

    //  Window's background
    children.push_back( new UIColored );
	Colored* quad = new Quad;
	quad->SetColor( Color( 100, 100, 100, 200 ) );
    UIColored* background = dynamic_cast<UIColored*>( children.back() );
	background->SetColored( quad );
    background->SetSize( vec2ui( 200, 200 ) );
    background->Move( vec2i( 0, 19 ) );
}

UIWindow::~UIWindow() {

}

void UIWindow::SetTitle( const std::string& title ) {
    dynamic_cast<UIWindowBar*>(children[0])->SetTitle( title );
}
