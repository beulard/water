#ifndef WATER_UISHAPE
#define WATER_UISHAPE
#include "Widget.hpp"
#include "Shape.hpp"

class UIShape : public Widget
{
    public:
        UIShape();
        ~UIShape();

        void SetShape( Shape* s );
        virtual void SetSize( const vec2i& sz );
        virtual void SetDepth( RenderDepth d );

        virtual void Move( const vec2i& offset );

        virtual void Draw();
        virtual void StopDrawing();

        virtual void Sleep();
        virtual void Activate();

private :
        Shape* shape;
};

#endif // WATER_UISHAPE
