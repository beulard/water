#include "Widget.hpp"
#include "RootWidget.hpp"

Widget::Widget() : active(true), state(WS_Normal){
	SetRenderMode( RM_None );
}

Widget::~Widget(){
    for( u32 i = 0; i < children.size(); ++i )
        delete children[i];
}

void Widget::Draw() {
    if( !drawn )
        for( u32 i = 0; i < children.size(); ++i )
            children[i]->Draw();
    else
        Warning( "Trying to add a widget to the renderer twice" );
    drawn = true;
}

void Widget::StopDrawing() {
    if( drawn )
        for( u32 i = 0; i < children.size(); ++i )
            children[i]->StopDrawing();
    else
        Warning( "Trying to remove a widget from the renderer but it is not in" );
    drawn = false;
}

void Widget::Render() {
    for( u32 i = 0; i < children.size(); ++i ) {
        children[i]->Render();
    }
}

void Widget::SetPosition( const vec2i& pos ) {
    vec2i offset = pos - position;
    Move( offset );
}

void Widget::Move( const vec2i& offset ) {
    position += offset;
    for( u32 i = 0; i < children.size(); ++i )
        children[i]->Move( offset );
}

const vec2i& Widget::GetPosition()   const {
    return position;
}

bool Widget::OnHover() {
    bool result = false;
    if( active ) {
        for( u32 i = 0; i < children.size(); ++i )
            if( children[i]->IsActive() )
                if( children[i]->OnHover() )
                    result = true;
    }
    return result;
}

bool Widget::OnPress() {
    bool result = false;
    if( active ) {
        for( u32 i = 0; i < children.size(); ++i )
            if( children[i]->IsActive() )
                if( children[i]->OnPress() )
                    result = true;
    }
    return result;
}

bool Widget::OnRelease() {
    bool result = false;
    if( active ) {
        for( u32 i = 0; i < children.size(); ++i )
            if( children[i]->IsActive() )
                if( children[i]->OnRelease() )
                    result = true;
    }
    return result;
}

Widget* Widget::GetChild( u32 id ) {
    if( id >= children.size() )
        Error( "Trying to access a widget's child that does not exist (id=%d)", id );
    return children[id];
}

void Widget::SetParent( Widget* w ) {
    parent = w;
}

void Widget::SetDepth( RenderDepth d ) {
    depth = d;
    for( u32 i = 0; i < children.size(); ++i )
        children[i]->SetDepth( d );
}

bool Widget::HasChildren()  const {
    return !children.empty();
}

void Widget::AddToRoot() {
    Global::rootWidget->AddChild( this );
    parent = Global::rootWidget;
}

void Widget::Sleep() {
    active = false;
    for( u32 i = 0; i < children.size(); ++i )
        children[i]->Sleep();
}

void Widget::Activate() {
    active = true;
    for( u32 i = 0; i < children.size(); ++i )
        children[i]->Activate();
}

bool Widget::IsActive() const{
    return active;
}
