#ifndef WATER_UITEXT
#define WATER_UITEXT
#include "Widget.hpp"
#include "Text.hpp"

class UIText : public Widget
{
    public:
        UIText();
        ~UIText();

        virtual void Move( const vec2i& offset );

		void SetColor( const Color& col );
        void SetFont( Font* f );
        void SetString( const std::string& s );

        virtual void Sleep();
        virtual void Activate();

        virtual void Draw();
        virtual void StopDrawing();
        virtual void Render();

		virtual void SetDepth( RenderDepth d );

    protected:
        Text text;
};

#endif // WATER_UITEXT
