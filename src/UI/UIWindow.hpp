#ifndef WATER_UIWINDOW
#define WATER_UIWINDOW
#include "Widget.hpp"
#include <string>

class UIWindow : public Widget {
    public:
        UIWindow();
        ~UIWindow();

        void SetTitle( const std::string& title );

};

#endif // WATER_UIWINDOW
