#include "UISprite.hpp"

UISprite::UISprite(){
    mode = RM_UISprite;
	sprite.SetDepth( RD_UISprites );
    sprite.SetRenderMode( mode );
    SetPosition( vec2i( 0, 0 ) );
}

UISprite::UISprite( Texture* t, const vec2i& pos, const vec2ui& sz ) {
    mode = RM_UISprite;
    sprite.SetPosition( pos );
    sprite.SetTexture( t );
    sprite.SetSize( sz );
    sprite.SetRenderMode( mode );
	sprite.SetDepth( RD_UISprites );
    position = pos;
    size = sz;
}

UISprite::UISprite( const Sprite& s ) {
    *this = s;
}

void UISprite::operator=( const Sprite& s ) {
    //  Copy the sprite
    sprite = Sprite(s);
    sprite.SetRenderMode( RM_UISprite );
}

UISprite::~UISprite(){

}

void UISprite::SetTexture( Texture* t ) {
    sprite.SetTexture( t );
}

void UISprite::Move( const vec2i& offset ) {
    Widget::Move( offset );
    sprite.Move( offset );
}

void UISprite::SetSize( const vec2ui& sz ) {
    sprite.SetSize( sz );
    size = sz;
}

void UISprite::SetSubRect( const Recti& rect ) {
    sprite.SetSubRect( rect );
}

void UISprite::Sleep() {
    if( drawn )
        StopDrawing();
}

void UISprite::Activate() {
    if( !drawn )
        Draw();
}

void UISprite::Draw() {
    if( !drawn )
        sprite.Draw();
    else
        Warning( "Trying to add a ui sprite to render twice" );
    drawn = true;
}

void UISprite::StopDrawing() {
    if( drawn )
        sprite.StopDrawing();
    else
        Warning( "Trying to remove a ui sprite from renderer but it is not in" );
    drawn = false;
}

void UISprite::SetDepth( RenderDepth d ) {
    depth = d;
    sprite.SetDepth( d );
}
