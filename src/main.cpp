#include "Common.hpp"
#include "Game.hpp"

Game Global::Water;
using namespace Global;

int main( int argc, char** argv ) {
    bool success = 1;
    if( Water.Init() == Game::Success )
        success = Water.Run();

    Water.Clean();

    Append( "\n\n\n[:::::::::::: End of Output ::::::::::::]\n\n\n" );

    //  Close the log file and exit
	fclose( Log );

    return success;
}
