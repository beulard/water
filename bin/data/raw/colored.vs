#version 120

attribute vec2 vertex;
attribute vec4 color;

varying vec4 fColor;

uniform vec2 camPos;
uniform mat3 projMat;

void main(){
	vec4 pos = vec4( projMat * vec3( vertex - camPos, 1 ), 1 );
	fColor = color;
	gl_Position = pos;
}
