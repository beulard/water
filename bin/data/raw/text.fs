#version 120

varying vec2 UV;
uniform vec4 color;

uniform sampler2D textureSampler;

void main(){
	gl_FragColor = vec4( 1, 1, 1, texture2D( textureSampler, UV ).a ) * color;
}
