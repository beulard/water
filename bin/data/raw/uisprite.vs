#version 120

attribute vec2 vertex;
attribute vec2 uv;

varying vec2 UV;

uniform mat3 projMat;

void main(){
	vec4 pos = vec4( projMat * vec3( vertex, 1 ), 1 );
	UV = uv;
	gl_Position = pos;
}
